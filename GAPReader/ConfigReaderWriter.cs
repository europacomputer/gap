﻿using System;
using System.Text;
using ReaderB;
using System.Threading;

namespace GAPReader
{
    public class ConfigReaderWriter
    {
        private int comPort;
        private string ip;
        private byte fComAdr = 0;

        public ConfigReaderWriter(string ip)
        {
            if (string.IsNullOrEmpty(ip))
            {
                throw new ArgumentException("IP non valido", nameof(ip));
            }

            this.ip = ip;
            fComAdr = Convert.ToByte("FF", 16); // $FF;
            //ApriPorta();
        }

        private void ApriPorta()
        {
            Console.WriteLine("stato:2");
            comPort = 0;
            byte fBaud = 0;
            int result = StaticClassReaderB.OpenNetPort(6000, ip, ref fBaud, ref comPort);
            if (result != 0)
            {
                Console.WriteLine("stato:1");
            }
            else
            {
                Console.WriteLine("stato:0");
            }
        }

        private string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            return sb.ToString().ToUpper();
        }

        private string Leggi()
        {
            int CardNum = 0;
            int Totallen = 0;
            int EPClen;
            int m;
            int CardIndex;
            byte AdrTID = 0;
            byte LenTID = 0;
            byte TIDFlag = 0;
            byte[] EPC = new byte[5000];
            string sEPC;
            string temps = "";
            int fCmdRet = StaticClassReaderB.Inventory_G2(ref fComAdr, AdrTID, LenTID, TIDFlag, EPC, ref Totallen, ref CardNum, comPort);
            if ((fCmdRet == 1) | (fCmdRet == 2) | (fCmdRet == 3) | (fCmdRet == 4) | (fCmdRet == 0xFB))
            {
                if (Totallen == 0)
                {
                    return "";
                }
                byte[] daw = new byte[Totallen];
                Array.Copy(EPC, daw, Totallen);
                temps = ByteArrayToHexString(daw);
                m = 0;
                if (CardNum > 0)
                {
                    StringBuilder letture = new StringBuilder(100);
                    for (CardIndex = 0; CardIndex < CardNum; CardIndex++)
                    {
                        EPClen = daw[m];
                        sEPC = temps.Substring(m * 2 + 2, EPClen * 2);
                        m = m + EPClen + 1;
                        if (sEPC.Length != EPClen * 2)
                        {
                            return "";
                        }
                        letture.Append(sEPC).Append("#");
                    }
                    return (letture.ToString().Substring(0, letture.Length - 1));
                }
            }
            else
            {
                return ("error");
            }
            return "";
        }

        private byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        private string LeggiTID(String epcString)
        {
            byte ENum = Convert.ToByte(epcString.Length / 4);
            byte EPClength = Convert.ToByte(epcString.Length / 2);
            byte[] EPC = new byte[ENum];
            EPC = HexStringToByteArray(epcString);
            byte Num = Convert.ToByte("6");
            byte[] CardData = new byte[320];
            byte[] fPassWord = HexStringToByteArray("");
            int ferrorcode = 0;
            int fCmdRet = StaticClassReaderB.ReadCard_G2(ref fComAdr, EPC, 2, Convert.ToByte("0", 16), Num, fPassWord, Convert.ToByte("0", 16), Convert.ToByte("0", 16), 0, CardData, EPClength, ref ferrorcode, comPort);
            string result = "";
            if (fCmdRet == 0)
            {
                byte[] daw = new byte[Num * 2];
                Array.Copy(CardData, daw, Num * 2);
                result = ByteArrayToHexString(daw);
            }
            if (ferrorcode != -1)
            {
                result = ("ERR:" + ferrorcode);
            }
            return result;
        }

        private void Scrivi(String epc)
        {
            try
            {
                byte[] WriteEPC = new byte[100];
                byte WriteEPClen;
                byte ENum;
                string nuovoEPC = epc.PadLeft(16, '0');
                if ((nuovoEPC.Length % 4) != 0)
                {
                    Console.WriteLine("La lunghezza del codice deve essere un multiplo di 4. Attuale:" + nuovoEPC.Length, "Informazione");
                    return;
                }
                WriteEPClen = Convert.ToByte(nuovoEPC.Length / 2);
                ENum = Convert.ToByte(nuovoEPC.Length / 4);
                byte[] EPC = HexStringToByteArray(nuovoEPC);
                int ferrorcode = 0;
                int fCmdRet = StaticClassReaderB.WriteEPC_G2(ref fComAdr, HexStringToByteArray("00000000"), EPC, WriteEPClen, ref ferrorcode, comPort);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // throw ex;
            }
        }

        public void Avvia()
        {
            ApriPorta();
            string command = "";
            while (command != "chiudi")
            {
                command = Console.ReadLine();
                switch (command)
                {
                    case "leggi":
                        string epc = Leggi();
                        if (epc.Contains("#"))
                        {
                            Console.WriteLine("Letti tag multipli:" + epc);
                            break;
                        }
                        if (!String.IsNullOrEmpty(epc))
                        {
                            string tid = LeggiTID(epc);
                            Console.WriteLine(epc + ":" + tid);
                        }
                        break;

                    case "restart":
                        Console.WriteLine("stato:1");
                        StaticClassReaderB.CloseNetPort(comPort);
                        ApriPorta();
                        break;

                    default:
                        Scrivi(command);
                        break;
                }
            }
        }
    }
}