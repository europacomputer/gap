﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GAPReader
{
    internal class FileReader
    {
        public void Leggi(string path)
        {
            while (true)
            {
                Thread.Sleep(500);
                if (File.Exists(path))
                {
                    Console.WriteLine(File.ReadAllText(path, Encoding.Default));
                    File.Delete(path);
                }
            }
        }
    }
}