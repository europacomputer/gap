﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace GAPReader
{
    internal class MultiReader
    {
        private void ApriPorta(String ip)
        {
            Console.WriteLine("stato:2");
            n_errori = 0;
            comPort = 0;
            byte fBaud = 0;
            int result = StaticClassReaderMultiB.OpenNetPort(6000, ip, ref fBaud, ref comPort);
            if (result != 0)
            {
                Console.WriteLine("stato:1");
            }
            else
            {
                Console.WriteLine("stato:0");
            }
        }

        private int n_errori;
        private int comPort;

        public void Leggi(string ip)
        {
            ApriPorta(ip);
            while (true)
            {
                int CardNum = 0;
                int EPClen;
                int m;
                byte[] EPC = new byte[5000];
                int CardIndex;
                string temps;
                string sEPC;
                byte AdrTID = 0;
                byte LenTID = 0;
                byte TIDFlag = 0;
                byte fComAdr = 0;
                byte Ant = 0;
                int Totallen = 0;
                Thread.Sleep(60);
                int fCmdRet = StaticClassReaderMultiB.Inventory_G2(ref fComAdr, 10, 0, AdrTID, LenTID, TIDFlag, EPC, ref Ant, ref Totallen, ref CardNum, comPort);
                if ((fCmdRet == 1) | (fCmdRet == 2) | (fCmdRet == 3) | (fCmdRet == 4) | (fCmdRet == 0xFB))
                {
                    if (Totallen == 0)
                    {
                        continue;
                    }
                    byte[] daw = new byte[Totallen];
                    Array.Copy(EPC, daw, Totallen);
                    temps = ByteArrayToHexString(daw);
                    m = 0;
                    if (CardNum > 0)
                    {
                        StringBuilder letture = new StringBuilder(100);
                        for (CardIndex = 0; CardIndex < CardNum; CardIndex++)
                        {
                            EPClen = daw[m];
                            sEPC = temps.Substring(m * 2 + 2, EPClen * 2);
                            m = m + EPClen + 2;
                            if (sEPC.Length != EPClen * 2)
                                return;
                            letture.Append(sEPC).Append("#");
                        }
                        Console.WriteLine(Ant.ToString() + ":" + letture.ToString().Substring(0, letture.Length - 1));
                    }
                }
                else
                {
                    if (n_errori == 10)
                    {
                        Console.WriteLine("stato:1");
                        StaticClassReaderMultiB.CloseNetPort(comPort);
                        ApriPorta(ip);
                    }
                    else { n_errori++; }
                }
            }
        }

        private string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            return sb.ToString().ToUpper();
        }
    }
}