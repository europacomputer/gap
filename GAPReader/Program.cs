﻿using System;
using System.Text;

namespace GAPReader
{
    internal class Program
    {
        public Program()
        {
        }

        private static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                args = new String[] { "c:192.168.1.193" };
            }

            String ip = args[0];
            if (ip.StartsWith("c:"))
            {
                ConfigReaderWriter readerwriter = new ConfigReaderWriter(ip.Substring(2));
                readerwriter.Avvia();
            }
            else if (ip.StartsWith("m:"))
            {
                MultiReader reader = new MultiReader();
                reader.Leggi(ip.Substring(2));
            }
            else if (ip.StartsWith("f:"))
            {
                FileReader reader = new FileReader();
                reader.Leggi(ip.Substring(2));
            }
            else
            {
                SingleReader reader = new SingleReader();
                reader.Leggi(ip);
            }
        }
    }
}