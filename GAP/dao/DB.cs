﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;

namespace GAP.Dao
{
    public class DB : IDisposable
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Object _lock = new Object();
        private SQLiteDatabase db;

        public DB()
        {
            lock (_lock)
            {
                db = new SQLiteDatabase();
                Boolean creaDatabase = !File.Exists(db.DBFileNamePath);
                db.OpenConnection();
                if (db.ConnectionState == false)
                {
                    log.Error("ERROR: Cannot open Database connection.\n" + db.Status);
                }
                if (creaDatabase)
                {
                    CreaDB();
                }
            }
        }

        private void CreaDB()
        {
            log.Debug("Creo il database");
            ExecuteCommand("CREATE TABLE  IF NOT EXISTS `Presenze` (`Tag`	TEXT,`Area`	TEXT,`Data`	NUMERIC)");
            log.Debug("Tabella presenze creata");
            ExecuteCommand("CREATE TABLE  IF NOT EXISTS `Regole` (`Id`	INTEGER PRIMARY KEY AUTOINCREMENT,`TAG1`	TEXT,`TAG2`	TEXT,`PERIODO`	TEXT,`AREA`	TEXT)");
            log.Debug("Tabella Regole creata");
            ExecuteCommand("CREATE TABLE  IF NOT EXISTS `Audit` (`Id`	INTEGER PRIMARY KEY AUTOINCREMENT,`Tag`	" +
                "TEXT,`Evento`	TEXT,`Anomalia`	TEXT,`Nota`	TEXT,`Data`	NUMERIC,`Area` TEXT)");
            log.Debug("Tabella Audit creata");
            ExecuteCommand("CREATE TABLE  IF NOT EXISTS `Anagrafiche` (`Codice`	TEXT  PRIMARY KEY,`Denominazione`	TEXT,`Tag` TEXT)");
            log.Debug("Tabella anagrafiche creata");
            ExecuteCommand("CREATE TABLE `Aree` (`Nome`	TEXT,`Principale`	TEXT,`Id`	INTEGER PRIMARY KEY AUTOINCREMENT)");
            log.Debug("Tabella Aree creata");
            ExecuteCommand("CREATE TABLE `Antenne` (`Codice` TEXT NOT NULL,	`Tipo`	TEXT NOT NULL,	`Area`	TEXT,`Abilitata`	NUMERIC,`Numero`	INTEGER NOT NULL,`Gate`	TEXT,`Ip`	TEXT,`X`	TEXT,	`Y`	TEXT,`Prolunga_presenza` INTEGER,`configurazioneRilevatoreIngresso` TEXT,`configurazioneRilevatoreUscita` TEXT , PRIMARY KEY(`Numero`))");
            log.Debug("Tabella Antenne creata");
            ExecuteCommand("CREATE TABLE `Gates` (	`Id`	INTEGER PRIMARY KEY AUTOINCREMENT,	`Nome`	TEXT,`Codice` TEXT 	`Area` TEXT ,`Ip`	TEXT,`Porta`	INTEGER,`X`	TEXT,`Y` TEXT)");
            log.Debug("Tabella Gates creata");
            ExecuteCommand("CREATE TABLE `Configurazione` (`MinutiWarning`	TEXT,`OrarioMail`	TEXT,`MailAddress`	TEXT,`UserMail`	TEXT,`ServerMail`	TEXT,`PasswordMail`	TEXT,`PortMail`	TEXT,`From`	TEXT); ");
            log.Debug("Tabella Configurazione creata");
            ExecuteCommand("INSERT INTO Configurazione('MinutiWarning','OrarioMail') values ('10','18:30')");
            for (int i = 1; i < 11; i++)
            {
                ExecuteCommand("insert into antenne (Numero,Abilitata,Codice,Tipo,Prolunga_presenza) values (" + i + ",0,'Antenna " + i + "','I',20)");
            }
        }

        public T ExecuteSelect<T>(string sqlCommand, Func<SQLiteDataReader, T> fillFunction, SQLiteParameter[] parameters = null)
        {
            try
            {
                lock (_lock)
                {                    
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, db.Connection))
                    {
                        if (parameters != null)
                        {
                            cmd.Parameters.AddRange(parameters);
                        }
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        T result = fillFunction(reader);
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw ex;
            }
        }

        public List<T> ExecuteSelect<T>(string sqlCommand, Func<SQLiteDataReader, List<T>> fillFunction, SQLiteParameter[] parameters = null)
        {
            try
            {
                lock (_lock)
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, db.Connection))
                    {
                        if (parameters != null)
                        {
                            cmd.Parameters.AddRange(parameters);
                        }
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        List<T> result = fillFunction(reader);
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        public void ExecuteCommand(String sqlCommand, SQLiteParameter[] parameters = null)
        {
            try
            {
                lock (_lock)
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(sqlCommand, db.Connection))
                    {
                        if (parameters != null)
                        {
                            cmd.Parameters.AddRange(parameters);
                        }
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Errore esecuzione query:");
                log.Error(sqlCommand);
                log.Error(ex);
            }
        }

        public void Dispose()
        {
            db.CloseConnection();
        }
    }
}