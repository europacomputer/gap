﻿using Microsoft.Owin.FileSystems;
using Microsoft.Owin.Hosting;
using Microsoft.Owin.StaticFiles;
using Owin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Thinktecture.IdentityModel.Owin;
using Topshelf;

namespace GAP
{
    public class Program
    {
        public static int Main(string[] args)
        {
            //lanciare con install per installarlo
            return (int)HostFactory.Run(x =>
                    {
                        x.Service<OwinService>(s =>
                        {
                            s.ConstructUsing(() => new OwinService());
                            s.WhenStarted(service => service.Start());
                            s.WhenStopped(service => service.Stop());
                        });
                    });
        }
    }

    public class OwinService
    {
        private IDisposable _webApp;
        private Controller.AccessiController controller = new Controller.AccessiController();
        private Dictionary<String, String> permessi;

        private Task<IEnumerable<Claim>> Authenticate(string username, string password)
        {
            if (permessi == null)
            {
                permessi = new Dictionary<string, string>
                {
                    { "admin", System.Configuration.ConfigurationManager.AppSettings["admin"] },
                    { "poweruser", System.Configuration.ConfigurationManager.AppSettings["poweruser"] },
                    { "user", System.Configuration.ConfigurationManager.AppSettings["user"] }
                };
            }
            if (permessi.ContainsKey(username) && permessi[username] == password)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Role, username)
            };
                return Task.FromResult<IEnumerable<Claim>>(claims);
            }

            return Task.FromResult<IEnumerable<Claim>>(null);
        }

        public void Start()
        {
            StartOptions option;
            if (System.Diagnostics.Debugger.IsAttached)
                option = new StartOptions("http://localhost:9000");
            else
                option = new StartOptions("http://*:9000");
            _webApp = WebApp.Start(option, (app) =>
            {
                app.UseBasicAuthentication(new BasicAuthenticationOptions("SecureApi", Authenticate));
                var config = new HttpConfiguration();
                config.Formatters.Add(new BrowserJsonFormatter());
                config.MapHttpAttributeRoutes();
                app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
                app.UseWebApi(config);
                app.UseDefaultFiles();
                config.Properties.TryAdd("AccessiController", controller);

                var physicalFileSystem = new PhysicalFileSystem(@"./www");
                var options = new FileServerOptions
                {
                    EnableDefaultFiles = true,
                    FileSystem = physicalFileSystem
                };
                options.StaticFileOptions.FileSystem = physicalFileSystem;
                options.StaticFileOptions.ServeUnknownFileTypes = true;
                options.DefaultFilesOptions.DefaultFileNames = new[]
                {
                    "index.html"
            };
                app.UseFileServer(options);
            });
            foreach (var process in Process.GetProcessesByName("GAPReader"))
            {
                process.Kill();
            }
            controller.StartAsync();
        }

        public void Stop()
        {
            _webApp.Dispose();
            controller.Stop();
        }
    }

    //public class StartOwin
    //{
    //    public void Configuration(IAppBuilder app)
    //    {
    //        var config = new HttpConfiguration();
    //        config.Formatters.Add(new BrowserJsonFormatter());
    //        config.MapHttpAttributeRoutes();
    //        app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
    //        app.UseDefaultFiles();

    //        app.UseBasicAuthentication(new BasicAuthenticationOptions("SecureApi",
    //            async (username, password) => await Authenticate(username, password)));
    //        //config.SuppressDefaultHostAuthentication();
    //        //config.Filters.Add(new HostAuthenticationFilter("Basic"));
    //        //app.UseBasicAuthentication(new BasicAuthenticationOptions("realm",
    //        //async (username, password) => await Authenticate(username, password)));
    //        //app.UseBasicAuthentication("realm", Authenticate);
    //        //config.Properties.TryAdd("AccessiController", "OK");
    //        app.UseWebApi(config);

    //        var physicalFileSystem = new PhysicalFileSystem(@"./www");
    //        var options = new FileServerOptions
    //        {
    //            EnableDefaultFiles = true,
    //            FileSystem = physicalFileSystem
    //        };
    //        options.StaticFileOptions.FileSystem = physicalFileSystem;
    //        options.StaticFileOptions.ServeUnknownFileTypes = true;
    //        options.DefaultFilesOptions.DefaultFileNames = new[]
    //        {
    //            "index.html"
    //        };
    //        app.UseFileServer(options);
    //    }
}