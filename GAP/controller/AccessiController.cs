﻿using GAP.controller;
using GAP.model;
using GAP.model.antenne;
using GAP.Model;
using GAP.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace GAP.Controller
{
    public class AccessiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Gates gates;
        private Antenne antenne;
        private AnagraficaImporter anagraficaImporter;
        private Aree aree;
        public Aree Aree { get => aree; }
        public IList<Antenna> Antenne { get => antenne; }
        public Gates Gates { get => gates; }
        public AntennaConfigurazione antennaConfigurazione;

        private MailWarning mailWarning;

        public async System.Threading.Tasks.Task StartAsync()
        {
            try
            {
                gates = new Gates();
                log.Info("Gates caricati");
                aree = new Aree(gates);
                log.Info("Aree caricate");
                antenne = new Antenne(aree);
                log.Info("Antenne caricate");
                antenne.Start();

                Configurazione configurazione = Configurazione.Carica();
                if (!String.IsNullOrEmpty(configurazione.AntennaConfigurazione))
                {
                    antennaConfigurazione = new AntennaConfigurazione(configurazione.AntennaConfigurazione);
                }
                antennaConfigurazione.Start();

                anagraficaImporter = new AnagraficaImporter();
                anagraficaImporter.Start(aree);
                log.Info("Anagrafica importer partito");
                mailWarning = new MailWarning();
                await mailWarning.Start(aree);
                log.Info("Mail scheduling partito...");
                log.Info("Controllo accessi avviato con successo.");
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        public AccessiController()
        {
        }

        internal void Stop()
        {
            mailWarning.Stop();
            if (anagraficaImporter != null)
            {
                anagraficaImporter.Stop();
            }
            foreach (Gate g in gates.Values)
            {
                g.Stop();
            }
            if (antenne != null)
            {
                antenne.Stop();
            }
        }
    }
}