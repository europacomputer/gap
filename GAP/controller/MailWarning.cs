﻿using GAP.model;
using GAP.Model;
using GAP.Util;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Threading.Tasks;

namespace GAP.controller
{
    public class MailJob : IJob
    {
        public async Task Execute(IJobExecutionContext context) => new MailSender().SendMail((Aree)context.MergedJobDataMap["aree"]);
    }

    public class MailWarning
    {
        private IScheduler scheduler;

        public MailWarning()
        {
        }

        public void Stop()
        {
            scheduler.Shutdown();
        }

        public async Task Start(Aree aree)
        {
            Console.WriteLine("Avvio mail sheduler...");

            NameValueCollection props = new NameValueCollection
                {
                    { "quartz.serializer.type", "binary" }
                };
            StdSchedulerFactory factory = new StdSchedulerFactory(props);
            scheduler = await factory.GetScheduler();

            await scheduler.Start();

            //Creo i trigger
            Configurazione configurazione = Configurazione.Carica();
            String[] orariSchedulazioniToken = configurazione.OrarioMail.Split(';');

            foreach (String orarioSchedulazione in orariSchedulazioniToken)
            {
                String[] orarioToken = orarioSchedulazione.Split(':');
                if (orarioToken.Length != 2)
                {
                    return;
                }

                if (!Int32.TryParse(orarioToken[0], out int ora))
                {
                    return;
                }
                if (!Int32.TryParse(orarioToken[1], out int minuti))
                {
                    return;
                }
                IJobDetail job = JobBuilder.Create<MailJob>().Build();
                job.JobDataMap.Put("aree", aree);
                String scheduleCron = String.Format("0 {0} {1} ? * *", minuti, ora);
                ITrigger trigger = TriggerBuilder.Create().WithIdentity("orario" + orarioSchedulazione, "spedizioneMail").StartNow().WithCronSchedule(scheduleCron).Build();
                Console.WriteLine("Configuro mail per le ore " + orarioSchedulazione);
                await scheduler.ScheduleJob(job, trigger);
            }
        }
    }
}