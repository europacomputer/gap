﻿using GAP.Dao;
using GAP.Model;
using System;

namespace GAP.Controller
{
    public enum Evento { ENTRATA, USCITA, GENERICO, ERRORE }

    public class AuditLogger

    {
        public void Error(String anomalia, String nota)
        {
            using (DB db = new DB())
            {
                db.ExecuteCommand(String.Format("insert into audit (Evento,Anomalia,Nota,Data) values ('{0}','{1}','{2}','{3}')", Evento.ERRORE, anomalia, nota, DateTime.Now.Ticks));
            }
        }

        public void Log(Area area, Tag tag, Evento evento, String anomalia, String nota, long data)
        {
            using (DB db = new DB())
            {
                db.ExecuteCommand(String.Format("insert into audit (Tag,Evento,Anomalia,Nota,Data,Area) values ('{0}','{1}','{2}','{3}',{4},'{5}')", tag.Codice, evento.ToString(), anomalia, nota, data, area.Nome));
            }
        }
    }
}