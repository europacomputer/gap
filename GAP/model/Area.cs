﻿using GAP.Controller;
using GAP.Dao;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SQLite;
using System.Linq;
using System.Runtime.Caching;

namespace GAP.Model
{
    public class Area
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string nome;
        private IReadOnlyDictionary<Tag, Anagrafica> anagrafiche;
        private Permessi permessi;
        private ConcurrentDictionary<Tag, Presenza> presenzeDict;
        private ConcurrentDictionary<String, Gate> gates;
        public String NomeAreaPrincipale { get; set; }
        public IList<Presenza> Presenze { get => presenzeDict.Values.OrderBy(p => p.Durata).ToList().AsReadOnly(); }
        public IList<Gate> Gates { get => gates.Values.ToList().AsReadOnly(); }
        public ConcurrentDictionary<String, Gate> GatesDict { get => gates; }

        [JsonIgnore]
        private MemoryCache cacheTagLetti;//Mantengo i tag letti nell'area, se leggo tag dall'antenna ma sono presenti in questa cache non li elaboro

        private IReadOnlyDictionary<String, Anagrafica> anagraficheByCodice;

        [JsonIgnore]
        public MemoryCache CacheTagLetti { get { return NomeAreaPrincipale == null ? cacheTagLetti : this.AreaPrincipale.CacheTagLetti; } }

        [JsonIgnore]
        public Area AreaPrincipale { get; set; }

        [JsonIgnore]
        public IList<Area> AreeSecondarie { get; set; }

        private readonly object syncLock = new object();

        public void AddGate(Gate gate)
        {
            gates.TryAdd(gate.Codice, gate);
        }

        public void RicaricaPermessi()
        {
            permessi.Carica();
        }

        [JsonIgnore]
        public IReadOnlyDictionary<Tag, Anagrafica> Anagrafiche
        {
            set
            {
                this.anagrafiche = value;
                CaricaPresenze();
            }
            get
            {
                return this.anagrafiche;
            }
        }

        [JsonIgnore]
        public IReadOnlyDictionary<String, Anagrafica> AnagraficheByCodice
        {
            set
            {
                this.anagraficheByCodice = value;
            }
            get
            {
                return this.anagraficheByCodice;
            }
        }

        public Area(int? id, String nome, bool caricaPresenze = true)
        {
            Id = id;
            this.nome = nome;
            this.permessi = new Permessi(nome);
            presenzeDict = new ConcurrentDictionary<Tag, Presenza>();
            gates = new ConcurrentDictionary<string, Gate>();
            AreeSecondarie = new List<Area>();
            NameValueCollection configCache = new NameValueCollection
            {
                { "pollingInterval", "00:00:03" }
            };
            cacheTagLetti = new MemoryCache(nome, configCache);
            if (caricaPresenze)
            {
                CaricaPresenze();
            }
        }

        private void AggiornaPresenze()
        {
            foreach (KeyValuePair<Tag, Presenza> p in presenzeDict)
            {
                anagrafiche.TryGetValue(p.Key, out Anagrafica anagrafica);
                p.Value.Anagrafica = anagrafica;
            }
        }

        private void CaricaPresenze()
        {
            Func<SQLiteDataReader, ConcurrentDictionary<Tag, Presenza>> fillResource = delegate (SQLiteDataReader reader)
            {
                ConcurrentDictionary<Tag, Presenza> result = new ConcurrentDictionary<Tag, Presenza>();
                while (reader.Read())
                {
                    Tag tag = new Tag(reader["Tag"].ToString())
                    {
                        Presente = false
                    };
                    Anagrafica anagrafica = null;
                    if (anagrafiche != null)
                    {
                        anagrafiche.TryGetValue(tag, out anagrafica);
                    }
                    Presenza p = new Presenza(tag, this.nome, anagrafica, long.Parse(reader["Data"].ToString()));
                    result.TryAdd(p.Tag, p);
                }
                return result;
            };
            using (DB db = new DB())
            {
                SQLiteParameter[] parameters = { new SQLiteParameter("area", this.nome) };
                presenzeDict = db.ExecuteSelect("select Tag,Data from Presenze where Area=@area", fillResource, parameters);
            }
        }

        public string Nome
        {
            get { return nome; }
        }

        [JsonIgnore]
        public ConcurrentDictionary<Tag, Presenza> PresenzeDict { get => presenzeDict; }

        public int? Id { get; }

        public Tag CheckPermessi(IList<Tag> tags)
        {
            lock (syncLock)
            {
                Tag tagAutorizzato = null;
                foreach (Tag tag in tags)
                {
                    if (permessi.IsAutorizzato(tag, tags))
                    {
                        tagAutorizzato = tag;
                        break;
                    }
                }

                if (tagAutorizzato == null)
                {
                    tags.Where(t => t.Elabora).ToList().ForEach(t => new AuditLogger().Log(this, t, Evento.ENTRATA, "non autorizzato", "", DateTime.Now.Ticks));
                }

                return tagAutorizzato;
            }
        }

        public void Entra(IList<Tag> tags, Tag tagAutorizzato = null, Area areaAzione = null, String nota = "", long time = 0)
        {
            long dataInserimento = time;
            if (time == 0)
            {
                dataInserimento = DateTime.Now.Ticks;
            }
            lock (syncLock)
            {
                foreach (Tag tag in tags)
                {
                    anagrafiche.TryGetValue(tag, out Anagrafica anagrafica);
                    if (presenzeDict.TryAdd(tag, new Presenza(tag, nome, anagrafica, dataInserimento)))
                    {
                        log.Debug("Aggiungo tag all'area " + Nome + " : " + tag.Codice);
                        using (DB db = new DB())
                        {
                            db.ExecuteCommand(String.Format("insert into presenze (tag,area,data) values ('{0}','{1}','{2}')", tag.Codice, nome, dataInserimento));

                            String anomalia = areaAzione == null ? "" : "AUTOMATICA DA AREA " + areaAzione.nome;
                            String notaDaInserire = nota;
                            if (tagAutorizzato != null && tagAutorizzato != tag)
                            {
                                notaDaInserire = "AUTORIZZATO DA" + tagAutorizzato.Codice;
                            }
                            new AuditLogger().Log(this, tag, Evento.ENTRATA, anomalia, notaDaInserire, dataInserimento);
                            if (AreaPrincipale != null)
                            {
                                AreaPrincipale.Entra(tags, tagAutorizzato, this, time: time);
                            }
                        }
                    }
                    else
                    {
                        log.Debug("Tag Presente  all'area " + Nome + " : " + tag.Codice);
                        if (tag.Elabora && areaAzione == null)
                        {
                            log.Debug("Aggiungo solo audit " + Nome + " : " + tag.Codice);
                            new AuditLogger().Log(this, tag, Evento.ENTRATA, "già presente", "", DateTime.Now.Ticks);
                        }
                    }
                }
            }
        }

        public void Esci(IList<Tag> tags, String nota = "", Boolean forzaPrincipale = true)
        {
            log.Debug("Esco tag all'area " + Nome);
            lock (syncLock)
            {
                foreach (Tag tag in tags.Where(t => t.Elabora))
                {
                    bool cancellata = presenzeDict.TryRemove(tag, out Presenza presenzaRimossa);
                    if (cancellata)
                    {
                        using (DB db = new DB())
                        {
                            db.ExecuteCommand("delete from presenze where tag='" + tag.Codice + "'");
                        }
                        if (presenzaRimossa.Anagrafica == null || presenzaRimossa.Anagrafica.RilevaInAudit)
                        {
                            new AuditLogger().Log(this, tag, Evento.USCITA, "", nota, DateTime.Now.Ticks);
                        }
                    }
                    else
                    {
                        String anomalia = "non presente";
                        if (AreaPrincipale != null && AreaPrincipale.PresenzeDict.ContainsKey(tag))
                        {
                            anomalia = "";
                        }
                        //recupero l'eventuale anagrafica

                        if (anagrafiche.TryGetValue(tag, out Anagrafica anagrafica))
                        {
                            if (anagrafica.RilevaInAudit)
                            {
                                new AuditLogger().Log(this, tag, Evento.USCITA, anomalia, nota, DateTime.Now.Ticks);
                            }
                        }
                        else
                        {
                            new AuditLogger().Log(this, tag, Evento.USCITA, anomalia, nota, DateTime.Now.Ticks);
                        }
                    }
                    //Se ho un'area base faccio uscire dall'area base e dalle secondarie
                    if (forzaPrincipale && !String.IsNullOrEmpty(NomeAreaPrincipale))
                    {
                        AreaPrincipale.Esci(new List<Tag>() { tag });
                        foreach (Area areaSecondaria in AreaPrincipale.AreeSecondarie)
                        {
                            if (areaSecondaria.PresenzeDict.ContainsKey(tag))
                            {
                                areaSecondaria.Esci(new List<Tag>() { tag }, "Forzata da area " + nome, false);
                            }
                        }
                    }
                    foreach (Area areaSecondaria in AreeSecondarie)
                    {
                        if (areaSecondaria.PresenzeDict.ContainsKey(tag))
                        {
                            areaSecondaria.Esci(new List<Tag>() { tag }, "Forzata da area " + nome, false);
                        }
                    }
                }
            }
        }

        public void ProssimitaEntra(IList<Tag> tags)
        {
            lock (syncLock)
            {
                tags.Except(presenzeDict.Keys).ToList().ForEach(tag =>
                {
                    Entra(tags);
                });
            }
        }

        public override bool Equals(object obj)
        {
            var area = obj as Area;
            return area != null &&
                   Id == area.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }
    }
}