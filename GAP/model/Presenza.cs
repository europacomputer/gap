﻿using System;

namespace GAP.Model
{
    public class Presenza
    {
        public Presenza(Tag Tag, String NomeArea, Anagrafica anagrafica, long data)
        {
            this.Tag = Tag;
            this.NomeArea = NomeArea;
            this.Anagrafica = anagrafica;
            Data = data;
        }

        public double Durata { get { return Math.Round((DateTime.Now - DataAzione).TotalMinutes, 0); } }
        public Tag Tag { get; }
        public string NomeArea { get; }
        public Anagrafica Anagrafica { get; set; }

        public string Descrizione
        {
            get
            {
                if (this.Anagrafica == null)
                {
                    if (Tag.Codice.Length > 9)
                    {
                        return this.Tag.Codice.Substring(0, 10) + " " + this.Tag.Codice.Substring(10);
                    }
                    else return Tag.Codice;
                }
                return this.Anagrafica.Denominazione;
            }
        }

        public string Codice
        {
            get
            {
                if (this.Anagrafica == null)
                {
                    if (Tag.Codice.Length > 9)
                    {
                        return this.Tag.Codice.Substring(0, 10) + " " + this.Tag.Codice.Substring(10);
                    }
                    else return Tag.Codice;
                }
                return this.Anagrafica.Codice;
            }
        }

        public DateTime DataAzione { get { return new DateTime(this.Data); } }
        public long Data { get; set; }

        public override bool Equals(object other)
        {
            if (other == null) { return false; }
            var obj = other as Presenza;
            if (obj == null) { return false; }
            return NomeArea == obj.NomeArea && Tag == obj.Tag;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = (int)2166136261;
                hash = hash * 16777619 ^ Tag.GetHashCode();
                hash = hash * 16777619 ^ NomeArea.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}