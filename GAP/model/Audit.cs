﻿using System;

namespace GAP.Model
{
    public class Audit
    {
        public int Id { get; set; }
        public String Tag { get; set; }
        public String Evento { get; set; }
        public String Anomalia { get; set; }
        public String Denominazione { get; set; }
        public String Nota { get; set; }
        public long Data { get; set; }
        public String Area { get; set; }
        public DateTime DataAzione { get { return new DateTime(this.Data); } }
    }
}