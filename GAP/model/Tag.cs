﻿using Newtonsoft.Json;

namespace GAP.Model
{
    public class Tag
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string Codice { get; }

        [JsonIgnore]
        public bool Elabora { get; set; }

        public bool Presente { get; set; }

        [JsonIgnore]
        public string Antenna { get; set; }

        public static bool ValidaPattern(string tag)
        {
            bool result = (tag.Length == 24 || tag.Length == 16) && tag.StartsWith("E");
            if (!result)
            {
                log.Info("Non considero il tag " + tag + " per mancanza di math con il pattern E24|16)");
            }
            return result;
        }

        public Tag(string codice, bool elabora = false)
        {
            Codice = string.IsNullOrEmpty(codice) ? "" : codice.TrimStart('0');
            Elabora = elabora;
            Presente = true;
            Antenna = "";
        }

        public override string ToString()
        {
            return Codice;
        }

        public override bool Equals(object other)
        {
            if (other == null) { return false; }
            var obj = other as Tag;
            if (obj == null) { return false; }
            return Codice == obj.Codice;
        }

        public override int GetHashCode()
        {
            return Codice.GetHashCode();
        }
    }
}