﻿using System;

namespace GAP.Model
{
    public class Regola
    {
        public int? Id { get; set; }
        public Tag Tag1 { get; set; }
        public Tag Tag2 { get; set; }
        public string Periodo { get; set; }
        public string Area { get; set; }
        public string AnagraficaTag1 { get; set; }

        public override bool Equals(object obj)
        {
            var regola = obj as Regola;
            return regola != null &&
                   Id == regola.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }

        public Boolean PeriodoValido
        {
            get
            {
                if (String.IsNullOrEmpty(Periodo))
                {
                    return true;
                }
                try
                {
                    String[] tokenPeriodo = Periodo.Split('-');
                    String[] spanInizioToken = tokenPeriodo[0].Split(':');
                    TimeSpan spanInizio = new TimeSpan(Int32.Parse(spanInizioToken[0]), Int32.Parse(spanInizioToken[1]), 0);
                    DateTime inizio = DateTime.Today + spanInizio;

                    String[] spanFineToken = tokenPeriodo[1].Split(':');
                    TimeSpan spanFine = new TimeSpan(Int32.Parse(spanFineToken[0]), Int32.Parse(spanFineToken[1]), 0);
                    DateTime fine = DateTime.Today + spanFine;

                    DateTime now = DateTime.Now;
                    return now >= inizio && now <= fine;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    return false;
                }
            }
        }
    }
}