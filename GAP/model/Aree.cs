﻿using GAP.Dao;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Linq;

namespace GAP.Model
{
    public class Aree : Dictionary<String, Area>
    {
        private ReadOnlyDictionary<Tag, Anagrafica> anagrafiche;
        private ReadOnlyDictionary<String, Anagrafica> anagraficheByCodice;
        private readonly Gates gates;

        private Func<SQLiteDataReader, Boolean> CaricaAree(Gates gates)
        {
            return delegate (SQLiteDataReader reader)
            {
                while (reader.Read())
                {
                    AggiungiArea(Convert.ToInt32(reader["Id"]), reader["nome"].ToString(), Convert.ToString(reader["Principale"]), Convert.ToString(reader["Gate"]), gates);
                }
                return true;
            };
        }

        public IList<Area> ListaAree()
        {
            return Values.ToList();
        }

        public Aree(Gates gates)
        {
            CaricaAnagrafiche();
            using (DB db = new DB())
            {
                db.ExecuteSelect("select a.Id,a.Nome,a.Principale,an.Gate from aree a left join Antenne an on an.Area=a.Nome order by a.Principale", CaricaAree(gates));
            }
            CaricaPermessi();
            this.gates = gates;
        }

        public void CaricaPermessi()
        {
            foreach (Area area in Values)
            {
                area.RicaricaPermessi();
            }
        }

        public void AggiornaAnagrafiche()
        {
            CaricaAnagrafiche();
            foreach (Area area in Values)
            {
                area.Anagrafiche = anagrafiche;
            }
        }

        public void CaricaAnagrafiche()
        {
            using (DB db = new DB())
            {
                anagrafiche = new ReadOnlyDictionary<Tag, Anagrafica>(db.ExecuteSelect("select Codice,Denominazione,Tag,RilevaInAudit,Pedonale,CodiceCollegato from Anagrafiche", Anagrafica.CaricaMethod()));
            }

            Dictionary<string, Anagrafica> anagraficheByCodiceTmp = new Dictionary<String, Anagrafica>();
            foreach (KeyValuePair<Tag, Anagrafica> entry in anagrafiche)
            {
                anagraficheByCodiceTmp.Add(entry.Value.Codice, entry.Value);
            }
            anagraficheByCodice = new ReadOnlyDictionary<string, Anagrafica>(anagraficheByCodiceTmp);
        }

        public String CaricaPresenzeHtml()
        {
            List<Area> areeOrdinate = ListaAree().OrderBy(a => a.Id).ToList();
            string content = "<H3>PRESENZE AL " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + "</H3>";
            content += "<div class='row'>";
            areeOrdinate.Where(a => a.NomeAreaPrincipale == null).ToList().ForEach(area =>
            {
                content += "<div>";
                content += "<table class='table'>";
                content += "<thead>";
                content += "<tr>";
                content += "<th class='left'>" + area.Nome;
                content +=
                    "<span> ( " + area.Presenze.Count + " ) </span>";
                content += "</th>";
                content += "</tr>";
                content += "</thead>";
                content += "<tbody>";
                area.Presenze.OrderBy(p => p.Codice).ToList().ForEach(presenza =>
                {
                    content += "<tr>";
                    content += "<td style='' class='left'>";
                    content +=
                        presenza.Anagrafica == null
                            ? presenza.Tag.Codice
                            : presenza.Anagrafica.Codice +
                              " - " +
                              presenza.Descrizione;
                    content += "</td>";
                    content += "<td class='left'>" + presenza.DataAzione.ToShortDateString() + " " + presenza.DataAzione.ToLongTimeString();
                    content += "</td>";
                    content += "</tr>";
                });
                content += "</tbody></table></div><HR>";
            });
            content += "</div>";
            return content;
        }

        private void AggiungiArea(Int32 id, string codiceArea, String principale, String codiceGate, Gates gates)
        {
            if (!ContainsKey(codiceArea))
            {
                Area area = new Area(id, codiceArea)
                {
                    Anagrafiche = anagrafiche,
                    AnagraficheByCodice = anagraficheByCodice
                };
                if (!String.IsNullOrEmpty(principale))
                {
                    area.AreaPrincipale = this[principale];
                    area.NomeAreaPrincipale = principale;
                    this[principale].AreeSecondarie.Add(area);
                }
                Add(key: codiceArea, value: area);
            }
            if (TryGetValue(codiceArea, out Area areaAggiunta))
            {
                if (!String.IsNullOrEmpty(codiceGate) && gates.ContainsKey(codiceGate))
                {
                    gates.TryGetValue(codiceGate, out Gate gate);
                    areaAggiunta.AddGate(gate);
                }
            }
        }
    }
}