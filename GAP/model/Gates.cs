﻿using GAP.Dao;
using GAP.Model;
using System;
using System.Collections.Concurrent;
using System.Data.SQLite;


namespace GAP.Model
{
    public class Gates : ConcurrentDictionary<string, Gate>
    {
        private Func<SQLiteDataReader, Boolean> CaricaMethod()
        {
            return delegate (SQLiteDataReader reader)
            {
                while (reader.Read())
                {
                    Gate gate = new Gate(Convert.ToInt32(reader["id"]),reader["codice"].ToString(), reader["nome"].ToString(), reader["ip"].ToString(), reader["porta"].ToString(), reader["x"].ToString(), reader["y"].ToString());
                    TryAdd(gate.Codice, gate);
                    gate.StartCheck();
                }
                return true;
            };
        }

        public Gates()
        {
            using (DB db = new DB())
            {
                db.ExecuteSelect("select * from gates", CaricaMethod());
            }
        }
    }
}