﻿using GAP.Dao;
using System;
using System.Data.SQLite;

namespace GAP.model
{
    public class Configurazione
    {
        public Int32 MinutiWarning { get; set; }
        public String MailAddress { get; set; }
        public String OrarioMail { get; set; }
        public String ServerMail { get; set; }
        public String UserMail { get; set; }
        public String PasswordMail { get; set; }
        public String PortMail { get; set; }
        public String From { get; set; }
        public String AntennaConfigurazione { get; set; }

        public static Configurazione Carica()
        {
            Func<SQLiteDataReader, Configurazione> fillResource = delegate (SQLiteDataReader reader)
            {
                reader.Read();
                return new Configurazione()
                {
                    MinutiWarning = Convert.ToInt32(reader["MinutiWarning"]),
                    OrarioMail = Convert.ToString(reader["OrarioMail"]),
                    MailAddress = Convert.ToString(reader["MailAddress"]),
                    ServerMail = Convert.ToString(reader["ServerMail"]),
                    UserMail = Convert.ToString(reader["UserMail"]),
                    PasswordMail = Convert.ToString(reader["PasswordMail"]),
                    PortMail = Convert.ToString(reader["PortMail"]),
                    From = Convert.ToString(reader["From"]),
                    AntennaConfigurazione = Convert.ToString(reader["AntennaConfigurazione"])
                };
            };
            using (DB db = new DB())
            {
                return db.ExecuteSelect("select * from Configurazione", fillResource);
            }
        }
    }
}