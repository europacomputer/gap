﻿using GAP.Controller;
using GAP.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Runtime.Caching;
using System.Text;
using System.Threading;

namespace GAP.model
{
    public class RilevatoreEventArgs : EventArgs
    {
        public RilevatoreEventArgs(IList<Tag> tags)
        {
            Tags = tags;
        }

        public IList<Tag> Tags { get; }
    }

    public class Rilevatore
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly string configurazione;
        private readonly string codiceAntenna;
        private MemoryCache cacheDaValidare;
        private double tempoCheck = 20;
        private Thread rilevatoreThread;

        public event EventHandler<RilevatoreEventArgs> RaiseRilevatoreEvent;

        public Rilevatore(String configurazione, String codiceAntenna)
        {
            string[] confToken = configurazione.Split(':');
            Ip = confToken[0];
            Porta = confToken[1];
            NameValueCollection configCache = new NameValueCollection
            {
                { "pollingInterval", "00:00:03" }
            };
            cacheDaValidare = new MemoryCache(codiceAntenna + configurazione, configCache);
            if (confToken.Length > 2)
            {
                double.TryParse(confToken[2], out tempoCheck);
            }
            this.configurazione = configurazione;
            this.codiceAntenna = codiceAntenna;
        }

        private void AvviaRilevazione()
        {
            {
                Boolean letto = false;
                do
                {
                    letto = VerificaStato();
                    if (letto)
                    {
                        log.Info("Rilevata presenza sul rilevatore:" + configurazione);
                        EventHandler<RilevatoreEventArgs> handler = RaiseRilevatoreEvent;
                        IList<Tag> tags = new List<Tag>();
                        StringBuilder sb = new StringBuilder("Cache rilevatore ").Append(configurazione).Append(":");
                        foreach (KeyValuePair<String, Object> item in cacheDaValidare)
                        {
                            Tag tag = (Tag)item.Value;
                            tag.Elabora = true;
                            tags.Add(tag);
                            sb.Append(tag.Codice).Append("#");
                        }
                        log.Info(sb.ToString());
                        handler(this, new RilevatoreEventArgs(tags));
                        cacheDaValidare.Dispose();
                        cacheDaValidare = new MemoryCache(codiceAntenna + configurazione);
                    }
                    else
                    {
                        Thread.Sleep(500);
                    }
                } while (cacheDaValidare.GetCount() > 0 && !letto);
                log.Info("Stop verifica sul rilevatore:" + configurazione);
            }
        }

        public void Valida(IList<Tag> tags)
        {
            if (tags.Count == 0)
            {
                log.Info("Il rilevatore " + Ip + ":" + Porta + " non ha tag da validare");
                return;
            }
            foreach (Tag tag in tags)
            {
                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy
                {
                    SlidingExpiration = new TimeSpan(0, 0, Convert.ToInt32(tempoCheck)),
                    //AbsoluteExpiration = DateTime.Now.AddSeconds(tempoCheck),
                    RemovedCallback = new CacheEntryRemovedCallback(CacheRemovedCallback),
                };
                cacheDaValidare.Set(tag.Codice, tag, cacheItemPolicy);
            }
            if (rilevatoreThread == null || !rilevatoreThread.IsAlive)
            {
                rilevatoreThread = new Thread(new ThreadStart(AvviaRilevazione));
                rilevatoreThread.Start();
                log.Info("Avvio verifica presenza su rilevatore " + Ip + ":" + Porta + " per " + tempoCheck + " sec");
            }
            else
            {
                log.Info("Aggiungo presenza su rilevatore già attivo" + Ip + ":" + Porta + " per " + tempoCheck + " sec");
            }
            StringBuilder sb = new StringBuilder("Cache rilevatore ").Append(configurazione).Append(":");
            foreach (KeyValuePair<String, Object> item in cacheDaValidare)
            {
                Tag tag = (Tag)item.Value;
                tag.Elabora = true;
                tags.Add(tag);
                sb.Append(tag.Codice).Append("#");
            }
            log.Info(sb.ToString());
        }

        protected virtual void CacheRemovedCallback(CacheEntryRemovedArguments arguments)
        {
            if (arguments.RemovedReason == CacheEntryRemovedReason.Expired)
            {
                log.Debug("Rimosso un tag dalla cache del rilevatore " + Ip + ":" + Porta + ":" + (Tag)arguments.CacheItem.Value);
            }
        }

        public string Ip { get; }
        public string Porta { get; }
        public string TempoCheck { get; set; }

        public Boolean VerificaStato()
        {
            Boolean result = false;
            WebClient client = new WebClient
            {
                Credentials = new NetworkCredential("root", "europa")
            };
            try
            {
                String[] state_result = client.DownloadString("http://" + Ip + "/axis-cgi/io/input.cgi?check=" + Porta).Split('=');
                if (state_result.Length > 1)
                {
                    result = state_result[1].Equals("1\n");
                }
            }
            catch (Exception ex)
            {
                new AuditLogger().Error("Errore in verifica rilevatore " + Ip + ":" + Porta, ex.Message.Replace("'", "''"));
            }
            return result;
        }

        public void Stop()
        {
            cacheDaValidare = new MemoryCache(codiceAntenna + configurazione);
        }
    }
}