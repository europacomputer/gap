﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GAP.model
{
    public class TagConfigReaderWriter
    {
        private string epc;
        private string tid;
        private string message;

        public TagConfigReaderWriter(string text)
        {
            epc = "";
            tid = "";
            if (!String.IsNullOrEmpty(text))
            {
                if (text.Equals("Error"))
                {
                    message = "Errore nel leggere il tag";
                }
                else if (text.Contains("#"))
                {
                    message = "Lettura di tag multipli. Appoggiare solamente un tag";
                    epc = text;
                }
                else if (String.IsNullOrEmpty(text))
                {
                    message = "Nessun tag rilevato";
                }
                else if (!text.Contains(":")) { message = text; }
                else
                {
                    string[] dati = text.Split(':');
                    Epc = dati[0];
                    Tid = dati[1];
                }
            }
        }

        public string Message { get => message; set => message = value; }
        public string Tid { get => tid; set => tid = value; }
        public string Epc { get => epc; set => epc = value; }
    }
}