﻿using GAP.Dao;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;

namespace GAP.Model
{
    public class Permessi
    {
        private List<Regola> regole;
        private Dictionary<Tag, List<Regola>> regoleDict;
        private string nomeArea;
        private readonly object syncLock = new object();

        public Permessi(string nomeArea)
        {
            this.nomeArea = nomeArea;
            Carica();
        }

        public void Carica()
        {
            lock (syncLock)
            {
                Func<SQLiteDataReader, List<Regola>> fillResource = delegate (SQLiteDataReader reader)
                {
                    List<Regola> result = new List<Regola>();
                    while (reader.Read())
                    {
                        Regola r = new Regola
                        {
                            Tag1 = new Tag(reader["tag1"].ToString()),
                            Tag2 = new Tag(reader["tag2"].ToString()),
                            Periodo = reader["periodo"].ToString(),
                            Area = reader["area"].ToString(),
                            Id = int.Parse(reader["id"].ToString())
                        };
                        result.Add(r);
                    }
                    return result;
                };
                using (DB db = new DB())
                {
                    regole = db.ExecuteSelect("select * from Regole r where area='" + this.nomeArea + "'", fillResource);
                }
                regoleDict = new Dictionary<Tag, List<Regola>>();
                foreach (Regola r in regole)
                {
                    if (!regoleDict.ContainsKey(r.Tag1))
                    {
                        regoleDict.Add(r.Tag1, new List<Regola>());
                    }
                    regoleDict[r.Tag1].Add(r);
                }
            }
        }

        public IList<Regola> Regole
        {
            get { return regole.AsReadOnly(); }
        }

        public Boolean IsAutorizzato(Tag tagToAuthorize, IList<Tag> tags)
        {
            if (!regoleDict.ContainsKey(tagToAuthorize))
            {
                return false;
            }
            //cerco la regola ALL
            List<Regola> regole = regoleDict[tagToAuthorize];
            foreach (Regola r in regole)
            {
                if (String.IsNullOrEmpty(r.Tag2.Codice) && r.PeriodoValido)
                {
                    return true;
                }
            }
            List<Tag> tagAbilitati = regole.Where(r => r.PeriodoValido).ToList().ConvertAll(r => r.Tag2);
            List<Tag> abilitati = tagAbilitati.Intersect(tags).ToList();
            return tagAbilitati.Intersect(tags).ToList().Count > 0;
        }
    }
}