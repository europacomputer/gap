﻿using GAP.Controller;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;

namespace GAP.Model
{
    public class Gate
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Int16 stato;
        private Thread threadCheck;
        private bool checkGate;

        public Gate(int id, string codice, string nome, string ip, string porta, string x, string y)
        {
            Id = id;
            Codice = codice;
            Nome = nome;
            Ip = ip;
            Porta = porta;
            X = x;
            Y = y;
        }

        public void StartCheck()
        {
            checkGate = true;
            threadCheck = new Thread(CheckStato);
            threadCheck.Start();
        }

        private void ApriCall()
        {
            WebClient client = new WebClient
            {
                Credentials = new NetworkCredential("root", "europa")
            };
            try
            {
                log.Debug("Apro il cancello " + Nome);
                client.DownloadString("http://" + Ip + "/axis-cgi/io/output.cgi?action=" + Porta + ":/500\\");
            }
            catch (Exception ex)
            {
                log.Error("Errore in apertura gate " + Nome, ex);
            }
        }

        private void CheckStato()
        {
            while (checkGate)
            {
                try
                {
                    stato = 1;
                    Ping pingSender = new Ping();
                    IPAddress address = IPAddress.Parse(Ip);
                    PingReply reply = pingSender.Send(address);
                    if (reply.Status == IPStatus.Success)
                    {
                        stato = 0;
                    }
                }
                catch (Exception ex)
                {
                    log.Error("errore nel pingare il gate con ip " + Ip, ex);
                }
                Thread.Sleep(10000);
            }
        }

        public void Apri()
        {
            Thread t = new Thread(ApriCall);
            t.Start();
        }

        public int Id { get; }
        public string Codice { get; }

        internal void Stop()
        {
            checkGate = false;
        }

        public string Nome { get; }
        public string Ip { get; }
        public string Porta { get; }
        public string X { get; }
        public string Y { get; }
        public Int16 Stato { get => stato; }
    }
}