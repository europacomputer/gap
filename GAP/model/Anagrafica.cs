﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace GAP.Model
{
    public class Anagrafica
    {
        public static Func<SQLiteDataReader, Dictionary<Tag, Anagrafica>> CaricaMethod()
        {
            return delegate (SQLiteDataReader reader)
            {             
                Dictionary <Tag, Anagrafica> result = new Dictionary<Tag, Anagrafica>();
                while (reader.Read())
                {
                    if (!String.IsNullOrEmpty(Convert.ToString(reader["Tag"])))
                    {
                        try
                        {             
                            Anagrafica an = new Anagrafica(reader["Codice"].ToString(), reader["Denominazione"].ToString(), reader["Tag"].ToString(), Convert.ToBoolean(reader["RilevaInAudit"]), Convert.ToBoolean(reader["Pedonale"]), Convert.ToString(reader["codiceCollegato"]), "");
                            Tag tag = new Tag(an.Tag);
                            if (!result.ContainsKey(tag))
                            {
                                result.Add(new Tag(an.Tag), an);
                            }
                        }
                        catch ( Exception ex)
                        {
                            Console.WriteLine(ex);
                            throw ex;
                        }
                    }
                }
                return result;
            };
        }

        public static Func<SQLiteDataReader, List<Anagrafica>> CaricaListaMethod()
        {
            return delegate (SQLiteDataReader reader)
            {
                List<Anagrafica> result = new List<Anagrafica>();
                while (reader.Read())
                {
                    Anagrafica an = new Anagrafica(reader["Codice"].ToString(), reader["Denominazione"].ToString(), reader["Tag"].ToString(), Convert.ToBoolean(reader["RilevaInAudit"]), Convert.ToBoolean(reader["Pedonale"]), Convert.ToString(reader["codiceCollegato"]), "");
                    result.Add(an);
                }
                return result;
            };
        }

        public static Func<SQLiteDataReader, bool> AnagraficaExistMethod(string codiceAnagrafica)
        {
            return delegate (SQLiteDataReader reader)
            {
                return reader.HasRows;
            };
        }

        private string codice;
        private string denominazione;
        private string tag;
        private bool locale;

        public Anagrafica(string codice, string denominazione, string tag, Boolean rilevaInAudit, Boolean pedonale, string codiceCollegato, string areeAbilitate)
        {
            this.codice = codice;
            this.denominazione = denominazione;
            this.tag = tag;
            RilevaInAudit = rilevaInAudit;
            Pedonale = pedonale;
            this.CodiceCollegato = codiceCollegato;
            this.AreeAbilitate = areeAbilitate;
        }

        public string Codice { get => codice; }
        public string Denominazione { get => denominazione; }
        public string Tag { get => tag; }
        public bool RilevaInAudit { get; }
        public bool Pedonale { get; }
        public String CodiceCollegato { get; }
        public String AreeAbilitate { get; }
    }
}