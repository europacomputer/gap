﻿using GAP.Dao;
using GAP.model.antenne;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading;

namespace GAP.Model
{
    public class Antenne : List<Model.Antenna>
    {
        private Dictionary<String, DeviceReader> readers = new Dictionary<string, DeviceReader>();
        public Dictionary<String, Thread> devicesThread = new Dictionary<string, Thread>();

        private Func<SQLiteDataReader, List<Model.Antenna>> CaricaMethod(Aree aree)
        {
            return delegate (SQLiteDataReader reader)
            {
                List<Model.Antenna> result = new List<Model.Antenna>();
                while (reader.Read())
                {
                    if (aree.ContainsKey(reader["area"].ToString()))
                    {
                        Enum.TryParse(reader["Tipo"].ToString(), out Model.Antenna.TipoFunzionamento tipoFunzionamento);
                        Antenna antenna = null;
                        switch (tipoFunzionamento)
                        {
                            case Antenna.TipoFunzionamento.I: antenna = new AntennaIngresso(Convert.ToInt32(reader["numero"]), Convert.ToString(reader["codice"]), Convert.ToString(reader["gate"]), aree[Convert.ToString(reader["Area"])], tipoFunzionamento, Convert.ToInt32(reader["Prolunga_presenza"]), Convert.ToString(reader["x"]), Convert.ToString(reader["y"]), true, Convert.ToString(reader["configurazioneRilevatoreIngresso"]), Convert.ToString(reader["configurazioneRilevatoreUscita"]), Convert.ToBoolean(reader["Pedonale"])); break;
                            case Antenna.TipoFunzionamento.U: antenna = new AntennaUscita(Convert.ToInt32(reader["numero"]), Convert.ToString(reader["codice"]), Convert.ToString(reader["gate"]), aree[Convert.ToString(reader["Area"])], tipoFunzionamento, Convert.ToInt32(reader["Prolunga_presenza"]), Convert.ToString(reader["x"]), Convert.ToString(reader["y"]), true, Convert.ToString(reader["configurazioneRilevatoreIngresso"]), Convert.ToString(reader["configurazioneRilevatoreUscita"]), Convert.ToBoolean(reader["Pedonale"])); break;
                            case Antenna.TipoFunzionamento.P: antenna = new AntennaProssimita(Convert.ToInt32(reader["numero"]), Convert.ToString(reader["codice"]), Convert.ToString(reader["gate"]), aree[Convert.ToString(reader["Area"])], tipoFunzionamento, Convert.ToInt32(reader["Prolunga_presenza"]), Convert.ToString(reader["x"]), Convert.ToString(reader["y"]), true, Convert.ToString(reader["configurazioneRilevatoreIngresso"]), Convert.ToString(reader["configurazioneRilevatoreUscita"]), Convert.ToBoolean(reader["Pedonale"])); break;
                            case Antenna.TipoFunzionamento.R: antenna = new AntennaRilevatore(Convert.ToInt32(reader["numero"]), Convert.ToString(reader["codice"]), Convert.ToString(reader["gate"]), aree[Convert.ToString(reader["Area"])], tipoFunzionamento, Convert.ToInt32(reader["Prolunga_presenza"]), Convert.ToString(reader["x"]), Convert.ToString(reader["y"]), true, Convert.ToString(reader["configurazioneRilevatoreIngresso"]), Convert.ToString(reader["configurazioneRilevatoreUscita"]), Convert.ToBoolean(reader["Pedonale"])); break;
                            case Antenna.TipoFunzionamento.IU: antenna = new AntennaIngressoUscita(Convert.ToInt32(reader["numero"]), Convert.ToString(reader["codice"]), Convert.ToString(reader["gate"]), aree[Convert.ToString(reader["Area"])], tipoFunzionamento, Convert.ToInt32(reader["Prolunga_presenza"]), Convert.ToString(reader["x"]), Convert.ToString(reader["y"]), true, Convert.ToString(reader["configurazioneRilevatoreIngresso"]), Convert.ToString(reader["configurazioneRilevatoreUscita"]), Convert.ToBoolean(reader["Pedonale"])); break;
                            default:
                                break;
                        }
                        Add(antenna);
                        //Verifico se l'Ip contiene un numero dell'antenna (xxx.xxx.xxx.xxx:Ant);
                        String ipConfigurato = Convert.ToString(reader["ip"]);
                        String ip = ipConfigurato;
                        int porta = 0;
                        if (ip.Contains(":") && !ip.StartsWith("f"))
                        {
                            ip = ipConfigurato.Substring(0, ipConfigurato.Length - 2);
                            porta = Convert.ToInt16(ipConfigurato.Substring(ipConfigurato.Length - 1, 1));
                        }

                        if (readers.TryGetValue(ip, out DeviceReader deviceReader))
                        {
                            deviceReader.AddAntenna(porta, antenna);
                        }
                        else
                        {
                            DeviceReader nuovoDeviceReader = new DeviceReader(ip);
                            readers.Add(ip, nuovoDeviceReader);
                            nuovoDeviceReader.AddAntenna(porta, antenna);
                        }
                    }
                }
                return result;
            };
        }

        public void Start()
        {
            foreach (DeviceReader deviceReader in readers.Values)
            {
                Thread t1 = new Thread(() => deviceReader.Start());
                t1.Start();
                devicesThread.Add(deviceReader.Ip, t1);
            }
        }

        public void Stop()
        {
            foreach (DeviceReader deviceReader in readers.Values)
            {
                deviceReader.Stop();
            }
        }

        public Antenne(Aree aree)
        {
            using (DB db = new DB())
            {
                db.ExecuteSelect("select * from Antenne where Abilitata=1", CaricaMethod(aree));
            }
        }
    }
}