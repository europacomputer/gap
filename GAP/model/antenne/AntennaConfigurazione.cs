﻿using System;
using System.Diagnostics;

namespace GAP.model.antenne
{
    public class AntennaConfigurazione
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AntennaConfigurazione(string ip)
        {
            Ip = ip;
        }

        private Process process;

        public string Ip { get; }

        public void Scrivi(string epc)
        {
            try
            {
                process.StandardInput.WriteLine(epc);
            }
            catch (Exception ex)
            {
                log.Error("Errore nello scriver l'epc " + epc, ex);
                throw ex;
            }
        }

        public string Leggi()
        {
            try
            {
                process.StandardInput.WriteLine("leggi");
                return process.StandardOutput.ReadLine();
            }
            catch (Exception ex)
            {
                log.Error("Errore nel leggere dall'antennza di conf", ex);
                throw ex;
            }
        }

        public void Start()
        {
            try
            {
                process = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = @"GAPReader.exe",
                        Arguments = "c:" + Ip,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        RedirectStandardInput = true
                    }
                };
                process.Start();
                log.Info("Lettore configurazione" + Ip + " partito");
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        internal void Restart()
        {
            try
            {
                process.Kill();            
            }
            catch (Exception ex)
            {
                log.Error("Errore nel far ripartire l'antenna di conf.", ex);
                //throw ex;
            }
            Start();
        }
    }
}