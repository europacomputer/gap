﻿using GAP.Model;
using System.Collections.Generic;
using System.Runtime.Caching;

namespace GAP.model.antenne
{
    internal class AntennaRilevatore : Antenna
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AntennaRilevatore(int numero, string nome, string codiceGate, Area area, TipoFunzionamento tipo, double prolungaPresenza, string x, string y, bool abilitata, string configurazioneRilevatoreIngresso, string configurazioneRilevatoreUscita, bool pedonale) : base(numero, nome, codiceGate, area, tipo, prolungaPresenza, x, y, abilitata, configurazioneRilevatoreIngresso, configurazioneRilevatoreUscita, pedonale)
        {
            if (rilevatoreUscita != null)
            {
                rilevatoreUscita.RaiseRilevatoreEvent += HandleRilevatoreUscita;
            }
            if (rilevatoreIngresso != null)
            {
                rilevatoreIngresso.RaiseRilevatoreEvent += HandleRilevatoreIngresso;
            }
        }

        private bool ricercaDirezione = false;
        private Tag tagAbilitato;

        protected override void EseguiAzione(IList<Tag> tags)
        {
            log.Debug("Attendo verifica verso del varco");
            ricercaDirezione = true;
            rilevatoreIngresso.Valida(tags);
            rilevatoreUscita.Valida(tags);
        }

        private void HandleRilevatoreUscita(object sender, RilevatoreEventArgs e)
        {
            if (ricercaDirezione)
            {//Sto uscendo
                log.Debug("Direzione uscita...");
                ricercaDirezione = false;
                if (Area.GatesDict.ContainsKey(CodiceGate)) { Area.GatesDict[CodiceGate].Apri(); };
                if (rilevatoreIngresso == null)
                {
                    log.Debug("Esco...");
                    Area.Esci(e.Tags);
                }
            }
            else
            {
                log.Debug("Presenza rilevata...entro");
                Area.Entra(e.Tags, tagAbilitato);
                tagAbilitato = null;
            }
        }

        private void HandleRilevatoreIngresso(object sender, RilevatoreEventArgs e)
        {
            if (ricercaDirezione)
            {
                ricercaDirezione = false;
                log.Debug("Entrata...");
                tagAbilitato = Area.CheckPermessi(e.Tags);

                if (tagAbilitato == null)
                {
                    log.Debug("Permesso negato");
                    if (rilevatoreUscita != null)
                    {
                        rilevatoreUscita.Stop();
                    }
                    return;
                }
                else
                {
                    if (Area.GatesDict.ContainsKey(CodiceGate)) { Area.GatesDict[CodiceGate].Apri(); };
                    if (rilevatoreUscita == null)
                    {
                        log.Debug("Entro");
                        Area.Entra(e.Tags, tagAbilitato);
                    }
                }
            }
            else
            {
                log.Debug("Presenza rilevata...esco");
                Area.Esci(e.Tags);
            }
        }
    }
}