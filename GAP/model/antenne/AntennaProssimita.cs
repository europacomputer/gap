﻿using GAP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace GAP.model.antenne
{
    internal class AntennaProssimita : Antenna
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AntennaProssimita(int numero, string nome, string codiceGate, Area area, TipoFunzionamento tipo, double prolungaPresenza, string x, string y, bool abilitata, string configurazioneRilevatoreIngresso, string configurazioneRilevatoreUscita, bool pedonale) : base(numero, nome, codiceGate, area, tipo, prolungaPresenza, x, y, abilitata, configurazioneRilevatoreIngresso, configurazioneRilevatoreUscita, pedonale)
        {
        }

        protected override void EseguiAzione(IList<Tag> tags)
        {
            Area.ProssimitaEntra(tags);
        }

        protected override void CacheRemovedCallback(CacheEntryRemovedArguments arguments)
        {
            log.Debug("Rimosso un tag dalla cache :" + (Tag)arguments.CacheItem.Value);
            if (arguments.RemovedReason == CacheEntryRemovedReason.Expired)
            {
                Tag tag = (Tag)arguments.CacheItem.Value;
                tag.Elabora = true;
                Area.Esci(new List<Tag>() { tag });
            }
        }
    }
}