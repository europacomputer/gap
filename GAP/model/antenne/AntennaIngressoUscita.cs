﻿using GAP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GAP.model.antenne
{
    internal class AntennaIngressoUscita : Antenna
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string configurazioneRilevatoreIngresso;
        private string configurazioneRilevatoreUscita;

        public AntennaIngressoUscita(int numero, string nome, string codiceGate, Area area, TipoFunzionamento tipo, double prolungaPresenza, string x, string y, bool abilitata, string configurazioneRilevatoreIngresso, string configurazioneRilevatoreUscita, bool pedonale) : base(numero, nome, codiceGate, area, tipo, prolungaPresenza, x, y, abilitata, configurazioneRilevatoreIngresso, configurazioneRilevatoreUscita, pedonale)
        {
        }

        protected override void ConfiguraRilevatori(string configurazioneRilevatoreIngresso, string configurazioneRilevatoreUscita)
        {
            this.configurazioneRilevatoreIngresso = configurazioneRilevatoreIngresso;
            this.configurazioneRilevatoreUscita = configurazioneRilevatoreUscita;
        }

        protected override void EseguiAzione(IList<Tag> tags)
        {
            foreach (Tag tag in tags)
            {
                if (!tag.Elabora)
                {
                    continue;
                }
                List<Tag> tagsDaValutare = new List<Tag>
                    {
                        tag
                    };
                if (Area.PresenzeDict.ContainsKey(tag))
                {
                    Area.Esci(tagsDaValutare);
                    AccendiLuce(configurazioneRilevatoreUscita);
                }
                else
                {
                    Area.Entra(tagsDaValutare, null, null, null);
                    AccendiLuce(configurazioneRilevatoreIngresso);
                }
            }
        }

        private void AccendiLuce(string configurazione)
        {
            WebClient client = new WebClient
            {
                Credentials = new NetworkCredential("root", "europa")
            };
            try
            {
                log.Debug("Accendo il led con la configurazione " + configurazione);
                string[] confToken = configurazione.Split(':');                
                client.DownloadString("http://" + confToken[0] + "/axis-cgi/io/port.cgi?action=" + confToken[1] + ":/8000\\");
            }
            catch (Exception ex)
            {
                log.Error("Errore nell'accendere la luce " + configurazione, ex);
            }
        }
    }
}