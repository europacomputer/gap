﻿using GAP.Model;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace GAP.model.antenne
{
    public class AntennaIngresso : Antenna
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AntennaIngresso(int numero, string nome, string codiceGate, Area area, TipoFunzionamento tipo, double prolungaPresenza, string x, string y, bool abilitata, string configurazioneRilevatoreIngresso, string configurazioneRilevatoreUscita, bool pedonale) : base(numero, nome, codiceGate, area, tipo, prolungaPresenza, x, y, abilitata, configurazioneRilevatoreIngresso, configurazioneRilevatoreUscita, pedonale)
        {
            if (rilevatoreUscita != null)
            {
                rilevatoreUscita.RaiseRilevatoreEvent += HandleRilevatoreUscita;
            }
        }

        private void HandleRilevatoreUscita(object sender, RilevatoreEventArgs e)
        {
            //Ricontrollo chi è abilitato
            Tag tagAbilitato = Area.CheckPermessi(e.Tags);
            if (tagAbilitato != null)
            {
                Area.Entra(e.Tags, tagAbilitato);
            }
            else
            {
                Area.Entra(e.Tags);
            }
        }

        protected override void EseguiAzione(IList<Tag> tags)
        {
            if (tags.Count == 0)
                return;

            if (tags.Where(t => t.Elabora).Count() == 0)
            {
                log.Info("Tutti i tag in cache");
                return;
            }

            Tag tagAbilitato = Area.CheckPermessi(tags);

            if (tagAbilitato == null)
            {
                log.Info("Non autorizzato");
                return;
            }

            if (Area.GatesDict.ContainsKey(CodiceGate)) { Area.GatesDict[CodiceGate].Apri(); };
            //Elimino i tag che non mi servono
            List<Tag> tagsFinali = tags.Where(t =>
            {
                Area.Anagrafiche.TryGetValue(t, out Anagrafica anag);
                if (anag == null)
                    return true;
                return anag.RilevaInAudit;
            }).ToList();
            if (rilevatoreUscita == null)
            {
                Area.Entra(tagsFinali, tagAbilitato);
            }
            else
            {
                rilevatoreUscita.Valida(tagsFinali.Where(t => t.Elabora).ToList());
            }
        }
    }
}