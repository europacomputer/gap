﻿using GAP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace GAP.model.antenne
{
    internal class AntennaUscita : Antenna
    {
        public AntennaUscita(int numero, string nome, string codiceGate, Area area, TipoFunzionamento tipo, double prolungaPresenza, string x, string y, bool abilitata, string configurazioneRilevatoreIngresso, string configurazioneRilevatoreUscita, bool pedonale) : base(numero, nome, codiceGate, area, tipo, prolungaPresenza, x, y, abilitata, configurazioneRilevatoreIngresso, configurazioneRilevatoreUscita, pedonale)
        {
            if (rilevatoreIngresso != null)
            {
                rilevatoreIngresso.RaiseRilevatoreEvent += HandleRilevatoreIngresso;
            }
        }

        protected override void EseguiAzione(IList<Tag> tags)
        {
            if (Area.GatesDict.ContainsKey(CodiceGate)) { Area.GatesDict[CodiceGate].Apri(); };
            List<Tag> tagDaValidare = tags.Where(t => t.Elabora).Where(t =>
            {
                Area.Anagrafiche.TryGetValue(t, out Anagrafica anag);
                if (anag == null)
                    return true;
                return anag.RilevaInAudit;
            }).ToList();
            if (tagDaValidare.Count == 0)
            {
                return;
            }
            if (rilevatoreIngresso == null)
            {
                Area.Esci(tagDaValidare);
            }
            else
            {
                rilevatoreIngresso.Valida(tagDaValidare);
            }
        }

        private void HandleRilevatoreIngresso(object sender, RilevatoreEventArgs e)
        {
            List<Tag> tagDaValidare = e.Tags.Where(t => t.Elabora).ToList();
            if (tagDaValidare.Count == 0)
            {
                return;
            }
            Area.Esci(tagDaValidare);
        }
    }
}