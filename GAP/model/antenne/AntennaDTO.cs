﻿using GAP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GAP.Model.Antenna;

namespace GAP.model.antenne
{
    public class AntennaDTO
    {
        public short Stato { get => stato; }
        private short stato;

        public Area Area { get; }
        public string Ip { get; }
        public TipoFunzionamento Tipo { get; set; }

        public String TipoCodice
        {
            get => this.Tipo.ToString();
            set
            {
                Enum.TryParse(value, out Model.Antenna.TipoFunzionamento tipoFunzionamento);
                Tipo = tipoFunzionamento;
            }
        }

        public string X { get; }
        public string Y { get; }
        public bool Abilitata { get; }
        public string ConfigurazioneRilevatoreIngresso { get; }
        public string ConfigurazioneRilevatoreUscita { get; }
        public bool Pedonale { get; }
        public string Nome { get; }
        public string AreaCodice { get => _areaCodice; set => _areaCodice = value; }
        public String CodiceGate { get; }
        public int Numero { get; }
        public double ProlungaPresenza => prolungaPresenza;

        private string _areaCodice;
        protected Rilevatore rilevatoreIngresso;
        protected Rilevatore rilevatoreUscita;
        private readonly double prolungaPresenza;

        public AntennaDTO(int numero, String nome, String codiceGate, Area area, String ip, TipoFunzionamento tipo, double prolungaPresenza, string x, string y, Boolean abilitata, String configurazioneRilevatoreIngresso, String configurazioneRilevatoreUscita, Boolean pedonale)
        {
            this.prolungaPresenza = prolungaPresenza;
            Area = area;
            Ip = ip;
            Tipo = tipo;
            X = x;
            Y = y;
            Abilitata = abilitata;
            ConfigurazioneRilevatoreIngresso = configurazioneRilevatoreIngresso;
            ConfigurazioneRilevatoreUscita = configurazioneRilevatoreUscita;
            Pedonale = pedonale;
            Numero = numero;
            Nome = nome;
            CodiceGate = codiceGate;
        }
    }
}