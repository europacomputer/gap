﻿using GAP.model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.Caching;
using System.Text;

namespace GAP.Model
{
    public abstract class Antenna
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public short Stato { get => stato; }
        private short stato;
        public Area Area { get; }
        public TipoFunzionamento Tipo { get; set; }

        public String TipoCodice
        {
            get => this.Tipo.ToString();
            set
            {
                Enum.TryParse(value, out Model.Antenna.TipoFunzionamento tipoFunzionamento);
                Tipo = tipoFunzionamento;
            }
        }

        public string X { get; }
        public string Y { get; }
        public bool Abilitata { get; }
        public string Nome { get; }
        public string AreaCodice { get => _areaCodice; set => _areaCodice = value; }
        public String CodiceGate { get; }
        public int Numero { get; }
        public double ProlungaPresenza => prolungaPresenza;
        public Boolean Pedonale { get; }

        public enum TipoFunzionamento { I, U, P, R, IU }

        private string _areaCodice;
        protected Rilevatore rilevatoreIngresso;
        protected Rilevatore rilevatoreUscita;
        private readonly double prolungaPresenza;
        private readonly bool pedonale;

        public Antenna(int numero, String nome, String codiceGate, Area area, TipoFunzionamento tipo, double prolungaPresenza, string x, string y, Boolean abilitata, String configurazioneRilevatoreIngresso, String configurazioneRilevatoreUscita, Boolean pedonale)
        {
            this.prolungaPresenza = prolungaPresenza;
            Area = area;
            Tipo = tipo;
            X = x;
            Y = y;
            Abilitata = abilitata;
            this.pedonale = pedonale;
            Numero = numero;
            Nome = nome;
            CodiceGate = codiceGate;
            ConfiguraRilevatori(configurazioneRilevatoreIngresso, configurazioneRilevatoreUscita);
        }

        protected virtual void ConfiguraRilevatori(string configurazioneRilevatoreIngresso, string configurazioneRilevatoreUscita)
        {
            if (!String.IsNullOrEmpty(configurazioneRilevatoreIngresso))
            {
                rilevatoreIngresso = new Rilevatore(configurazioneRilevatoreIngresso, Nome);
            }
            if (!String.IsNullOrEmpty(configurazioneRilevatoreUscita))
            {
                rilevatoreUscita = new Rilevatore(configurazioneRilevatoreUscita, Nome);
            }
        }

        protected abstract void EseguiAzione(IList<Tag> tags);

        virtual protected void Elabora(IList<Tag> tagsLetti)
        {
            IList<Tag> tags = new List<Tag>();
            StringBuilder sb = null;
            foreach (Tag tag in tagsLetti)
            {
                //Se il tag è in cache di un'altra antenna non lo considero
                if (Area.CacheTagLetti.Contains(tag.Codice))
                {
                    Tag tagLettoInCache = (Tag)Area.CacheTagLetti[tag.Codice];
                    if (tagLettoInCache.Antenna.Equals(Nome))
                    {
                        tags.Add(tag);
                    }
                    else
                    {
                        log.Info("Tag " + tag.Codice + " letto dall'antenna " + tagLettoInCache.Antenna + " : non lo elaboro");
                    }
                }
                else
                {
                    tags.Add(tag);
                }
            }
            foreach (KeyValuePair<String, Object> tagCache in Area.CacheTagLetti)
            {
                if (sb == null)
                {
                    sb = new StringBuilder("Tags in cache area ");
                    sb.Append(Area.CacheTagLetti.Name).Append(":");
                }
                Tag tagInCache = (Tag)tagCache.Value;
                sb.Append(tagInCache.Codice).Append("@").Append(tagInCache.Antenna).Append(":").Append(tagInCache.Elabora).Append("#");
                if (tagInCache.Antenna.Equals(Nome))
                {
                    tagInCache.Presente = tags.Contains(tagInCache);
                    tagInCache.Elabora = false;
                    tags.Remove(tagInCache);
                    tags.Add(tagInCache);
                }
            }
            if (sb != null)
            {
                log.Debug(sb.ToString());
            }
            if (tags.Count == 0)
            {
                log.Info("I Tag letti da antenna " + Nome + " non sono da elaborare");
                return;
            }
            EseguiAzione(tags);

            foreach (Tag tag in tags.Where(t => t.Presente))
            {
                if (!Area.CacheTagLetti.Contains(tag.Codice))
                {
                    CacheItemPolicy cacheItemPolicy = new CacheItemPolicy
                    {
                        AbsoluteExpiration = DateTime.Now.AddSeconds(prolungaPresenza),
                        RemovedCallback = new CacheEntryRemovedCallback(CacheRemovedCallback),
                    };
                    tag.Elabora = false;
                    tag.Antenna = Nome;
                    Area.CacheTagLetti.Set(tag.Codice, tag, cacheItemPolicy);
                }
            }
        }

        protected virtual void CacheRemovedCallback(CacheEntryRemovedArguments arguments)
        {
            if (arguments.RemovedReason == CacheEntryRemovedReason.Expired)
            {
                log.Debug("Rimosso un tag dalla cache " + Area.CacheTagLetti.Name + ":" + (Tag)arguments.CacheItem.Value);
            }
        }

        public void ElaboraTags(String tags)
        {
            if (tags.StartsWith("stato:"))
            {
                stato = Int16.Parse(tags.Replace("stato:", ""));
            }
            else
            {
                IList<Tag> tagsLetti = new List<Tag>();
                log.Info("#Tags Letti da antenna " + Nome + " : " + tags);
                if (!String.IsNullOrEmpty(tags))
                {
                    string[] tagToken = tags.Split('#');
                    foreach (String tag in tagToken)
                    {
                        Tag newTag = new Tag(tag, true);
                        Area.Anagrafiche.TryGetValue(newTag, out Anagrafica anagrafica);
                        if (Tag.ValidaPattern(tag) && anagrafica!=null)
                        {
                            tagsLetti.Add(new Tag(tag, true));
                            if (anagrafica.Pedonale && this.pedonale || !anagrafica.Pedonale)
                            {                                
                                //Se ho una anagrafica collegata la aggiungo.
                                if (!String.IsNullOrEmpty(anagrafica.CodiceCollegato))
                                {
                                    string[] token = anagrafica.CodiceCollegato.Split('|');
                                    if (Area.AnagraficheByCodice.TryGetValue(token[0], out Anagrafica anagraficaCollegata))
                                    {
                                        log.InfoFormat("Inserisco il tag {0} per l'anagrafica collegata {1}", anagraficaCollegata.Tag, anagraficaCollegata.Denominazione);
                                        tagsLetti.Add(new Tag(anagraficaCollegata.Tag, true));
                                    }
                                }
                            }
                            else
                            {
                                log.InfoFormat("Non considero {0} perché anagrafica pedonale ", tag);
                            }
                        }
                    }
                    Elabora(tagsLetti);
                }
            }
        }

        public override string ToString()
        {
            return Nome + ":" + CodiceGate + ":" + Area;
        }
    }
}