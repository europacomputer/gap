﻿using GAP.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace GAP.model.antenne
{
    internal class DeviceReader
    {
        private Dictionary<int, Antenna> antenne;

        private Process process;
        public string Ip { get; }

        public DeviceReader(string ip)
        {
            Ip = ip;
            antenne = new Dictionary<int, Antenna>();
        }

        public void AddAntenna(int porta, Antenna antenna)
        {
            antenne.Add(porta, antenna);
        }

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void Stop()
        {
            process.Kill();
        }

        private void GapReaderOutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (outLine == null)
            {
                return;
            }
            if (String.IsNullOrEmpty(outLine.Data))
            {
                return;
            }

            if (antenne.Count == 1)
            {
                foreach (Antenna antenna in antenne.Values)
                {
                    antenna.ElaboraTags(outLine.Data);
                }
            }
            else
            {
                //Se è uno stato lo passo a tutte le antenne
                if (outLine.Data.StartsWith("stato"))
                {
                    foreach (Antenna antenna in antenne.Values)
                    {
                        antenna.ElaboraTags(outLine.Data);
                    }
                }
                else
                {
                    int porta = Convert.ToInt16(outLine.Data.Substring(0, 1));
                    if (antenne.TryGetValue(porta, out Antenna antenna))
                    {
                        antenna.ElaboraTags(outLine.Data.Substring(2));
                    }
                }
            }
        }

        public void Start()
        {
            try
            {
                process = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = @"GAPReader.exe",
                        Arguments = antenne.Count == 1 ? Ip : "m:" + Ip,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true
                    }
                };
                process.OutputDataReceived += new DataReceivedEventHandler(GapReaderOutputHandler);
                process.ErrorDataReceived += new DataReceivedEventHandler(GapReaderOutputHandler);
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                log.Info("Lettore " + Ip + " partito");
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
    }
}