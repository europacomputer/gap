﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAP.Util
{
    public class Page
    {
        private int? from;
        private int? to;
        private int? size;

        public int? Size { get => size; set => size = value; }
        public int? To { get => to; set => to = value; }
        public int? From { get => from; set => from = value; }
    }

    public class Filter
    {
        private string property;
        private string value;

        public string Value { get => value; set => this.value = value; }
        public string Property { get => property; set => property = value; }
    }

    public class RicercaAudit
    {
        private List<Filter> filters;
        private Page page;

        public RicercaAudit()
        {
            filters = new List<Filter>();
            page = new Page()
            {
                Size = 1000,
                From = 0
            };
        }

        public Page Page { get => page; set => page = value; }
        public List<Filter> Filters { get => filters; set => filters = value; }

        public string GeneraSql()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Filter filtro in filters)
            {
                if (filtro.Property.ToLower() == "areeprincipali")
                {
                    sb.Append(" and ar.Principale='' ");
                }
                else if (filtro.Property.ToLower() == "dataazione" || filtro.Property.ToLower() == "data")
                {
                    string dataFiltro = filtro.Value;
                    DateTime dataInizio = DateTime.MinValue;
                    DateTime dataFine = DateTime.MinValue;
                    string operatore = ">=";
                    if (filtro.Value.StartsWith(">") || filtro.Value.StartsWith("<"))
                    {
                        DateTime.TryParse(filtro.Value.Substring(1), out dataInizio);
                        operatore = filtro.Value.Substring(0, 1);
                    }
                    else
                    {
                        DateTime.TryParse(filtro.Value, out dataInizio);
                        if (DateTime.TryParse(filtro.Value, out dataFine))
                        {
                            dataFine = dataFine.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                        }
                    }

                    if (dataInizio != DateTime.MinValue)
                    {
                        sb.Append(" AND ").Append(" Data ").Append(operatore).Append(dataInizio.Ticks);
                    }
                    if (dataFine != DateTime.MinValue)
                    {
                        sb.Append(" AND ").Append(" Data <=").Append(dataFine.Ticks);
                    }
                }
                else
                {
                    sb.Append(" AND ").Append(filtro.Property.Replace("Tag", "au.Tag")).Append(" like '").Append(filtro.Value).Append("%'");
                }
            }
            return sb.ToString();
        }
    }
}