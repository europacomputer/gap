﻿using GAP.Dao;
using GAP.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GAP.util
{
    public class AnagraficaExporter
    {
        public void Esporta()
        {
            String fileName = "ACCESSI_AN" + System.Configuration.ConfigurationManager.AppSettings["impianto"] + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            using (DB db = new DB())
            {
                List<Anagrafica> anagrafiche = db.ExecuteSelect("select Codice,Denominazione,Tag,RilevaInAudit,Pedonale,CodiceCollegato from Anagrafiche", Anagrafica.CaricaListaMethod());
                string pathFile = System.IO.Path.GetTempPath() + "/" + fileName;
                FileStream stream = new FileStream(pathFile, FileMode.OpenOrCreate);
                StreamWriter writer = new StreamWriter(stream);

                writer.WriteLine("ID_IMP;COD_AUT;NOME_AUT;AUT_TAG");
                foreach (Anagrafica an in anagrafiche)
                {
                    writer.Write(String.Format(
                        "Z" + System.Configuration.ConfigurationManager.AppSettings["impianto"] + ";{0};{1};{2}",
                        an.Codice,
                        an.Denominazione,
                        an.Tag
                        ));
                    writer.WriteLine();
                }
                writer.Flush();
                writer.Close();
                stream.Close();
                if (System.Configuration.ConfigurationManager.AppSettings["auditExportSource"].Length > 0)
                {
                    String auditExportSource = System.Configuration.ConfigurationManager.AppSettings["auditExportSource"];
                    File.Copy(pathFile, auditExportSource + "/" + fileName);
                }
            }
        }
    }
}