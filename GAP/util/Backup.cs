﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GAP.util
{
    public class Backup
    {
        public void Esegui()
        {
            string folder = System.Configuration.ConfigurationManager.AppSettings["backupdbFolder"];
            string pathDB = @".\GAP.sqlite";
            String fileName = "ACCESSI_DB" + System.Configuration.ConfigurationManager.AppSettings["impianto"] + DateTime.Now.ToString("yyyyMMddHHmmss") + ".sqlite";
            File.Copy(pathDB, folder + fileName, true);
        }
    }
}