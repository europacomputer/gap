﻿using GAP.Dao;
using GAP.Model;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace GAP.Util
{
    public class AnagraficaImporter
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Aree aree;
        private Dictionary<String, DateTime> filePresenti;
        private bool leggi;
        private string path;

        public void Start(Aree aree)
        {
            filePresenti = new Dictionary<string, DateTime>();
            leggi = true;
            this.aree = aree ?? throw new ArgumentNullException(nameof(aree));
            path = Assembly.GetExecutingAssembly().Location.Replace("GAP.exe", "") + "IMPORT";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            Thread t1 = new Thread(() => Check());
            t1.Start();
        }

        public void Stop()
        {
            leggi = false;
        }

        private void Check()
        {
            while (leggi)
            {
                //Se ho folderImportSource impostata copio i file che mi interessano nella cartella..poi li cancello
                String folderImportSource = System.Configuration.ConfigurationManager.AppSettings["folderImportSource"];
                if (folderImportSource.Length > 0)
                {
                    ImportaFileDaCartellaSource(folderImportSource, path);
                }
                String[] files = Directory.GetFiles(path, "*.zip");
                //Primo ciclo per scompattare eventuali zip
                foreach (string file in files)
                {
                    Decomprimifile(file);
                }

                files = Directory.GetFiles(path);
                foreach (string file in files)
                {
                    FileInfo fi = new System.IO.FileInfo(file);
                    if (filePresenti.ContainsKey(file))
                    {
                        DateTime ultimoAggiornamento = filePresenti[file];
                        if (fi.LastWriteTime == ultimoAggiornamento)
                        {
                            Importa(file);
                        }
                        else
                        {
                            filePresenti.Remove(file);
                            filePresenti.Add(file, fi.LastWriteTime);
                        }
                    }
                    else
                    {
                        filePresenti.Add(file, fi.LastWriteTime);
                    }
                }
                Thread.Sleep(10000);
            }
        }

        private void ImportaFileDaCartellaSource(string folderImportSource, string folderDest)
        {
            String[] filesPath = Directory.GetFiles(folderImportSource, "*");

            foreach (string filePath in filesPath)
            {
                string fileName = Path.GetFileName(filePath);
                if (fileName.StartsWith("CONCMASTER"))
                {
                    if (fileName.ToUpper().EndsWith("AUTOBET.ZIP") || fileName.ToUpper().EndsWith("AUTISTI.ZIP") || fileName.ToUpper().EndsWith("UTENTI.ZIP"))
                    {
                        File.Copy(filePath, folderDest + "\\" + fileName);
                    }
                }
                File.Delete(filePath);
            }
        }

        private void Decomprimifile(string file)
        {
            try
            {
                using (ZipFile zipFile = ZipFile.Read(file))
                {
                    String nomeFile = Path.GetFileName(file);
                    Int64 token0 = Int64.Parse(nomeFile.Substring(13, 14));
                    Int64 token1 = Int64.Parse(nomeFile.Substring(10, 3));
                    Int64 pwdInt = token0 % token1;
                    zipFile.Password = Convert.ToString(pwdInt) + token1;
                    zipFile.ExtractAll(path, ExtractExistingFileAction.OverwriteSilently);
                }
                File.Delete(file);
            }
            catch (Exception ex)
            {
                log.Error("Errore nel decomprimere il file " + file, ex);
            }
        }

        private void Importa(string path)
        {
            int colonnaTag = 0;
            if (path.Contains("UTENTI"))
            {
                colonnaTag = 7;
            }
            else if (path.Contains("AUTOBET"))
            {
                colonnaTag = 35;
            }
            else if (path.Contains("AUTOBET"))
            {
                colonnaTag = 8;
            }
            using (StreamReader sr = new StreamReader(path))
            {
                try
                {
                    sr.ReadLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("file di importazione vuoto");
                }
                using (DB db = new DB())
                {
                    while (sr.Peek() >= 0)
                    {
                        try
                        {
                            string[] token = sr.ReadLine().Split(';');

                            SQLiteParameter[] parametriInsert = { new SQLiteParameter("codice", token[1].Replace(@"""", "")), new SQLiteParameter("denominazione", token[2].Replace(@"""", "")), new SQLiteParameter("tag", token[colonnaTag]) };

                            List<Anagrafica> anagrafiche = db.ExecuteSelect("select Codice,Denominazione,Tag,RilevaInAudit,Pedonale,CodiceCollegato from Anagrafiche where codice=@codice", Anagrafica.CaricaListaMethod(), parametriInsert);
                            if (anagrafiche.Count == 0)
                            {
                                db.ExecuteCommand("insert into Anagrafiche('Codice', 'Denominazione','Tag') values(@codice, @denominazione,@tag)", parametriInsert);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(anagrafiche[0].Tag))
                                {
                                    db.ExecuteCommand("update Anagrafiche set Denominazione=@denominazione,tag=@tag where Codice=@codice", parametriInsert);
                                }
                                else
                                {
                                    db.ExecuteCommand("update Anagrafiche set Denominazione=@denominazione where Codice=@codice", parametriInsert);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("Errore noll'inserire l'anagrafica ", ex);
                        }
                    }
                }
            }
            File.Delete(path);
            this.aree.AggiornaAnagrafiche();
        }
    }
}