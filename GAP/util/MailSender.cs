﻿using GAP.Controller;
using GAP.Dao;
using GAP.model;
using GAP.Model;
using GAP.Rest;
using GAP.util;
using GAP.Util;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace GAP.Util
{
    public class MailSender
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public String SendMail(Aree aree)
        {
            new AnagraficaExporter().Esporta();
            new Backup().Esegui();
            String result = "";
            Configurazione configurazione = Configurazione.Carica();
            if (String.IsNullOrEmpty(configurazione.MailAddress))
            {
                return "Nessun destinatario per la mail di warning";
            }
            else
            {
                try
                {
                    MailMessage mail = new MailMessage
                    {
                        IsBodyHtml = true
                    };
                    string[] dests = configurazione.MailAddress.Split(',');
                    foreach (String dest in dests)
                    {
                        mail.To.Add(dest);
                    }

                    mail.From = new MailAddress(configurazione.From);
                    using (SmtpClient client = new SmtpClient())
                    {
                        if (Int32.TryParse(configurazione.PortMail, out int porta))
                        {
                            client.Port = porta;
                        }
                        else
                        {
                            return "Porta smtp non impostata o non numerica";
                        }

                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        //client.UseDefaultCredentials = false;
                        client.Host = configurazione.ServerMail;
                        client.EnableSsl = true;
                        client.Credentials = new System.Net.NetworkCredential(configurazione.UserMail, configurazione.PasswordMail);
                        try
                        {
                            FillMail(configurazione, mail, aree);
                            client.Send(mail);
                            result = "Mail inviata correttamente";
                            log.Info("Mail inviata correttamente");
                        }
                        catch (Exception ex)
                        {
                            result = ex.ToString();
                            log.Error("Errore nell'invio della mail", ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = ex.ToString();
                }
            }
            return result;
        }

        public void FillMail(Configurazione configurazione, MailMessage message, Aree aree)
        {
            message.Subject = "Presenze nell'impianto " + System.Configuration.ConfigurationManager.AppSettings["impianto"];
            message.Body = aree.CaricaPresenzeHtml();
            Attachment fileAudit = new Attachment(CreaFileAudit(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            message.Attachments.Add(fileAudit);
        }

        private string CreaFileAudit()
        {
            AuditController auditController = new AuditController();
            RicercaAudit ricercaAudit = new RicercaAudit();
            DateTime dataInizioGiornata = DateTime.Now.AddDays(-1).Date;
            Filter filtro = new Filter
            {
                Property = "data",
                Value = dataInizioGiornata.ToShortDateString()
            };
            ricercaAudit.Filters.Add(filtro);
            List<Audit> resultAudit = auditController.CercaAudit(ricercaAudit);
            String fileName = "ACCESSI___" + System.Configuration.ConfigurationManager.AppSettings["impianto"] + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            string pathFile = System.IO.Path.GetTempPath() + "/" + fileName;
            FileStream stream = new FileStream(pathFile, FileMode.OpenOrCreate);
            StreamWriter writer = new StreamWriter(stream);

            foreach (Audit audit in resultAudit)
            {
                writer.Write($"{audit.Id};{audit.Tag};{audit.Denominazione};{audit.Evento};{audit.Area};{audit.DataAzione};{audit.Nota};{audit.Anomalia}");
                writer.WriteLine();
            }
            writer.Flush();
            writer.Close();
            stream.Close();
            if (System.Configuration.ConfigurationManager.AppSettings["auditExportSource"].Length > 0)
            {
                String auditExportSource = System.Configuration.ConfigurationManager.AppSettings["auditExportSource"];
                File.Copy(pathFile, auditExportSource + "/" + fileName);
            }
            return pathFile;
        }
    }
}