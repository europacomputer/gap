﻿using GAP.Controller;
using GAP.Dao;
using GAP.model;
using GAP.Util;
using System;
using System.Data.SQLite;
using System.IO;
using System.Web.Http;

namespace GAP.rest
{
    [Authorize(Roles = "admin")]
    [RoutePrefix("configurazione")]
    public class ConfigurazioneController : ApiController
    {
        [Route("mail/test")]
        [HttpGet]
        public IHttpActionResult TestMail()
        {
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            String result = new MailSender().SendMail(controller.Aree);
            var dir = new DirectoryInfo(System.IO.Path.GetTempPath());
            foreach (var file in dir.EnumerateFiles("mailsend*.csv"))
            {
                try { file.Delete(); } catch (Exception e) { };
            }
            return Ok(result);
        }

        [Route("")]
        [HttpPut]
        public IHttpActionResult Salva(Configurazione configurazione)
        {
            using (DB db = new DB())
            {
                db.ExecuteCommand("delete from Configurazione");
                db.ExecuteCommand(String.Format("INSERT INTO Configurazione('MinutiWarning','OrarioMail','MailAddress',`UserMail`,`ServerMail`,`PasswordMail`,`PortMail`,`From`,AntennaConfigurazione) values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", configurazione.MinutiWarning, configurazione.OrarioMail, configurazione.MailAddress, configurazione.UserMail, configurazione.ServerMail, configurazione.PasswordMail, configurazione.PortMail, configurazione.From, configurazione.AntennaConfigurazione));
                return Ok();
            }
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult Carica()
        {
            return Ok(Configurazione.Carica());
        }
    }
}