﻿using GAP.Controller;
using GAP.Dao;
using GAP.Model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Net;
using System.Web.Http;

namespace GAP.rest
{
    [RoutePrefix("anagrafiche")]
    public class AnagraficheController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("")]
        [HttpPost]
        ////[Authorize(Roles ="admin,poweruser")]
        public IHttpActionResult Aggiorna(Anagrafica anagrafica)
        {
            try
            {
                using (DB db = new DB())
                {
                    if (!String.IsNullOrEmpty(anagrafica.Tag))
                    {
                        Func<SQLiteDataReader, int> countResult = delegate (SQLiteDataReader reader)
                        {
                            int countTag = 0;
                            while (reader.Read())
                            {
                                countTag = (Convert.ToInt16(reader["tags_num"]));
                            }
                            return countTag;
                        };

                        SQLiteParameter[] parametriSelectTag = { new SQLiteParameter("tag", anagrafica.Tag), new SQLiteParameter("codice", anagrafica.Codice) };
                        int numAnagraficheConTag = db.ExecuteSelect("select count(tag) as tags_num from Anagrafiche where Tag=@tag and codice!=@codice", countResult, parametriSelectTag);
                        if (numAnagraficheConTag > 0)
                        {
                            throw new HttpResponseException(HttpStatusCode.Conflict);
                        }
                    }

                    SQLiteParameter[] parametriSelect = { new SQLiteParameter("codice", anagrafica.Codice) };
                    SQLiteParameter[] parametriCommand = { new SQLiteParameter("codice", anagrafica.Codice), new SQLiteParameter("denominazione", anagrafica.Denominazione), new SQLiteParameter("tag", anagrafica.Tag == null ? "" : anagrafica.Tag.Trim()), new SQLiteParameter("audit", Convert.ToInt16(anagrafica.RilevaInAudit)), new SQLiteParameter("pedonale", Convert.ToInt16(anagrafica.Pedonale)), new SQLiteParameter("ancollegata", anagrafica.CodiceCollegato), new SQLiteParameter("locale", true) };
                    if (db.ExecuteSelect("select Codice from Anagrafiche where Codice=@codice", Anagrafica.AnagraficaExistMethod(anagrafica.Codice), parametriSelect))
                    {
                        db.ExecuteCommand("update Anagrafiche set Denominazione=@denominazione,Tag=@tag,RilevaInAudit=@audit,Pedonale=@pedonale,CodiceCollegato=@ancollegata where  Codice=@codice", parametriCommand);
                    }
                    else
                    {
                        db.ExecuteCommand("insert into Anagrafiche('Codice', 'Denominazione', 'Tag','RilevaInAudit','Pedonale','CodiceCollegato','locale') values(@codice,@denominazione, @tag,@audit,@pedonale,@ancollegata,@locale)", parametriCommand);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("errore in aggiornamento anagrafica", ex);
                throw ex;
            }
            return Ok();
        }

        [Route("")]
        [HttpPut]
        public IHttpActionResult Applica()
        {
            try
            {
                AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
                controller.Aree.AggiornaAnagrafiche();
                controller.Aree.CaricaPermessi();
            }
            catch (Exception ex)
            {
                log.Error("errore nel ricaricare le anagrafiche ed i permessi ", ex);
            }
            return Ok();
        }

        [Route("{codice}")]
        [HttpDelete]
        ////[Authorize(Roles ="admin,poweruser")]
        public IHttpActionResult Cancella(string codice)
        {
            try
            {
                using (DB db = new DB())
                {
                    SQLiteParameter[] parameters = { new SQLiteParameter("codice", codice) };
                    db.ExecuteCommand("delete from Anagrafiche where codice=@codice", parameters);
                }
            }
            catch (Exception ex)
            {
                log.Error("Errore nella cancellazione dell'anagrafica", ex);
                throw ex;
            }
            return Ok();
        }

        [Route("")]
        [HttpGet]
        ////[Authorize(Roles ="admin,poweruser,user")]
        public IHttpActionResult Carica()
        {
            using (DB db = new DB())
            {
                List<Anagrafica> anagrafiche = db.ExecuteSelect("select Codice,Denominazione,Tag,RilevaInAudit,Pedonale,CodiceCollegato from Anagrafiche", Anagrafica.CaricaListaMethod());
                return Ok(anagrafiche);
            }
        }

        [Route("{ricerca}")]
        [HttpGet]
        ////[Authorize(Roles ="admin,poweruser,user")]
        public IHttpActionResult Carica(String ricerca)
        {
            try
            {
                Func<SQLiteDataReader, List<String>> fillResource = delegate (SQLiteDataReader reader)
                {
                    List<String> result = new List<String>();
                    while (reader.Read())
                    {
                        result.Add(Convert.ToString(reader["CodDen"]));
                    }
                    return result;
                };
                using (DB db = new DB())
                {
                    SQLiteParameter[] parameters = { new SQLiteParameter("filtro", "%" + ricerca + "%") };
                    List<String> anagrafiche = db.ExecuteSelect("select Codice || '|' || Denominazione as CodDen from Anagrafiche an where an.Codice like @filtro or an.Denominazione like @filtro", fillResource, parameters);
                    return Ok(anagrafiche);
                }
            }
            catch (Exception ex)
            {
                log.Error("errore nel caricare l'anagrafica con ricerca " + ricerca, ex);
                throw ex;
            }
        }
    }
}