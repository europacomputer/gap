﻿using GAP.Dao;
using GAP.Model;
using GAP.Util;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Web.Security;

namespace GAP.Rest
{
    [RoutePrefix("audit")]
    public class AuditController : ApiController
    {
        [Route("size")]
        [HttpPost]
        [Authorize(Roles = "admin,poweruser")]
        public IHttpActionResult Size(RicercaAudit parametriRicerca)
        {
            Dictionary<string, string> parametriRequest = this.Request.GetQueryNameValuePairs()
                                               .ToDictionary(kv => kv.Key, kv => kv.Value,
                                                    StringComparer.OrdinalIgnoreCase);
            if (parametriRequest.ContainsKey("areeprincipali"))
            {
                Filter f = new Filter
                {
                    Property = "areeprincipali"
                };
                parametriRicerca.Filters.Add(f);
            }
            return Ok(SizeAudit(parametriRicerca));
        }

        private int SizeAudit(RicercaAudit parametriRicerca)
        {
            //gli id della tabella sono crescenti e incrementali di 1
            Func<SQLiteDataReader, int> fillResourceCount = delegate (SQLiteDataReader reader)
            {
                reader.Read();
                return int.Parse(reader["size"].ToString());
            };
            int numRecord = 0;
            using (DB db = new DB())
            {
                numRecord = db.ExecuteSelect("select count(*) as size " + BuildFrom(parametriRicerca), fillResourceCount);
            }
            return numRecord;
        }

        /**       {
         "pages":{
           "from":1,
           "size":50
         },
         "filters": [
           {
             "property":"tag",
             "value":"12"
           }
         ]
}**/

        [Route("ricerche/csv")]
        [HttpPost]
        [Authorize(Roles = "admin,poweruser")]
        public HttpResponseMessage CercaCsv(RicercaAudit parametriRicerca)
        {
            if (parametriRicerca == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Parametri di ricerca non impostati")
                });
            }

            List<Audit> resultAudit = CercaAudit(parametriRicerca);
            StringBuilder sb = new StringBuilder();
            parametriRicerca.Page.From = 0;
            parametriRicerca.Page.Size = 10000;
            foreach (Audit audit in resultAudit)
            {
                sb.AppendFormat(
                    "{0};{1};{2};{3};{4};{5};{6};{7}",
                    audit.Id,
                    audit.Tag,
                    audit.Denominazione,
                    audit.Evento,
                    audit.Area,
                    audit.DataAzione,
                    audit.Nota,
                    audit.Anomalia
                    );
                sb.AppendLine();
            }

            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(sb.ToString());
            writer.Flush();
            stream.Position = 0;

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(stream)
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("text/csv");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "Audit.csv" };
            return result;
        }

        public List<Audit> CercaAudit(RicercaAudit parametriRicerca)
        {
            Func<SQLiteDataReader, List<Audit>> fillResource = delegate (SQLiteDataReader reader)
            {
                List<Audit> result = new List<Audit>();
                while (reader.Read())
                {
                    Audit audit = new Audit
                    {
                        Id = int.Parse(reader["id"].ToString()),
                        Tag = reader["tag"].ToString(),
                        Evento = reader["evento"].ToString(),
                        Anomalia = reader["anomalia"].ToString(),
                        Nota = reader["nota"].ToString(),
                        Data = long.Parse(reader["data"].ToString()),
                        Area = reader["area"].ToString(),
                        Denominazione = Convert.ToString(reader["denominazione"])
                    };
                    result.Add(audit);
                }
                return result;
            };
            using (DB db = new DB())
            {
                Func<SQLiteDataReader, int> fillResourceCount = delegate (SQLiteDataReader reader)
                {
                    reader.Read();
                    return int.Parse(reader["minId"].ToString());
                };
                int minId = db.ExecuteSelect("select min(id) as minId from Audit", fillResourceCount);
                int numRecord = SizeAudit(parametriRicerca);
                //string whereId = String.Format(" and au.id<={0} and au.id>={1} ", numRecord - parametriRicerca.Page.From, numRecord - parametriRicerca.Page.From - parametriRicerca.Page.Size);
                string whereId = String.Format(" LIMIT {0}, {1} ", parametriRicerca.Page.From, parametriRicerca.Page.Size);
                return db.ExecuteSelect("select au.*,an.Denominazione as denominazione " + BuildFrom(parametriRicerca) + whereId, fillResource);
            }
        }

        private String BuildFrom(RicercaAudit parametri, String whereToAppend = "")
        {
            return " from Audit au left join Anagrafiche an on an.Tag=au.Tag left join Aree ar on ar.nome=Area where 1=1 " + parametri.GeneraSql() + whereToAppend + " order by au.id desc";
        }

        [Route("ricerche/html")]
        [HttpPost]
        [Authorize(Roles = "admin,poweruser")]
        public HttpResponseMessage Html(RicercaAudit parametriRicerca)
        {
            if (parametriRicerca == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Parametri di ricerca non impostati")
                });
            }
            Dictionary<string, string> parametriRequest = this.Request.GetQueryNameValuePairs()
                                              .ToDictionary(kv => kv.Key, kv => kv.Value,
                                                   StringComparer.OrdinalIgnoreCase);
            if (parametriRequest.ContainsKey("areeprincipali"))
            {
                Filter f = new Filter
                {
                    Property = "areeprincipali"
                };
                parametriRicerca.Filters.Add(f);
            }
            List<Audit> resultAudit = CercaAudit(parametriRicerca);
            StringBuilder sb = new StringBuilder();
            parametriRicerca.Page.From = 0;
            parametriRicerca.Page.Size = 10000;
            sb.Append("{\"testo\":\"");
            sb.Append("<h2>LOG ACCESSI</h2>");
            sb.Append("<TABLE>");
            sb.Append("<TR>");
            sb.Append("<TH align='left'>ID</TH>");
            sb.Append("<TH align='left'>TAG</TH>");
            sb.Append("<TH align='left'>Anag</TH>");
            sb.Append("<TH align='left'>Evento</TH>");
            sb.Append("<TH align='left'>Area</TH>");
            sb.Append("<TH align='left'>Data</TH>");
            sb.Append("<TH align='left'>Nota</TH>");
            //sb.Append("<TD>").Append(audit.Anomalia).Append("</TD>");
            sb.Append("</TR>");
            foreach (Audit audit in resultAudit)
            {
                sb.Append("<TR>");
                sb.Append("<TD>").Append(audit.Id).Append("</TD>");
                sb.Append("<TD>").Append(audit.Tag).Append("</TD>");
                sb.Append("<TD>").Append(audit.Denominazione).Append("</TD>");
                sb.Append("<TD>").Append(audit.Evento).Append("</TD>");
                sb.Append("<TD>").Append(audit.Area).Append("</TD>");
                sb.Append("<TD>").Append(audit.DataAzione).Append("</TD>");
                sb.Append("<TD>").Append(audit.Nota).Append("</TD>");
                //sb.Append("<TD>").Append(audit.Anomalia).Append("</TD>");
                sb.Append("</TR>");
            }
            sb.Append("</TABLE>");
            sb.Append("\"}");
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(sb.ToString());
            writer.Flush();
            stream.Position = 0;

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(stream)
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("text/html");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "Audit.html" };
            return result;
        }

        [Route("ricerche")]
        [HttpPost]
        [Authorize(Roles = "admin,poweruser")]
        public IHttpActionResult Cerca(RicercaAudit parametriRicerca)
        {
            if (parametriRicerca == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Parametri di ricerca non impostati")
                });
            }

            Dictionary<string, string> parametriRequest = this.Request.GetQueryNameValuePairs()
                                               .ToDictionary(kv => kv.Key, kv => kv.Value,
                                                    StringComparer.OrdinalIgnoreCase);
            if (parametriRequest.ContainsKey("areeprincipali"))
            {
                Filter f = new Filter
                {
                    Property = "areeprincipali"
                };
                parametriRicerca.Filters.Add(f);
            }
            return Ok(CercaAudit(parametriRicerca));
        }
    }
}