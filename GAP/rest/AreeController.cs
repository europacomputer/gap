﻿using GAP.Controller;
using GAP.Dao;
using GAP.Model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;

namespace GAP.Rest
{
    [RoutePrefix("area")]
    public class AreeController : ApiController
    {
        [Route("{area}/{nome}")]
        [HttpPut]
        public IHttpActionResult Entrata(string area, string nome, [FromBody]string infoData)
        {
            String nota = "";
            long data = 0;

            string[] infoToken = infoData.Split(';');
            if (DateTime.TryParseExact(infoToken[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dataTime))
            {
                data = dataTime.Ticks;
            }
            if (infoToken.Count() == 2 && (!infoToken[1].Equals("undefined"))) { nota = infoToken[1]; }

            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];

            //Cerco il codice anagrafica
            String codice = nome.Split('|')[0];

            Tag tagDaInserire = new Tag("*" + nome, true);
            using (DB db = new DB())

            {
                SQLiteParameter[] parametriSelectTag = { new SQLiteParameter("codice", codice) };
                List<Anagrafica> anagrafiche = db.ExecuteSelect<Anagrafica>("select Codice,Denominazione,Tag,RilevaInAudit,Pedonale,CodiceCollegato from Anagrafiche where Codice=@codice", Anagrafica.CaricaListaMethod(), parametriSelectTag);
                if (anagrafiche.Count > 0)
                {
                    tagDaInserire = new Tag(anagrafiche[0].Tag, true);
                }
            }

            IList<Tag> tags = new List<Tag>() { tagDaInserire };
            controller.Aree[area].Entra(tags, null, null, nota, data);
            return Ok();
        }

        [Route("{area}/{gate}/{codiceTag}")]
        [HttpPut]
        [Authorize(Roles = "admin,poweruser")]
        public IHttpActionResult Uscita(string area, string gate, string codiceTag, [FromBody]string nota)
        {
            if (nota.Equals("undefined"))
                nota = "";
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            IList<Tag> tags = new List<Tag>() { new Tag(codiceTag, true) };
            if (controller.Aree[area].GatesDict.ContainsKey(gate)) { controller.Aree[area].GatesDict[gate].Apri(); };
            controller.Aree[area].Esci(tags, nota);
            return Ok();
        }

        [Route("principali")]
        [HttpGet]
        [Authorize(Roles = "admin,poweruser,user")]
        public IHttpActionResult CaricaPrincipali()
        {
            Func<SQLiteDataReader, List<Area>> fillResource = delegate (SQLiteDataReader reader)
            {
                List<Area> result = new List<Area>();
                while (reader.Read())
                {
                    Area a = new Area(Convert.ToInt32(reader["Id"]), Convert.ToString(reader["Nome"]))
                    {
                        NomeAreaPrincipale = Convert.ToString(reader["Principale"])
                    };
                    result.Add(a);
                }
                return result;
            };
            using (DB db = new DB())
            {
                return Ok(db.ExecuteSelect("select * from Aree where principale is null or principale=''", fillResource));
            }
        }

        [Route("")]
        [HttpGet]
        ////[Authorize(Roles = "admin,poweruser,user")]
        public IHttpActionResult Carica()
        {
            Func<SQLiteDataReader, List<Area>> fillResource = delegate (SQLiteDataReader reader)
            {
                List<Area> result = new List<Area>();
                while (reader.Read())
                {
                    Area a = new Area(Convert.ToInt32(reader["Id"]), Convert.ToString(reader["Nome"]), false)
                    {
                        NomeAreaPrincipale = Convert.ToString(reader["Principale"])
                    };
                    result.Add(a);
                }
                return result;
            };
            using (DB db = new DB())
            {
                return Ok(db.ExecuteSelect("select * from Aree", fillResource));
            }
        }

        [Route("stato")]
        [HttpGet]
        public IHttpActionResult StatoAree()
        {
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            IList<Area> aree = controller.Aree.ListaAree();
            List<Area> areeOrdinate = aree.OrderBy(a => a.Id).ToList();
            return Ok(areeOrdinate);
        }

        [Route("stato/html")]
        [HttpGet]
        public IHttpActionResult StatoAreeHtml()
        {
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            return Ok(controller.Aree.CaricaPresenzeHtml());
        }

        [Route("{area}/presenze")]
        [HttpGet]
        [Authorize(Roles = "admin,poweruser,user")]
        public IHttpActionResult Get(string area)
        {
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            return Ok(controller.Aree.ListaAree().Where(areaCaricata => areaCaricata.Nome.Equals(area)).Select(a => a.Presenze));
        }

        [Route("{idArea}")]
        [HttpDelete]
        [Authorize(Roles = "admin")]
        public IHttpActionResult Cancella(Int32 idArea)
        {
            using (DB db = new DB())
            {
                if (db.ExecuteSelect(String.Format("select a.id from Aree a inner join Antenne an on an.Area=a.nome where a.id={0}", idArea), delegate (SQLiteDataReader reader) { return reader.HasRows; }))
                {
                    throw new HttpResponseException(new HttpResponseMessage(System.Net.HttpStatusCode.Conflict)
                    {
                        Content = new StringContent("Impossibile eliminare.Ci sono antenne collegate all'area")
                    });
                }
                if (db.ExecuteSelect(String.Format("select a.id from Aree a inner join Aree ap on ap.Principale=a.Nome where a.id={0}", idArea), delegate (SQLiteDataReader reader) { return reader.HasRows; }))
                {
                    throw new HttpResponseException(new HttpResponseMessage(System.Net.HttpStatusCode.Conflict)
                    {
                        Content = new StringContent("Impossibile eliminare.Ci sono aree collegate all'area")
                    });
                }
                db.ExecuteCommand(String.Format("delete from Aree where id ={0}", idArea));
            }
            return Ok();
        }

        [Route("")]
        [HttpPost]
        [Authorize(Roles = "admin")]
        public IHttpActionResult Aggiorna(Area area)
        {
            Int32 lastId;
            using (DB db = new DB())
            {
                if (area.Id != null)
                {
                    db.ExecuteCommand(String.Format("update Aree set Nome='{0}',Principale='{1}' where  Id={2}", area.Nome, area.NomeAreaPrincipale, area.Id));
                }
                else
                {
                    db.ExecuteCommand(String.Format("insert into Aree('Nome', 'Principale') values('{0}', '{1}' )", area.Nome, area.NomeAreaPrincipale));
                }

                lastId = db.ExecuteSelect("select seq from sqlite_sequence where name='Aree'", delegate (SQLiteDataReader reader)
                 {
                     int nuovoId = 0;
                     while (reader.Read())
                     {
                         nuovoId = int.Parse(reader["seq"].ToString());
                     };
                     return nuovoId;
                 });
            }
            return Ok(lastId);
        }
    }
}