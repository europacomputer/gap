﻿using GAP.Controller;
using GAP.Dao;
using GAP.Model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;

namespace GAP.Rest
{
    [RoutePrefix("gate")]
    public class GateController : ApiController
    {
        [Route("")]
        [HttpGet]
        [Authorize(Roles = "admin,poweruser,user")]
        public IHttpActionResult Get()
        {
            Func<SQLiteDataReader, List<Gate>> fillResource = delegate (SQLiteDataReader reader)
            {
                List<Gate> result = new List<Gate>();
                while (reader.Read())
                {
                    Gate g = new Gate(Convert.ToInt32(reader["Id"]), Convert.ToString(reader["Codice"]), Convert.ToString(reader["Nome"]), Convert.ToString(reader["Ip"]), Convert.ToString(reader["porta"]), Convert.ToString(reader["X"]), Convert.ToString(reader["Y"])); result.Add(g);
                }
                return result;
            };
            using (DB db = new DB())
            {
                return Ok(db.ExecuteSelect("select * from Gates", fillResource));
            }
        }

        [Route("")]
        [HttpPost]
        [Authorize(Roles = "admin")]
        public IHttpActionResult Aggiorna(Gate gate)
        {
            Int32 lastId;
            using (DB db = new DB())
            {
                if (db.ExecuteSelect(String.Format("select Id from Gates where Id='{0}'", gate.Id), delegate (SQLiteDataReader reader) { return reader.HasRows; }))
                {
                    db.ExecuteCommand(String.Format("update Gates set Codice='{1}',Nome='{2}',`Ip`='{3}',`Porta`={4},`X`='{5}',`Y`='{6}' where  Id ='{0}'", gate.Id, gate.Codice, gate.Nome, gate.Ip, gate.Porta, gate.X, gate.Y));
                }
                else
                {
                    db.ExecuteCommand(String.Format("insert into Gates('Codice', 'Nome','Ip','Porta','X','Y') values('{0}', '{1}','{2}',{3},'{4}','{5}' )", gate.Codice, gate.Nome, gate.Ip, gate.Porta, gate.X, gate.Y));
                }
                lastId = db.ExecuteSelect("select seq from sqlite_sequence where name='Gates'", delegate (SQLiteDataReader reader)
                {
                    int nuovoId = 0;
                    while (reader.Read())
                    {
                        nuovoId = int.Parse(reader["seq"].ToString());
                    };
                    return nuovoId;
                });
            }
            return Ok(lastId);
        }

        [Route("{area}/out")]
        [HttpGet]
        [Authorize(Roles = "admin,poweruser,user")]
        public IHttpActionResult Get(string tipo, string area)
        {
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            IList<String> gates = new List<String>();
            foreach (Antenna antenna in controller.Antenne)
            {
                if (antenna.Tipo == Antenna.TipoFunzionamento.U && antenna.Area.Nome.ToLower().Equals(area.ToLower()))
                {
                    gates.Add(antenna.CodiceGate);
                }
            }
            return Ok(gates);
        }

        [Route("{codiceGate}/stato")]
        [HttpPost]
        [Authorize(Roles = "admin,poweruser,user")]
        public IHttpActionResult Apri(string codiceGate)
        {
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            controller.Gates[codiceGate].Apri();
            return Ok();
        }

        [Route("{id}")]
        [HttpDelete]
        [Authorize(Roles = "admin")]
        public IHttpActionResult Cancella(Int32 id)
        {
            Int32 lastId;
            using (DB db = new DB())
            {
                if (db.ExecuteSelect(String.Format("select * from Antenne a inner join gates g on g.Codice=a.Gate where g.id={0}", id), delegate (SQLiteDataReader reader) { return reader.HasRows; }))
                {
                    throw new HttpResponseException(new HttpResponseMessage(System.Net.HttpStatusCode.Conflict)
                    {
                        Content = new StringContent("Impossibile eliminare.Ci sono antenne collegate.")
                    });
                }
                db.ExecuteCommand(String.Format("delete from Gates where id ={0}", id));

                lastId = db.ExecuteSelect("select seq from sqlite_sequence where name='Aree'", delegate (SQLiteDataReader reader)
                {
                    int nuovoId = 0;
                    while (reader.Read())
                    {
                        nuovoId = int.Parse(reader["seq"].ToString());
                    };
                    return nuovoId;
                });
            }
            return Ok(lastId);
        }
    }
}