﻿using GAP.Controller;
using GAP.model;
using System.Web.Http;

namespace GAP.rest
{
    [RoutePrefix("antenneconfigurazione")]
    public class AntennaConfigurazioneController : ApiController
    {
        [Route("")]
        [HttpGet]
        [Authorize(Roles = "admin,poweruser,user")]
        public IHttpActionResult Get()
        {
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            if (controller.antennaConfigurazione == null)
            {
                return Ok(new TagConfigReaderWriter("Nessuna antenna per la configurazione dei tag"));
            }
            return Ok(new TagConfigReaderWriter(controller.antennaConfigurazione.Leggi()));
        }

        [Route("restart")]
        [HttpGet]
        [Authorize(Roles = "admin,poweruser,user")]
        public IHttpActionResult Restart()
        {
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            if (controller.antennaConfigurazione == null)
            {
                return Ok(new TagConfigReaderWriter("Nessuna antenna per la configurazione dei tag"));
            }
            controller.antennaConfigurazione.Restart();
            return Ok();
        }

        [Route("{epc}")]
        [HttpGet]
        public IHttpActionResult ScriviEpc(string epc)
        {
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            if (controller.antennaConfigurazione == null)
            {
                return Ok("Nessuna antenna per la configurazione dei tag");
            }
            controller.antennaConfigurazione.Scrivi(epc);
            return Ok();
        }
    }
}