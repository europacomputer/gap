﻿using GAP.Controller;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace GAP.Rest
{
    internal class StatoPeriferica
    {
        private readonly short stato;
        private readonly string nome;
        private readonly string codice;
        private readonly string tipo;
        private readonly string x;
        private readonly string y;

        public StatoPeriferica(short stato, string nome, string codice, string tipo, string x, string y)
        {
            this.stato = stato;
            this.nome = nome;
            this.codice = codice;
            this.tipo = tipo;
            this.x = x;
            this.y = y;
        }

        public string X => x;

        public string Y => y;

        public string Codice => codice;

        public short Stato => stato;

        public string Nome => nome;

        public string Tipo => tipo;
    }

    [RoutePrefix("impianto")]
    public class ImpiantoController : ApiController
    {
        [Route("")]
        [HttpPut]
        public IHttpActionResult Result()
        {
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            controller.Stop();
            controller.StartAsync();
            return Ok();
        }

        [Route("")]
        [HttpGet]
        ////[Authorize(Roles = "admin,poweruser,user")]
        public IHttpActionResult Get()
        {
            List<StatoPeriferica> stati = new List<StatoPeriferica>();
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            stati.AddRange(controller.Antenne.Select(ant => new StatoPeriferica(ant.Stato, ant.Nome, ant.Nome, "ANTENNA", ant.X, ant.Y)));
            stati.AddRange(controller.Gates.Values.Select(gate => new StatoPeriferica(gate.Stato, gate.Nome, gate.Codice, "GATE", gate.X, gate.Y)));
            return Ok(stati);
        }
    }
}