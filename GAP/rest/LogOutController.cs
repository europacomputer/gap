﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace GAP.rest
{
    [RoutePrefix("log")]
    public class LogController : ApiController
    {
        [Route("tail")]
        [HttpGet]
        public IHttpActionResult Log()
        {
            using (FileStream fs = File.Open("gap.log", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                // Seek 1024 bytes from the end of the file
                fs.Seek(-1024, SeekOrigin.End);
                // read 1024 bytes
                byte[] bytes = new byte[1024];
                fs.Read(bytes, 0, 1024);
                // Convert bytes to string
                string s = Encoding.Default.GetString(bytes);
                s = "<HTML>" + Regex.Replace(s, @"\r\n?|\n", "<br/>") + "</HTML>";
                return Ok(s);
            }
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult LogFull()
        {
            return Ok();
        }
    }
}