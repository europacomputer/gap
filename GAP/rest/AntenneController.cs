﻿using GAP.Dao;
using GAP.model.antenne;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Web.Http;
using System.Web.Security;

namespace GAP.rest
{
    [RoutePrefix("antenne")]
    public class AntenneController : ApiController
    {
        [Route("")]
        [HttpPost]
        [Authorize(Roles = "admin")]
        public IHttpActionResult Aggiorna(AntennaDTO antenna)
        {
            int? numero = antenna.Numero;
            using (DB db = new DB())
            {
                {
                    int abilitata = antenna.Abilitata ? 1 : 0;
                    db.ExecuteCommand(String.Format("update Antenne set `Codice`='{0}',`Tipo`='{1}',`Area`='{2}',`Abilitata`='{3}',`Gate`='{4}',`Ip`='{5}',`X`='{6}',`Y`='{7}','Prolunga_presenza'='{8}',configurazioneRilevatoreIngresso='{9}',configurazioneRilevatoreUscita='{10}',pedonale={12} where Numero={11}", antenna.Nome, antenna.TipoCodice, antenna.AreaCodice, abilitata, antenna.CodiceGate, antenna.Ip, antenna.X, antenna.Y, antenna.ProlungaPresenza, antenna.ConfigurazioneRilevatoreIngresso, antenna.ConfigurazioneRilevatoreUscita, antenna.Numero, Convert.ToInt16(antenna.Pedonale)));
                }
                return Ok(numero);
            }
        }

        [Route("")]
        [HttpGet]
        [Authorize(Roles = "admin,poweruser,user")]
        public IHttpActionResult Get()
        {
            Func<SQLiteDataReader, List<AntennaDTO>> fillResource = delegate (SQLiteDataReader reader)
            {
                List<AntennaDTO> result = new List<AntennaDTO>();
                while (reader.Read())
                {
                    Enum.TryParse(reader["Tipo"].ToString(), out Model.Antenna.TipoFunzionamento tipoFunzionamento);
                    AntennaDTO a = new AntennaDTO(Convert.ToInt32(reader["Numero"]), Convert.ToString(reader["Codice"]), Convert.ToString(reader["Gate"]), null, Convert.ToString(reader["Ip"]), tipoFunzionamento, Convert.ToInt32(reader["Prolunga_presenza"]), Convert.ToString(reader["X"]), Convert.ToString(reader["Y"]), Convert.ToBoolean(reader["Abilitata"]), Convert.ToString(reader["configurazioneRilevatoreIngresso"]), Convert.ToString(reader["configurazioneRilevatoreUscita"]), Convert.ToBoolean(reader["Pedonale"]))
                    {
                        AreaCodice = Convert.ToString(reader["Area"])
                    };
                    result.Add(a);
                }
                return result;
            };
            using (DB db = new DB())
            {
                return Ok(db.ExecuteSelect("select * from Antenne", fillResource));
            }
        }
    }
}