﻿using GAP.Controller;
using GAP.Dao;
using GAP.Model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Web.Http;
using System.Web.Security;

namespace GAP.rest
{
    [RoutePrefix("permessi")]
    public class PermessiController : ApiController
    {
        [Route("{Id}")]
        [HttpDelete]
        [Authorize(Roles = "admin,poweruser")]
        public IHttpActionResult Cancella(int Id)
        {
            using (DB db = new DB())
            {
                db.ExecuteCommand("delete from Regole where id=" + Id);
            }
            return Ok();
        }

        [Route("")]
        [HttpPut]
        [Authorize(Roles = "admin,poweruser")]
        public IHttpActionResult Ricarica()
        {
            AccessiController controller = (AccessiController)Configuration.Properties["AccessiController"];
            controller.Aree.CaricaPermessi();
            return Ok();
        }

        [Route("")]
        [HttpPost]
        [Authorize(Roles = "admin,poweruser")]
        public IHttpActionResult Aggiorna(Regola regola)
        {
            int? id = regola.Id;
            using (DB db = new DB())
            {
                Func<SQLiteDataReader, int> fillId = delegate (SQLiteDataReader reader)
                {
                    int nuovoId = 0;
                    while (reader.Read())
                    {
                        nuovoId = int.Parse(reader["seq"].ToString());
                    };
                    return nuovoId;
                };
                if (regola.Id == null)
                {
                    db.ExecuteCommand(String.Format("insert into Regole (TAG1,TAG2,AREA,PERIODO) VALUES ('{0}','{1}','{2}','{3}')", regola.Tag1.Codice, regola.Tag2.Codice, regola.Area, regola.Periodo));
                }
                else
                {
                    db.ExecuteCommand(String.Format("update Regole set TAG1='{0}',TAG2='{1}',AREA='{2}',PERIODO='{3}' where id={4}", regola.Tag1.Codice, regola.Tag2.Codice, regola.Area, regola.Periodo, regola.Id));
                }
                id = db.ExecuteSelect("select seq from sqlite_sequence where name='Regole'", fillId);
                return Ok(id);
            }
        }

        [Route("{tag}")]
        [HttpPost]
        [Authorize(Roles = "admin,poweruser")]
        public IHttpActionResult AggiornaTagConAree(string tag, Area[] aree)
        {
            using (DB db = new DB())
            {
                db.ExecuteCommand(String.Format("delete from regole where tag1='{0}' and tag2='' and periodo='' ", tag));
                if (aree != null)
                {
                    foreach (Area area in aree)
                    {
                        Console.WriteLine(area.Nome);
                        db.ExecuteCommand(String.Format("insert into Regole (TAG1,TAG2,AREA,PERIODO) VALUES ('{0}','','{1}','')", tag, area.Nome));
                    }
                }
                return Ok();
            }
        }

        [Route("")]
        [HttpGet]
        [Authorize(Roles = "admin,poweruser,user")]
        public IHttpActionResult Get()
        {
            Func<SQLiteDataReader, List<Regola>> fillResource = delegate (SQLiteDataReader reader)
            {
                List<Regola> result = new List<Regola>();
                while (reader.Read())
                {
                    Regola r = new Regola
                    {
                        Id = int.Parse(reader["Id"].ToString()),
                        Tag1 = new Tag(reader["tag1"].ToString()),
                        Tag2 = new Tag(reader["tag2"].ToString()),
                        Periodo = reader["periodo"].ToString(),
                        Area = reader["area"].ToString(),
                        AnagraficaTag1 = Convert.ToString(reader["anagraficaTag1"])
                    };
                    result.Add(r);
                }
                return result;
            };
            using (DB db = new DB())
            {
                return Ok(db.ExecuteSelect("select r.*,a.codice || ' - ' || a.denominazione as anagraficaTag1 from Regole r left join Anagrafiche a on a.tag=r.tag1", fillResource));
            }
        }

        [Route("{tag}")]
        [HttpGet]
        [Authorize(Roles = "admin,poweruser,user")]
        public IHttpActionResult GetByTag(string tag)
        {
            Func<SQLiteDataReader, List<Area>> fillResource = delegate (SQLiteDataReader reader)
            {
                List<Area> result = new List<Area>();
                while (reader.Read())
                {
                    result.Add(new Area(null, Convert.ToString(reader["area"])));
                }
                return result;
            };
            using (DB db = new DB())
            {
                if (String.IsNullOrWhiteSpace(tag))
                {
                    return Ok(new List<Area>());
                }
                return Ok(db.ExecuteSelect(String.Format("select area from regole where tag1='{0}' and tag2='' and periodo=''", tag), fillResource));
            }
        }
    }
}