import { NgModule, SystemJsNgModuleLoader } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AppRoutes } from './app.routes';

import { BreadcrumbService } from './breadcrumb.service';
import { AppComponent } from './app.component';
import { AppMenuComponent, AppSubMenuComponent } from './app.menu.component';
import { AppBreadcrumbComponent } from './app.breadcrumb.component';
import { AppFooterComponent } from './app.footer.component';
import { AppTopBarComponent } from './app.topbar.component';
import { ScrollPanelModule } from 'primeng/primeng';
import { AnagraficheService } from './service/anagrafiche.service';
import { AntenneService } from './service/antenne.service';
import { AreeService } from './service/aree.service';
import { ConfiguratoreTagService } from './service/configuratoretag.service';
import { ConfigurazioneService } from './service/configurazione.service';
import { GateService } from './service/gate.service';
import { ImpiantoService } from './service/impianto.service';
import { PermessiService } from './service/permessi.service';
import { PresenzeComponent } from './views/presenze/presenze.component';
import { DxDataGridModule, DxTemplateModule, DxBulletModule } from 'devextreme-angular';
import { Service } from './views/presenze/app.service';
import { AuditComponent } from './views/audit/audit.component';
import { AnagraficheComponent } from './views/anagrafiche/anagrafiche.component';
import { PermessiComponent } from './views/permessi/permessi.component';
import { ConfigurazioneComponent } from './views/configurazione/configurazione.component';
import { AboutComponent } from './views/about/about.component';


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutes,
        HttpClientModule,
        BrowserAnimationsModule,
        ScrollPanelModule,
        DxDataGridModule,
        DxTemplateModule,
        DxBulletModule,

    ],
    declarations: [
        AuditComponent,
        PresenzeComponent,
        AppComponent,
        AppMenuComponent,
        AppBreadcrumbComponent,
        AppFooterComponent,
        AppTopBarComponent,
        AppSubMenuComponent,
        AuditComponent,
        AnagraficheComponent,
        PermessiComponent,
        ConfigurazioneComponent,
        AboutComponent],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        BreadcrumbService,
        AnagraficheService,
        AntenneService,
        AreeService,
        ConfiguratoreTagService,
        ConfigurazioneService,
        GateService,
        ImpiantoService,
        PermessiService,
        Service
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
