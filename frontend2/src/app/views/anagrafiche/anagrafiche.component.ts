import { Component, OnInit } from '@angular/core';
import { AnagraficheService } from '../../service/anagrafiche.service';
import { Anagrafica } from '../../domain/Anagrafica';
import CustomStore from 'devextreme/data/custom_store';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

const API_URL = environment.apiUrl;

@Component({
    selector: 'app-anagrafiche',
    templateUrl: './anagrafiche.component.html',
    styleUrls: ['./anagrafiche.component.css']
})
export class AnagraficheComponent {
    anagrafiche: Array<Anagrafica>;
    dialogEdit = false;
    anagraficaInEdit: Anagrafica;
    displayTagWriter = false;
    store: CustomStore;
    dataSource: any = {};

    constructor(public anagraficaService: AnagraficheService) {
        this.store = new CustomStore({
            load: function () {
                return anagraficaService.caricaAnagrafiche().toPromise();
            },
            remove: function (anagrafica: Anagrafica) {
                return anagraficaService.cancellaAnagrafica(anagrafica.id).toPromise();
            },
        });
        this.dataSource.store = this.store;
    }

    onContentReady(e) {
        e.component.columnOption('command:edit', {
            visibleIndex: -1
        });
    }
}
