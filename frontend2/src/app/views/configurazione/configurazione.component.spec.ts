import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurazioneComponent } from './configurazione.component';

describe('ConfigurazioneComponent', () => {
  let component: ConfigurazioneComponent;
  let fixture: ComponentFixture<ConfigurazioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurazioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurazioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
