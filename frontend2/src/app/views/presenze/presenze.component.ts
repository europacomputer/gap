import DataSource from 'devextreme/data/data_source';
import { Service } from './app.service';
import { Component } from '@angular/core';

@Component({
    selector: 'app-presenze',
    templateUrl: './presenze.component.html',
    styleUrls: ['./presenze.component.css']
})
export class PresenzeComponent {
    dataSource: DataSource;
    collapsed = false;
    contentReady = (e) => {
        if (!this.collapsed) {
            this.collapsed = true;
            e.component.expandRow(['EnviroCare']);
        }
    }
    customizeTooltip = (pointsInfo) => {
        return { text: parseInt(pointsInfo.originalValue, 10) + '%' };
    }

    constructor(service: Service) {
        this.dataSource = service.getDataSource();
    }
}
