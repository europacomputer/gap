import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { PresenzeComponent } from './views/presenze/presenze.component';
import { AuditComponent } from './views/audit/audit.component';
import { AnagraficheComponent } from './views/anagrafiche/anagrafiche.component';
import { PermessiComponent } from './views/permessi/permessi.component';
import { ConfigurazioneComponent } from './views/configurazione/configurazione.component';
import { AboutComponent } from './views/about/about.component';

export const routes: Routes = [
    { path: '', component: PresenzeComponent },
    { path: 'audit', component: AuditComponent },
    { path: 'anagrafiche', component: AnagraficheComponent },
    { path: 'permessi', component: PermessiComponent },
    { path: 'configurazione', component: ConfigurazioneComponent },
    { path: 'about', component: AboutComponent }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
