import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StatoImpianto } from '../domain/stato.impianto';

const API_URL = environment.apiUrl;

@Injectable()
export class ImpiantoService {
    constructor(private http: HttpClient) { }

    restart(): void {
        this.http
            .put(`${API_URL}impianto`, {})
            .subscribe();
    }

    public logout(): Observable<Object> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        }).set('authorization', 'Basic xxx');
        return this.http
            .get(API_URL + 'gate', {
                headers: headers
            });
    }

    caricaImpianto(): Observable<Array<StatoImpianto>> {
        return this.http
            .get<Array<StatoImpianto>>(`${API_URL}impianto`);
    }
}
