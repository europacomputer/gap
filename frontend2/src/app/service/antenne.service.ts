import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Antenna } from '../domain/Antenna';

const API_URL = environment.apiUrl;

@Injectable()
export class AntenneService {
    constructor(private http: HttpClient) { }

    public caricaAntenne(): Observable<Array<Antenna>> {
        return this.http.get<Array<Antenna>>(`${API_URL}antenne`);
    }

    salvaAntenne(antenna: Antenna): Observable<Object> {
        return this.http.post(`${API_URL}antenne`, antenna);
    }
}
