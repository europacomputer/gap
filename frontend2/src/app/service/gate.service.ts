import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Gate } from '../domain/Gate';

const API_URL = environment.apiUrl;

@Injectable()
export class GateService {
    constructor(private http: HttpClient) { }

    cancellaGate(gate: Gate): Observable<Number> {
        return this.http.delete<Number>(`${API_URL}gate/${gate.Id}`);
    }

    public caricaGates(): Observable<Array<Gate>> {
        return this.http
            .get<Array<Gate>>(`${API_URL}gate`);
    }

    salvaGate(gate: Gate): Observable<Object> {
        return this.http.post(`${API_URL}gate`, gate);
    }

    public apri(codiceGate: String): void {
        this.http
            .post(`${API_URL}gate/${codiceGate}/stato`, {}, {}).subscribe();
    }
}
