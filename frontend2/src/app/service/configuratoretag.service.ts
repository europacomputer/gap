import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TagConfigReaderWriter } from '../domain/TagConfigReaderWriter';

const API_URL = environment.apiUrl;

@Injectable()
export class ConfiguratoreTagService {
    constructor(private http: HttpClient) { }
    leggiTag(): Observable<TagConfigReaderWriter> {
        return this.http
            .get<TagConfigReaderWriter>(`${API_URL}antenneconfigurazione`);
    }

    reset(): Observable<void> {
        return this.http
            .get<void>(`${API_URL}antenneconfigurazione/restart`);
    }

    scriviTag(tag: String): Observable<void> {
        return this.http
            .get<void>(`${API_URL}antenneconfigurazione/${tag}`);
    }

}
