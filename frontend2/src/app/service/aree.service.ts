import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Area } from '../domain/Area';
import { Presenza } from '../domain/Presenza';
const API_URL = environment.apiUrl;

@Injectable()
export class AreeService {
    constructor(private http: HttpClient) { }

    public caricaAree(): Observable<Array<Area>> {
        return this.http
            .get<Array<Area>>(`${API_URL}area`);
    }

    cancellaArea(area: Area): Observable<Object> {
        return this.http.delete(`${API_URL}area/${area.Id}`);
    }

    public caricaAreePrincipale(): Observable<Array<Area>> {
        return this.http
            .get<Array<Area>>(`${API_URL}area/principali`);
    }

    public caricaStato(): Observable<Array<Area>> {
        return this.http
            .get<Array<Area>>(`${API_URL}area/stato`);
    }

    public caricaStatoHtml(): Observable<String> {
        return this.http
            .get<String>(`${API_URL}area/stato/html`);
    }

    public forzaEntrata(
        codiceArea: String,
        nomeIngresso: String,
        notaIngresso: String
    ): Observable<Object> {
        return this.http.request(
            'put',
            `${API_URL}area/${codiceArea}/${nomeIngresso}`,
            { body: '"' + notaIngresso + '"' }
        );
    }

    public forzaUscita(
        presenza: Presenza,
        nota: string,
        gate: String
    ): Observable<Object> {
        return this.http.request(
            'put',
            `${API_URL}area/${presenza.NomeArea}/${gate}/${
            presenza.Tag.Codice
            }`,
            { body: '"' + nota + '"' }
        );
    }

    salvaArea(area: Area): Observable<Object> {
        return this.http.post(`${API_URL}area`, area);
    }

}
