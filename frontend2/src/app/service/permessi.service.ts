import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Area } from '../domain/Area';
import { Regola } from '../domain/Regola';

const API_URL = environment.apiUrl;

@Injectable()
export class PermessiService {
    constructor(private http: HttpClient) { }

    caricaAreeRegole(tag: String): Observable<Array<Area>> {
        return this.http
            .get<Array<Area>>(`${API_URL}permessi/${tag}`);
    }

    caricaRegole(): Observable<Array<Regola>> {
        return this.http
            .get<Array<Regola>>(`${API_URL}permessi`);
    }

    salvaRegola(regola: Regola): Observable<Object> {
        return this.http.post(`${API_URL}permessi`, regola);
    }

    associaAree(tag: String, areeSelezionate: Array<Area>): Observable<Object> {
        return this.http.post(`${API_URL}permessi/${tag}`, areeSelezionate);
    }

    applicaRegole(): void {
        this.http.put(`${API_URL}permessi`, null).subscribe();
    }

    cancellaRegola(regola: Regola): Observable<Object> {
        return this.http.delete(`${API_URL}permessi/${regola.Id}`);
    }
}
