import { environment } from 'src/environments/environment';
import { Anagrafica } from '../domain/Anagrafica';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const API_URL = environment.apiUrl;

@Injectable()
export class AnagraficheService {
    salvaAnagrafica(anagrafica: Anagrafica): Observable<Object> {
        return this.http
            .post(`${API_URL}anagrafiche`, anagrafica);
    }

    applicaRegole(): void {
        this.http.put(`${API_URL}anagrafiche`, null).subscribe();
    }

    cancellaAnagrafica(id: Number): Observable<Object> {
        return this.http.delete(`${API_URL}anagrafiche/${id}`);
    }

    constructor(private http: HttpClient) { }
    public caricaAnagrafiche(): Observable<Array<Anagrafica>> {
        return this.http
            .get<Array<Anagrafica>>(`${API_URL}anagrafiche`);
    }

    public ricercaAnagrafiche(query: string): Observable<Array<String>> {
        return this.http
            .get<Array<String>>(`${API_URL}anagrafiche/${query}`);
    }
}
