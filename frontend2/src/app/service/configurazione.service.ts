import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Configurazione } from '../domain/Configurazione';

const API_URL = environment.apiUrl;
@Injectable()
export class ConfigurazioneService {
    constructor(private http: HttpClient) { }

    public caricaConfigurazione(): Observable<Configurazione> {
        return this.http
            .get<Configurazione>(API_URL + `configurazione`);
    }

    public testMail(): Observable<String> {
        return this.http.get<String>(`${API_URL}configurazione/mail/test`, {});
    }

    public salvaConfigurazione(
        configurazione: Configurazione
    ): Observable<void> {
        return this.http.put<void>(`${API_URL}configurazione`, configurazione);
    }
}
