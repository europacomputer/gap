import { Tag } from './Tag';
import { Anagrafica } from './Anagrafica';

export class Presenza {
    Tag: Tag;
    NomeArea: string;
    Data: number;
    DataAzione: Date;
    Anagrafica: Anagrafica;
    Durata: number;
    Descrizione: String;

    get DurataValida(): boolean {
        console.log(this.Durata);
        return this.Durata < 2;
    }
}
