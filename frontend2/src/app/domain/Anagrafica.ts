import { Regola } from './Regola';

export class Anagrafica {
    id: number;
    codice: string;
    denominazione: string;
    tag: string;
    rilevaInAudit: boolean;
    pedonale: boolean;
    codiceCollegato: string;
    regole: any;

    public constructor(anagrafica?: Anagrafica) {
        if (anagrafica !== undefined) {
            this.codice = anagrafica.codice;
            this.denominazione = anagrafica.denominazione;
            this.tag = anagrafica.tag;
            this.rilevaInAudit = anagrafica.rilevaInAudit;
            this.pedonale = anagrafica.pedonale;
            this.codiceCollegato = anagrafica.codiceCollegato;
        }
    }
}
