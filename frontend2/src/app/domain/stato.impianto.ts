export class StatoImpianto {
    Stato: Number;
    Nome: String;
    Tipo: String;
    Codice: String;
    X: String;
    Y: String;
}
