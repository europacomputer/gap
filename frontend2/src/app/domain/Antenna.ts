
export class Antenna {
    Stato: number;
    AreaCodice: string;
    Ip: string;
    TipoCodice: string;
    X: string;
    Y: string;
    Abilitata: boolean;
    Pedonale: boolean;
    Nome: string;
    CodiceGate: string;
    Numero: number;
    ProlungaPresenza: number;
    ConfigurazioneRilevatoreIngresso: string;
    ConfigurazioneRilevatoreUscita: string;

    public constructor(antenna?: Antenna) {
        if (antenna !== undefined) {
            this.Ip = antenna.Ip;
            this.Pedonale = antenna.Pedonale;
            this.TipoCodice = antenna.TipoCodice;
            this.X = antenna.X;
            this.Y = antenna.Y;
            this.AreaCodice = antenna.AreaCodice;
            this.Abilitata = antenna.Abilitata;
            this.Nome = antenna.Nome;
            this.CodiceGate = antenna.CodiceGate;
            this.Numero = antenna.Numero;
            this.ProlungaPresenza = antenna.ProlungaPresenza;
            this.ConfigurazioneRilevatoreIngresso =
                antenna.ConfigurazioneRilevatoreIngresso;
            this.ConfigurazioneRilevatoreUscita =
                antenna.ConfigurazioneRilevatoreUscita;
        }
    }
}
