export class Configurazione {
    MinutiWarning: String;
    OrarioMail: String;
    MailAddress: String;
    ServerMail: String;
    UserMail: String;
    PasswordMail: String;
    PortMail: String;
    From: String;
    AntennaConfigurazione: String;
}
