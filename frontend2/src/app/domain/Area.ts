import { Presenza } from './Presenza';
import { Gate } from './Gate';

export class Area {
    Nome: string;
    Presenze?: Array<Presenza>;
    Gates?: Array<Gate>;
    NomeAreaPrincipale?: String = '';
    Id?: Number;

    public constructor(area?: Area) {
        if (area !== undefined) {
            this.Id = area.Id;
            this.Nome = area.Nome;
            this.Presenze = area.Presenze;
            this.NomeAreaPrincipale = area.NomeAreaPrincipale;
        }
    }
}
