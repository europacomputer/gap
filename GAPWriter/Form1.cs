﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SQLite;
using System.Threading;
using System.ComponentModel;
using System.Net;

namespace GAPWriter
{
    public partial class frmMain : Form
    {
        private int comPort;
        private byte fComAdr;
        private string ultimoAssegnato;
        private string dbPath;
        private SQLiteConnection connessione;
        private SQLiteConnectionStringBuilder connectString;

        public frmMain()
        {
            InitializeComponent();
        }

        private void LeggiEPC()
        {
            try
            {
                int CardNum = 0;
                int Totallen = 0;
                int EPClen;
                int m;
                byte[] EPC = new byte[5000];
                int CardIndex;
                string temps;
                string sEPC;
                byte AdrTID = 0;
                byte LenTID = 0;
                byte TIDFlag = 0;
                int fCmdRet = StaticClassReader.Inventory_G2(ref fComAdr, AdrTID, LenTID, TIDFlag, EPC, ref Totallen, ref CardNum, comPort);
                if ((fCmdRet == 1) | (fCmdRet == 2) | (fCmdRet == 3) | (fCmdRet == 4) | (fCmdRet == 0xFB))
                {
                    byte[] daw = new byte[Totallen];
                    Array.Copy(EPC, daw, Totallen);
                    temps = ByteArrayToHexString(daw);
                    m = 0;
                    if (CardNum == 0)
                    {
                        lblEPC.Text = "";
                        txtNuovoEpc.Text = "";
                    }
                    for (CardIndex = 0; CardIndex < CardNum; CardIndex++)
                    {
                        EPClen = daw[m];
                        sEPC = temps.Substring(m * 2 + 2, EPClen * 2);
                        m = m + EPClen + 1;
                        try
                        {
                            if (sEPC.Length != EPClen * 2)
                                return;
                            if (!String.IsNullOrEmpty(sEPC))
                            {
                                if (lblEPC.Text != sEPC)
                                {
                                    lblEPC.Text = sEPC;
                                }
                                if (!String.IsNullOrEmpty(lblEPC.Text) && ultimoAssegnato != lblEPC.Text && chkSerie.Checked)
                                {
                                    Scrivi();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Errore nell leggere l'antenna " + ex.Message);
            }
        }

        private void LeggiTID()
        {
            String str = lblEPC.Text;
            if (String.IsNullOrEmpty(str))
            {
                lblTID.Text = "";
            }
            byte ENum = Convert.ToByte(str.Length / 4);
            byte EPClength = Convert.ToByte(str.Length / 2);
            byte[] EPC = new byte[ENum];
            EPC = HexStringToByteArray(str);
            byte Num = Convert.ToByte("6");
            byte[] CardData = new byte[320];
            byte[] fPassWord = HexStringToByteArray("");
            int ferrorcode = 0;
            int fCmdRet = StaticClassReaderB.ReadCard_G2(ref fComAdr, EPC, 2, Convert.ToByte("0", 16), Num, fPassWord, Convert.ToByte("0", 16), Convert.ToByte("0", 16), 0, CardData, EPClength, ref ferrorcode, comPort);
            if (fCmdRet == 0)
            {
                byte[] daw = new byte[Num * 2];
                Array.Copy(CardData, daw, Num * 2);
                lblTID.Text = ByteArrayToHexString(daw);
                if (chkUsaTid.Checked)
                {
                    txtNuovoEpc.Text = lblTID.Text;
                }
            }
            if (ferrorcode != -1)
            {
                Console.WriteLine(ferrorcode);
            }
        }

        private void tmrAntenna_Tick(object sender, EventArgs e)
        {
            LeggiEPC();
            LeggiTID();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            System.Configuration.Configuration c = ConfigurationManager.OpenExeConfiguration(
        ConfigurationUserLevel.None) as Configuration;
            txtIp.Text = c.AppSettings.Settings["ip"].Value.ToString();
            dbPath = c.AppSettings.Settings["dbPath"].Value.ToString();
            ColumnHeader header = new ColumnHeader();
            header.Text = "Codice";
            header.Width = 100;
            header.TextAlign = HorizontalAlignment.Center;
            lstAnagrafica.Columns.Add(header);

            header = new ColumnHeader();
            header.Text = "Anagrafica";
            header.Width = 380;
            header.TextAlign = HorizontalAlignment.Left;
            lstAnagrafica.Columns.Add(header);

            header = new ColumnHeader();
            header.Text = "Tag";
            header.Width = 220;
            header.TextAlign = HorizontalAlignment.Left;
            lstAnagrafica.Columns.Add(header);

            lstAnagrafica.View = View.Details;

            connectString = new SQLiteConnectionStringBuilder
            {
                DataSource = dbPath,
                ForeignKeys = false,
                JournalMode = SQLiteJournalModeEnum.Wal
            };
        }

        private void CaricaAnagrafiche()
        {
            lstAnagrafica.Items.Clear();
            using (connessione = new SQLiteConnection(connectString.ToString()))
            {
                connessione.Open();
                String sql = "select Codice, Denominazione, Tag from Anagrafiche where 1=1 ";
                if (txtCercaAnagrafica.Text.Length > 0)
                {
                    sql += " and Denominazione like '%" + txtCercaAnagrafica.Text.Replace("'", "''") + "%'";
                    sql += " or Tag like '%" + txtCercaAnagrafica.Text.Replace("'", "''") + "%'";
                    sql += " or Codice like '%" + txtCercaAnagrafica.Text.Replace("'", "''") + "%'";
                }
                sql += " order by codice";
                using (SQLiteCommand command = new SQLiteCommand(sql, connessione))
                {
                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ListViewItem item = new ListViewItem(new[] { reader["Codice"].ToString(), reader["Denominazione"].ToString(), reader["Tag"].ToString() });
                        lstAnagrafica.Items.Add(item);
                    }
                }
            }
        }

        private string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            return sb.ToString().ToUpper();
        }

        private byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        private void cmdScrivi_Click(object sender, EventArgs e)
        {
            if (tmrAntenna.Enabled)
            {
                Scrivi();
            }
        }

        private void Scrivi()
        {
            Int32 numEPC = -1;
            //if (!Int32.TryParse(txtNuovoEpc.Text, out numEPC) && !chkUsaTid.Checked)
            //{
            //   return;
            //}

            tmrAntenna.Enabled = false;
            try
            {
                byte[] WriteEPC = new byte[100];
                byte WriteEPClen;
                byte ENum;
                string nuovoEPC = txtNuovoEpc.Text.PadLeft(16, '0');
                if ((nuovoEPC.Length % 4) != 0)
                {
                    MessageBox.Show("La lunghezza del codice deve essere un multiplo di 4. Attuale:" + nuovoEPC.Length, "Informazione");
                    return;
                }
                WriteEPClen = Convert.ToByte(nuovoEPC.Length / 2);
                ENum = Convert.ToByte(nuovoEPC.Length / 4);
                byte[] EPC = new byte[ENum];
                EPC = HexStringToByteArray(nuovoEPC);
                int ferrorcode = 0;
                int fCmdRet = StaticClassReaderB.WriteEPC_G2(ref fComAdr, HexStringToByteArray("00000000"), EPC, WriteEPClen, ref ferrorcode, comPort);
                if (fCmdRet == 0)
                {
                    ultimoAssegnato = nuovoEPC;
                    if (chkSerie.Checked)
                    {
                        AumentaEPC();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // throw ex;
            }
            finally
            {
                tmrAntenna.Enabled = true;
            }
        }

        private void AumentaEPC()
        {
            Int32 numEPC = -1;
            if (Int32.TryParse(txtNuovoEpc.Text, out numEPC))
            {
                numEPC++;
                txtNuovoEpc.Text = numEPC.ToString();
            }
        }

        private void chkSerie_CheckedChanged(object sender, EventArgs e)
        {
            ultimoAssegnato = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cmdConnecti.Text == "DISCONNETTI")
            {
                tmrAntenna.Enabled = false;
                StaticClassReaderB.CloseNetPort(comPort);
                lblEPC.Text = "";
                lblTID.Text = "";
                cmdConnecti.Text = "CONNETTI";
            }
            else
            {
                fComAdr = Convert.ToByte("FF", 16); // $FF;
                byte fBaud = 0;
                int openresult = StaticClassReaderB.OpenNetPort(6000, txtIp.Text, ref fBaud, ref comPort);
                //int openresult = Sta/ticClassReaderB.OpenComPort(6,)
                if (openresult != 0)
                {
                    Console.WriteLine("Erore apertura antenna" + openresult);
                }
                tmrAntenna.Enabled = true;
                cmdConnecti.Text = "DISCONNETTI";
            }
        }

        private void lstAnagrafica_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lstAnagrafica.SelectedItems.Count > 0)
                {
                    ListViewItem item = lstAnagrafica.SelectedItems[0];
                    if (!chkUsaTid.Checked)
                    {
                        if (!Int32.TryParse(item.SubItems[0].Text, out int codiceAnagrafica) || lblEPC.Text == "")
                        {
                            return;
                        }
                        txtNuovoEpc.Text = item.SubItems[0].Text;
                    }
                    else
                    {
                        txtNuovoEpc.Text = lblTID.Text;
                    }

                    if (chkAbilitaScrittura.Checked)
                    {
                        if (lstAnagrafica.SelectedItems.Count > 0)
                        {
                            //Verifico se la scrittura è andata a buon fine ..altrimenti non faccio nulla
                            int tentativi = 0;
                            while (tentativi < 10)
                            {
                                Scrivi();
                                Thread.Sleep(50);
                                tentativi++;
                                if (lblEPC.Text == txtNuovoEpc.Text)
                                {
                                    tentativi = 10;
                                    item.SubItems[2].Text = txtNuovoEpc.Text;
                                    using (connessione = new SQLiteConnection(connectString.ToString()))
                                    {
                                        connessione.Open();
                                        using (SQLiteCommand command = new SQLiteCommand("update Anagrafiche set Tag='" + txtNuovoEpc.Text + "' where codice='" + item.SubItems[0].Text + "'", connessione))
                                        {
                                            SQLiteDataReader reader = command.ExecuteReader();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Errore " + ex);
            }
        }

        private void TabEPC_SelectedIndexChanged(object sender, EventArgs e)
        {
            chkSerie.Checked = false;
        }

        private void lstAnagrafica_DoubleClick(object sender, EventArgs e)
        {
            if (lstAnagrafica.SelectedItems.Count > 0)
            {
                ListViewItem item = lstAnagrafica.SelectedItems[0];
                using (connessione = new SQLiteConnection(connectString.ToString()))
                {
                    connessione.Open();
                    //Verifico che il tag non sia già associato
                    if (lblEPC.Text.Length > 0)
                    {
                        using (SQLiteCommand commandDuplicati = new SQLiteCommand("select Denominazione from Anagrafiche where Tag='" + lblEPC.Text + "'", connessione))
                        {
                            SQLiteDataReader readerDuplicati = commandDuplicati.ExecuteReader();
                            string denomPresente = "";
                            while (readerDuplicati.Read())
                            {
                                denomPresente = readerDuplicati["Denominazione"].ToString();
                            }
                            if (denomPresente.Length > 0)
                            {
                                MessageBox.Show("Il tag è già associato a " + denomPresente);
                                return;
                            }
                        }
                    }

                    using (SQLiteCommand command = new SQLiteCommand("update Anagrafiche set Tag='" + lblEPC.Text + "' where codice='" + item.SubItems[0].Text + "'", connessione))
                    {
                        SQLiteDataReader reader = command.ExecuteReader();
                    }
                    try
                    {
                        WebRequest req = WebRequest.Create(@"http://localhost:9000/anagrafiche");
                        req.Method = "PUT";
                        req.ContentLength = 0;
                        //req.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes("username:password"));
                        //req.Credentials = new NetworkCredential("username", "password");req.
                        HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Errore nell'applicare le modifiche al server. Andare nella schermata anagrafica del server e premere applica.");
                    }
                }
                item.SubItems[2].Text = lblEPC.Text;
            }
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            CaricaAnagrafiche();
        }

        private void cmdTutti_Click(object sender, EventArgs e)
        {
            txtCercaAnagrafica.Text = "";
            CaricaAnagrafiche();
        }

        private void cmdNuovaAnagrafica_Click(object sender, EventArgs e)
        {
            FrmAnagrafica frmAnagrafica = new FrmAnagrafica();
            frmAnagrafica.ShowDialog(this);
        }
    }
}