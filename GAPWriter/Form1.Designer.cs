﻿namespace GAPWriter
{
    partial class frmMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmrAntenna = new System.Windows.Forms.Timer(this.components);
            this.TabEPC = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.chkSerie = new System.Windows.Forms.CheckBox();
            this.cmdScrivi = new System.Windows.Forms.Button();
            this.txtNuovoEpc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cmdNuovaAnagrafica = new System.Windows.Forms.Button();
            this.cmdTutti = new System.Windows.Forms.Button();
            this.cmdOK = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCercaAnagrafica = new System.Windows.Forms.TextBox();
            this.chkAbilitaScrittura = new System.Windows.Forms.CheckBox();
            this.lstAnagrafica = new System.Windows.Forms.ListView();
            this.cmdConnecti = new System.Windows.Forms.Button();
            this.txtIp = new System.Windows.Forms.TextBox();
            this.lblIp = new System.Windows.Forms.Label();
            this.lblEPC = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTID = new System.Windows.Forms.TextBox();
            this.chkUsaTid = new System.Windows.Forms.CheckBox();
            this.TabEPC.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmrAntenna
            // 
            this.tmrAntenna.Interval = 50;
            this.tmrAntenna.Tick += new System.EventHandler(this.tmrAntenna_Tick);
            // 
            // TabEPC
            // 
            this.TabEPC.Controls.Add(this.tabPage1);
            this.TabEPC.Controls.Add(this.tabPage2);
            this.TabEPC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabEPC.Location = new System.Drawing.Point(16, 173);
            this.TabEPC.Margin = new System.Windows.Forms.Padding(4);
            this.TabEPC.Name = "TabEPC";
            this.TabEPC.SelectedIndex = 0;
            this.TabEPC.Size = new System.Drawing.Size(972, 466);
            this.TabEPC.TabIndex = 12;
            this.TabEPC.SelectedIndexChanged += new System.EventHandler(this.TabEPC_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.chkSerie);
            this.tabPage1.Controls.Add(this.cmdScrivi);
            this.tabPage1.Controls.Add(this.txtNuovoEpc);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(964, 428);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Lettura/Scrittura";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // chkSerie
            // 
            this.chkSerie.AutoSize = true;
            this.chkSerie.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSerie.Location = new System.Drawing.Point(221, 59);
            this.chkSerie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkSerie.Name = "chkSerie";
            this.chkSerie.Size = new System.Drawing.Size(223, 29);
            this.chkSerie.TabIndex = 16;
            this.chkSerie.Text = "Avvia scrittura in serie";
            this.chkSerie.UseVisualStyleBackColor = true;
            this.chkSerie.Click += new System.EventHandler(this.chkSerie_CheckedChanged);
            // 
            // cmdScrivi
            // 
            this.cmdScrivi.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdScrivi.Location = new System.Drawing.Point(671, 6);
            this.cmdScrivi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmdScrivi.Name = "cmdScrivi";
            this.cmdScrivi.Size = new System.Drawing.Size(151, 37);
            this.cmdScrivi.TabIndex = 14;
            this.cmdScrivi.Text = "&SCRIVI";
            this.cmdScrivi.UseVisualStyleBackColor = true;
            this.cmdScrivi.Click += new System.EventHandler(this.cmdScrivi_Click);
            // 
            // txtNuovoEpc
            // 
            this.txtNuovoEpc.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNuovoEpc.Location = new System.Drawing.Point(221, 6);
            this.txtNuovoEpc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNuovoEpc.Name = "txtNuovoEpc";
            this.txtNuovoEpc.Size = new System.Drawing.Size(443, 38);
            this.txtNuovoEpc.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(201, 36);
            this.label2.TabIndex = 12;
            this.label2.Text = "NUOVO EPC:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cmdNuovaAnagrafica);
            this.tabPage2.Controls.Add(this.cmdTutti);
            this.tabPage2.Controls.Add(this.cmdOK);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.txtCercaAnagrafica);
            this.tabPage2.Controls.Add(this.chkAbilitaScrittura);
            this.tabPage2.Controls.Add(this.lstAnagrafica);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(964, 428);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Anagrafica";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cmdNuovaAnagrafica
            // 
            this.cmdNuovaAnagrafica.Location = new System.Drawing.Point(735, 8);
            this.cmdNuovaAnagrafica.Name = "cmdNuovaAnagrafica";
            this.cmdNuovaAnagrafica.Size = new System.Drawing.Size(106, 31);
            this.cmdNuovaAnagrafica.TabIndex = 18;
            this.cmdNuovaAnagrafica.Text = "NUOVO";
            this.cmdNuovaAnagrafica.UseVisualStyleBackColor = true;
            this.cmdNuovaAnagrafica.Click += new System.EventHandler(this.cmdNuovaAnagrafica_Click);
            // 
            // cmdTutti
            // 
            this.cmdTutti.Location = new System.Drawing.Point(633, 8);
            this.cmdTutti.Name = "cmdTutti";
            this.cmdTutti.Size = new System.Drawing.Size(96, 31);
            this.cmdTutti.TabIndex = 17;
            this.cmdTutti.Text = "TUTTI";
            this.cmdTutti.UseVisualStyleBackColor = true;
            this.cmdTutti.Click += new System.EventHandler(this.cmdTutti_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.Location = new System.Drawing.Point(552, 8);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(75, 31);
            this.cmdOK.TabIndex = 16;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = true;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 25);
            this.label4.TabIndex = 15;
            this.label4.Text = "Cerca";
            // 
            // txtCercaAnagrafica
            // 
            this.txtCercaAnagrafica.Location = new System.Drawing.Point(103, 8);
            this.txtCercaAnagrafica.Name = "txtCercaAnagrafica";
            this.txtCercaAnagrafica.Size = new System.Drawing.Size(442, 30);
            this.txtCercaAnagrafica.TabIndex = 14;
            // 
            // chkAbilitaScrittura
            // 
            this.chkAbilitaScrittura.AutoSize = true;
            this.chkAbilitaScrittura.Location = new System.Drawing.Point(839, 10);
            this.chkAbilitaScrittura.Margin = new System.Windows.Forms.Padding(4);
            this.chkAbilitaScrittura.Name = "chkAbilitaScrittura";
            this.chkAbilitaScrittura.Size = new System.Drawing.Size(113, 29);
            this.chkAbilitaScrittura.TabIndex = 13;
            this.chkAbilitaScrittura.Text = "Aggiorna";
            this.chkAbilitaScrittura.UseVisualStyleBackColor = true;
            this.chkAbilitaScrittura.Visible = false;
            // 
            // lstAnagrafica
            // 
            this.lstAnagrafica.Location = new System.Drawing.Point(8, 46);
            this.lstAnagrafica.Margin = new System.Windows.Forms.Padding(4);
            this.lstAnagrafica.Name = "lstAnagrafica";
            this.lstAnagrafica.Size = new System.Drawing.Size(944, 374);
            this.lstAnagrafica.TabIndex = 12;
            this.lstAnagrafica.UseCompatibleStateImageBehavior = false;
            this.lstAnagrafica.SelectedIndexChanged += new System.EventHandler(this.lstAnagrafica_SelectedIndexChanged);
            this.lstAnagrafica.DoubleClick += new System.EventHandler(this.lstAnagrafica_DoubleClick);
            // 
            // cmdConnecti
            // 
            this.cmdConnecti.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdConnecti.Location = new System.Drawing.Point(573, 11);
            this.cmdConnecti.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmdConnecti.Name = "cmdConnecti";
            this.cmdConnecti.Size = new System.Drawing.Size(399, 38);
            this.cmdConnecti.TabIndex = 19;
            this.cmdConnecti.Text = "CONNETTI";
            this.cmdConnecti.UseVisualStyleBackColor = true;
            this.cmdConnecti.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtIp
            // 
            this.txtIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIp.Location = new System.Drawing.Point(123, 11);
            this.txtIp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtIp.Name = "txtIp";
            this.txtIp.Size = new System.Drawing.Size(443, 38);
            this.txtIp.TabIndex = 18;
            // 
            // lblIp
            // 
            this.lblIp.AutoSize = true;
            this.lblIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIp.Location = new System.Drawing.Point(61, 11);
            this.lblIp.Name = "lblIp";
            this.lblIp.Size = new System.Drawing.Size(51, 36);
            this.lblIp.TabIndex = 17;
            this.lblIp.Text = "IP:";
            // 
            // lblEPC
            // 
            this.lblEPC.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEPC.Location = new System.Drawing.Point(123, 53);
            this.lblEPC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblEPC.Name = "lblEPC";
            this.lblEPC.Size = new System.Drawing.Size(849, 38);
            this.lblEPC.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 36);
            this.label1.TabIndex = 11;
            this.label1.Text = "EPC:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(28, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 36);
            this.label3.TabIndex = 20;
            this.label3.Text = "TID:";
            // 
            // lblTID
            // 
            this.lblTID.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTID.Location = new System.Drawing.Point(123, 95);
            this.lblTID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblTID.Name = "lblTID";
            this.lblTID.Size = new System.Drawing.Size(849, 38);
            this.lblTID.TabIndex = 21;
            // 
            // chkUsaTid
            // 
            this.chkUsaTid.AutoSize = true;
            this.chkUsaTid.Checked = true;
            this.chkUsaTid.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUsaTid.Location = new System.Drawing.Point(123, 138);
            this.chkUsaTid.Name = "chkUsaTid";
            this.chkUsaTid.Size = new System.Drawing.Size(138, 21);
            this.chkUsaTid.TabIndex = 22;
            this.chkUsaTid.Text = "Copia TID in EPC";
            this.chkUsaTid.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AcceptButton = this.cmdScrivi;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 644);
            this.Controls.Add(this.chkUsaTid);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblTID);
            this.Controls.Add(this.cmdConnecti);
            this.Controls.Add(this.TabEPC);
            this.Controls.Add(this.txtIp);
            this.Controls.Add(this.lblIp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblEPC);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GAP Writer";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.TabEPC.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer tmrAntenna;
        private System.Windows.Forms.TabControl TabEPC;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button cmdConnecti;
        private System.Windows.Forms.TextBox txtIp;
        private System.Windows.Forms.Label lblIp;
        private System.Windows.Forms.CheckBox chkSerie;
        private System.Windows.Forms.TextBox lblEPC;
        private System.Windows.Forms.Button cmdScrivi;
        private System.Windows.Forms.TextBox txtNuovoEpc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView lstAnagrafica;
        private System.Windows.Forms.CheckBox chkAbilitaScrittura;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox lblTID;
        private System.Windows.Forms.CheckBox chkUsaTid;
        private System.Windows.Forms.Button cmdTutti;
        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCercaAnagrafica;
        private System.Windows.Forms.Button cmdNuovaAnagrafica;
    }
}

