﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SQLite;

namespace GAPWriter
{
    public partial class FrmAnagrafica : Form
    {
        public FrmAnagrafica()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Configuration.Configuration c = ConfigurationManager.OpenExeConfiguration(
             ConfigurationUserLevel.None) as Configuration;
            string dbPath = c.AppSettings.Settings["dbPath"].Value.ToString();
            SQLiteConnectionStringBuilder connectString = new SQLiteConnectionStringBuilder
            {
                DataSource = dbPath,
                ForeignKeys = false,
                JournalMode = SQLiteJournalModeEnum.Wal
            };
            using (SQLiteConnection connessione = new SQLiteConnection(connectString.ToString()))
            {
                connessione.Open();
                using (SQLiteCommand command = new SQLiteCommand("insert into Anagrafiche (codice,Denominazione,Pedonale,RilevaInAudit) values ('" + txtCodice.Text.Replace("'", "''") + "','" + txtDenominazione.Text.Replace("'", "''") + "'," +Convert.ToInt16(ckPedonale.Checked) + "," + Convert.ToInt16(ckAudit.Checked) + ")", connessione))
                {
                    try
                    {
                        SQLiteDataReader reader = command.ExecuteReader();
                    }
                    catch (SQLiteException ex)
                    {
                        if (ex.ErrorCode == 19)
                        {
                            MessageBox.Show("Codice già presente");
                        }
                        else
                        {
                            MessageBox.Show("Errore in inserimento anagrafica");
                        }
                    }
                }
            }
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void label3_Click(object sender, EventArgs e)
        {
        }

        private void FrmAnagrafica_Load(object sender, EventArgs e)
        {
        }
    }
}