﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace GAP3
{
    internal class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static object GlobalConfiguration { get; private set; }

        private static int Main(string[] args)
        {
            log.Debug("-->Avvio gap3");
            //lanciare con install per installarlo
            return (int)HostFactory.Run(x =>
            {
                x.UseLog4Net();
                x.Service<GapService>(s =>
                {
                    s.ConstructUsing(() => new GapService());
                    s.WhenStarted(service => service.Start());
                    s.WhenStopped(service => service.Stop());
                });
            });
        }
    }
}