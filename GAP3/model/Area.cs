﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GAP3.model
{
    [Table("Aree")]
    public class Area
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Principale { get; set; }
    }
}