﻿using GAP.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GAP3.model
{
    [Table("Regole")]
    public class Regola
    {
        public int ID { get; set; }
        public string Periodo { get; set; }

        public int Anagrafica_id { get; set; }

        [ForeignKey("Anagrafica_id")]
        public Anagrafica Anagrafica { get; set; }

        public int Area_id { get; set; }

        [ForeignKey("Area_id")]
        public Area Area { get; set; }
    }
}