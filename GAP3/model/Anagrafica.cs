﻿using GAP3.model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SQLite;

namespace GAP.Model
{
    [Table("Anagrafiche")]
    public class Anagrafica
    {
        public int ID { get; set; }
        public string Codice { get; set; }
        public string Denominazione { get; set; }
        public string Tag { get; set; }
        public bool RilevaInAudit { get; set; }
        public bool Pedonale { get; set; }
        public String CodiceCollegato { get; set; }
        public ICollection<Regola> Regole { get; set; }
    }
}