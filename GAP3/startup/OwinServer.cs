﻿using GAP3.util;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Thinktecture.IdentityModel.Owin;

namespace GAP3.startup
{
    public class OwinServer
    {
        private Dictionary<String, String> permessi;

        public void Configuration(IAppBuilder appBuilder)
        {
            HttpConfiguration config = new HttpConfiguration();
            config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.Formatting = Formatting.Indented;
            json.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            json.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            //config.Formatters.Add(new BrowserJsonFormatter());
            //config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            config.MapHttpAttributeRoutes();
            appBuilder.UseWebApi(config);

            appBuilder.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            appBuilder.UseDefaultFiles();
            var physicalFileSystem = new PhysicalFileSystem(@"./www");
            var options = new FileServerOptions
            {
                EnableDefaultFiles = true,
                FileSystem = physicalFileSystem
            };
            options.StaticFileOptions.FileSystem = physicalFileSystem;
            options.StaticFileOptions.ServeUnknownFileTypes = true;
            options.DefaultFilesOptions.DefaultFileNames = new[]
            {
                    "index.html"
            };
            appBuilder.UseFileServer(options);

            appBuilder.UseBasicAuthentication(new BasicAuthenticationOptions("SecureApi", Authenticate));
        }

        private Task<IEnumerable<Claim>> Authenticate(string username, string password)
        {
            if (permessi == null)
            {
                permessi = new Dictionary<string, string>
                {
                    { "admin", System.Configuration.ConfigurationManager.AppSettings["admin"] },
                    { "poweruser", System.Configuration.ConfigurationManager.AppSettings["poweruser"] },
                    { "user", System.Configuration.ConfigurationManager.AppSettings["user"] }
                };
            }
            if (permessi.ContainsKey(username) && permessi[username] == password)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Role, username)
            };
                return Task.FromResult<IEnumerable<Claim>>(claims);
            }

            return Task.FromResult<IEnumerable<Claim>>(null);
        }
    }
}