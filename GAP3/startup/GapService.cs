﻿using Microsoft.Owin.Hosting;
using System;
using System.Diagnostics;
using GAP3.model;
using GAP.Model;
using System.Data.Entity;
using System.Linq;

namespace GAP3
{
    public partial class GapService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void Start()
        {
            log.Debug("-->Avvio servizio gap3");
            StartOptions option;
            if (System.Diagnostics.Debugger.IsAttached)
            {
                option = new StartOptions("http://localhost:9000");
                log.Debug("server in attesa su localhost");
            }
            else
            {
                option = new StartOptions("http://*:9000");
                log.Debug("server in attesa su tutti gli indirizzi");
            }
            WebApp.Start<startup.OwinServer>(option);
            foreach (var process in Process.GetProcessesByName("GAPReader"))
            {
                process.Kill();
            }
        }

        public void Stop()
        {
            log.Debug("-->Blocco servizio gap3");
            log.Debug("-->Servizio GAP3 bloccato");
        }
    }
}