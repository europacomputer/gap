﻿using GAP.Model;
using GAP3.model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Web.Http;
using System.Linq;
using Newtonsoft.Json;
using System.Data.Entity.Core.Objects;
using Microsoft.EntityFrameworkCore;

namespace GAP.rest
{
    [RoutePrefix("api/anagrafiche")]
    public class AnagraficheController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Route("")]
        [HttpPost]
        ////[Authorize(Roles ="admin,poweruser")]
        public IHttpActionResult Modifica(Anagrafica anagrafica)
        {
            using (DatabaseContext dbContext = new DatabaseContext())
            {
                dbContext.Anagrafiche.Attach(anagrafica);
                dbContext.Entry<Anagrafica>(anagrafica).State = System.Data.Entity.EntityState.Modified;
                dbContext.SaveChanges();
            }
            return Ok();
        }

        [Route("")]
        [HttpPut]
        ////[Authorize(Roles ="admin,poweruser")]
        public IHttpActionResult Crea(Anagrafica anagrafica)
        {
            try
            {
                using (DatabaseContext dbContext = new DatabaseContext())
                {
                    dbContext.Anagrafiche.Add(anagrafica);
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error("Errore nel salvare l'anagrafica " + anagrafica.Codice, ex);
                throw ex;
            }
            return Ok();
        }

        [Route("{id}")]
        [HttpDelete]
        ////[Authorize(Roles ="admin,poweruser")]
        public IHttpActionResult Cancella(int id)
        {
            try
            {
                using (DatabaseContext dbContext = new DatabaseContext())
                {
                    Anagrafica anagrafica = dbContext.Anagrafiche.Find(id);

                    dbContext.Entry(anagrafica).Collection(a => a.Regole).Load();
                    dbContext.Regole.RemoveRange(anagrafica.Regole);
                    dbContext.Anagrafiche.Remove(anagrafica);
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error("Errore nella cancellazione dell'anagrafica", ex);
                throw ex;
            }
            return Ok();
        }

        [Route("")]
        [HttpGet]
        ////[Authorize(Roles ="admin,poweruser,user")]
        public IHttpActionResult Carica()
        {
            using (DatabaseContext dbContext = new DatabaseContext())
            {
                return Ok(dbContext.Anagrafiche.Include("Regole.Area").ToList());
            }
        }

        [Route("{ricerca}")]
        [HttpGet]
        ////[Authorize(Roles ="admin,poweruser,user")]
        public IHttpActionResult Carica(String ricerca)
        {
            try
            {
                using (DatabaseContext dbContext = new DatabaseContext())
                {
                    dbContext.Database.Log = Console.WriteLine;
                    return Ok(dbContext.Anagrafiche.Where<Anagrafica>(a => a.Codice.Contains(ricerca) || a.Denominazione.Contains(ricerca)).ToList());
                }
            }
            catch (Exception ex)
            {
                log.Error("errore nel caricare l'anagrafica con ricerca " + ricerca, ex);
                throw ex;
            }
        }
    }
}