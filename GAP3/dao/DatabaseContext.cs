﻿using GAP.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GAP3.model
{
    internal class DatabaseContext : DbContext
    {
        public DatabaseContext() :
            base(new SQLiteConnection()
            {
                ConnectionString = new SQLiteConnectionStringBuilder() { DataSource = "c:\\Users\\utente\\source\\repos\\Accessi\\GAP3\\bin\\Debug\\GAP.sqlite", ForeignKeys = true }.ConnectionString
            }, true)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            Database.Log = Console.Write;
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Area> Aree { get; set; }
        public DbSet<Anagrafica> Anagrafiche { get; set; }
        public DbSet<Regola> Regole { get; set; }
    }
}