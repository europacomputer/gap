import {
    Component,
    OnInit,
    EventEmitter,
    Output,
    Input,
    OnDestroy
} from "@angular/core";
import { ConfiguratoreTagService } from "../service/configuratoretag.service";
import { Subscription, Observable } from "rxjs";
import { Anagrafica } from "app/domain/Anagrafica";
import { TagConfigReaderWriter } from "../domain/TagConfigReaderWriter";
import { AnagraficheService } from "../service/anagrafiche.service";
import { Message } from "primeng/components/common/message";

@Component({
    selector: "app-tagwriter",
    templateUrl: "./tagwriter.component.html",
    styleUrls: ["./tagwriter.component.scss"]
})
export class TagwriterComponent implements OnInit, OnDestroy {
    tag: TagConfigReaderWriter;
    nuovoEpc: String;
    updateObservable: Subscription;
    msgs: Message[] = [];
    @Output() onUpdated: EventEmitter<String> = new EventEmitter<String>();
    _anagraficaToEdit: Anagrafica;
    _startReader: boolean;
    constructor(
        private tagWriterService: ConfiguratoreTagService,
        private anagraficaService: AnagraficheService
    ) {}

    ngOnDestroy(): void {
        if (this.updateObservable != null) {
            this.updateObservable.unsubscribe();
        }
    }
    ngOnInit() {
        this.tag = new TagConfigReaderWriter();
        this.tag.Epc = "non valido";
        this.nuovoEpc = "";
    }

    @Input()
    set anagraficaToEdit(anagrafica: Anagrafica) {
        this._anagraficaToEdit = new Anagrafica(anagrafica);
        this.nuovoEpc = "";
    }

    @Input()
    set startReader(start: boolean) {
        this._startReader = start;
        this.tag = new TagConfigReaderWriter();
        this.tag.Epc = "non valido";
        this.nuovoEpc = "";
        if (this._startReader) {
            this.updateObservable = Observable.interval(1000).subscribe(() =>
                this.tagWriterService
                    .leggiTag()
                    .subscribe(data => (this.tag = data))
            );
        } else {
            if (this.updateObservable != null) {
                this.updateObservable.unsubscribe();
            }
        }
    }

    associa() {
        this._anagraficaToEdit.Tag = this.tag.Epc;
        this.anagraficaService
            .salvaAnagrafica(this._anagraficaToEdit)
            .subscribe(
                data => {
                    console.error("anag salvata");
                    this._startReader = false;
                    this.updateObservable.unsubscribe();
                    this.onUpdated.emit(this.tag.Epc);
                },
                err => {
                    console.error("anag errore nel salva");
                    this.msgs = [];
                    this.msgs.push({
                        severity: "error",
                        summary: "Tag già associato",
                        detail: "Il tag è già associato ad altra anagrafica"
                    });
                }
            );
    }

    reset() {
        this.updateObservable.unsubscribe();
        this.tagWriterService.reset().subscribe();
        this.startReader = true;
    }

    copiaTidInEpc() {
        this.nuovoEpc = this.tag.Tid;
    }

    scriviEpc() {
        this.tagWriterService.scriviTag(this.nuovoEpc).subscribe(
            dataWriter => {
                this.tagWriterService
                    .leggiTag()
                    .subscribe(data => (this.tag = data));
            },
            err => {
                this.msgs = [];
                this.msgs.push({
                    severity: "error",
                    summary: "Errore",
                    detail: "Errore nello scrivere il tag"
                });
            }
        );
    }
}
