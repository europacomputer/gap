import { Component, OnInit } from "@angular/core";
import { Regola } from "app/domain/Regola";
import { PermessiService } from "app/service/permessi.service";
import { PermessiEditorComponent } from "app/permessi/permessi-editor/permessi-editor.component";

@Component({
    selector: "app-permessi",
    templateUrl: "./permessi.component.html",
    styleUrls: ["./permessi.component.scss"]
})
export class PermessiComponent implements OnInit {
    regole: Array<Regola>;
    dialogEdit: boolean = false;
    regolaInEdit: Regola;
    loading: boolean;
    constructor(private permessiService: PermessiService) {}

    ngOnInit() {
        this.carica();
    }

    public carica() {
        this.loading = true;
        this.permessiService.caricaRegole().subscribe(data => {
            this.regole = data;
            this.loading = false;
        });
    }

    public modificaRegola(regola: Regola) {
        this.regolaInEdit = regola;
        this.dialogEdit = true;
    }

    public nuovaRegola() {
        this.regolaInEdit = new Regola();
        this.dialogEdit = true;
    }

    public update(regola: Regola) {
        let aggiungi: boolean = this.regolaInEdit.Id === null;
        Object.assign(this.regolaInEdit, regola);
        if (aggiungi) {
            this.regole.push(this.regolaInEdit);
        }
        this.dialogEdit = false;
    }

    public applicaRegole() {
        this.permessiService.applicaRegole();
    }

    public cancellaRegola(regola: Regola): void {
        console.log(regola);
        this.permessiService.cancellaRegola(regola).subscribe(() => {
            let index = this.regole.findIndex(d => d.Id === regola.Id);
            this.regole.splice(index, 1);
        });
    }
}
