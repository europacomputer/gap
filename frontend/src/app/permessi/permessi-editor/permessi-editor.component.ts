import {
    Component,
    OnInit,
    Input,
    OnChanges,
    SimpleChanges,
    EventEmitter,
    Output
} from "@angular/core";
import { Regola } from "app/domain/Regola";
import { Area } from "app/domain/Area";
import { AreeService } from "app/service/aree.service";
import { PermessiService } from "app/service/permessi.service";

@Component({
    selector: "app-permessi-editor",
    templateUrl: "./permessi-editor.component.html",
    styleUrls: ["./permessi-editor.component.scss"]
})
export class PermessiEditorComponent implements OnInit, OnChanges {
    @Output() onUpdate: EventEmitter<Regola> = new EventEmitter<Regola>();
    @Input() regolaToEdit: Regola;
    model: Regola;
    aree: Array<Area>;

    ngOnChanges(changes: SimpleChanges): void {
        if (changes["regolaToEdit"]) {
            this.model = new Regola(this.regolaToEdit);
        }
    }

    constructor(
        private permessiService: PermessiService,
        private areeService: AreeService
    ) {
        this.areeService.caricaStato().subscribe(data => (this.aree = data));
    }

    ngOnInit() {}

    public salva(): void {
        this.permessiService.salvaRegola(this.model).subscribe((id: number) => {
            this.model.Id = id;
            this.onUpdate.emit(this.model);
        });
    }
}
