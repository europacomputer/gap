import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "environments/environment";

const API_URL = environment.apiUrl;
@Component({
    selector: "my-app",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"]
})
export class AppComponent {
    constructor(private router: Router) {
        this.router.routeReuseStrategy.shouldReuseRoute = function() {
            return false;
        };
    }
}
