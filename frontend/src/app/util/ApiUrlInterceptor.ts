import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent
} from "@angular/common/http";
import { Injectable, Inject, InjectionToken } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";
import "rxjs/add/observable/empty";

export const API_URL = new InjectionToken<string>("apiUrl");

@Injectable()
export class ApiUrlInterceptor implements HttpInterceptor {
    constructor(
        //@Inject(API_URL) private apiUrl: string,
        private router: Router
    ) { }

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        //req = req.clone({ url: this.prepareUrl(req.url) });
        return next.handle(req).catch((error, caught) => {
            if (error.status === 401) {
                //logout users, redirect to login page
                //redirect to the signin page or show login modal here
                this.router.navigate(["/home"]); //remember to import router class and declare it in the class
                return Observable.throw(error);
            } else {
                return Observable.throw(error);
            }
        });
    }
}
