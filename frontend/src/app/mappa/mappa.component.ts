import { Component, OnInit } from "@angular/core";
import { ImpiantoService } from "app/service/impianto.service";
import { StatoImpianto } from "app/domain/stato.impianto";
import { Observable } from "rxjs/Observable";
import { GateService } from "app/service/gate.service";
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: "app-mappa",
    templateUrl: "./mappa.component.html",
    styleUrls: ["./mappa.component.scss"]
})
export class MappaComponent implements OnInit {
    sub: Subscription;
    statoAntenne: Array<StatoImpianto>;
    statoGates: Array<StatoImpianto>;
    constructor(
        private impiantoService: ImpiantoService,
        private gateService: GateService
    ) {}

    caricaStato(): void {
        this.impiantoService
            .caricaImpianto()
            .subscribe((stati: Array<StatoImpianto>) => {
                this.statoGates = stati.filter(
                    (stato: StatoImpianto) => stato.Tipo === "GATE"
                );
                this.statoAntenne = stati.filter(
                    (stato: StatoImpianto) => stato.Tipo === "ANTENNA"
                );
            });
    }

    ngOnInit() {
        this.caricaStato();
        this.sub = Observable.interval(8000).subscribe(() =>
            this.caricaStato()
        );
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    getStyle(stato: StatoImpianto) {
        let style = {
            top: stato.Y + "px",
            left: stato.X + "px",
            position: "absolute"
        };
        return style;
    }

    apriGate(codiceGate: string): void {
        this.gateService.apri(codiceGate);
    }
}
