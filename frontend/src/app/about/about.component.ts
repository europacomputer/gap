/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { SafeHtml } from "@angular/platform-browser/src/security/dom_sanitization_service";

@Component({
    styleUrls: ["./about.component.scss"],
    templateUrl: "./about.component.html"
})
export class AboutComponent {
    open: Boolean = false;
    example: SafeHtml;

    constructor(private sanitized: DomSanitizer) {
        this.example = `{
    "pages":{
        "from":1,
        "size":2000
    },
    "filters": [
        {
        "property":"data",
        "value":">16/1/2018"
        },
        {
        "property":"evento",
        "value":"ENTRATA"
        }
    ]
}`;
    }
}
