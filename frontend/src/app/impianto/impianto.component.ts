import { Component, OnInit } from "@angular/core";
import { ImpiantoService } from "app/service/impianto.service";
import { StatoImpianto } from "app/domain/stato.impianto";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/interval";
import { GateService } from "app/service/gate.service";
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: "app-impianto",
    templateUrl: "./impianto.component.html",
    styleUrls: ["./impianto.component.scss"]
})
export class ImpiantoComponent implements OnInit {
    statoAntenne: Array<StatoImpianto>;
    statoGates: Array<StatoImpianto>;
    refreshObservable: Subscription;
    constructor(
        private impiantoService: ImpiantoService,
        private gateService: GateService
    ) { }

    ngOnInit() {
        this.caricaStato();
        this.refreshObservable = Observable.interval(2000).subscribe(() => this.caricaStato());
    }

    ngOnDestroy() {
        this.refreshObservable.unsubscribe();
    }

    caricaStato(): void {
        this.impiantoService
            .caricaImpianto()
            .subscribe((stati: Array<StatoImpianto>) => {
                this.statoGates = stati.filter(
                    (stato: StatoImpianto) => stato.Tipo === "GATE"
                );
                this.statoAntenne = stati.filter(
                    (stato: StatoImpianto) => stato.Tipo === "ANTENNA"
                );
            });
    }
    apriGate(codiceGate: string): void {
        this.gateService.apri(codiceGate);
    }
}
