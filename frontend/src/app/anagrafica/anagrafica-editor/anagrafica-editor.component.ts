import {
    Component,
    OnInit,
    SimpleChanges,
    Output,
    Input,
    EventEmitter,
    OnChanges
} from "@angular/core";
import { Anagrafica } from "app/domain/Anagrafica";
import { AnagraficheService } from "app/service/anagrafiche.service";
import { AreeService } from "../../service/aree.service";
import { Area } from "../../domain/Area";
import { PermessiService } from "../../service/permessi.service";
import { Message } from "../../../../node_modules/primeng/components/common/message";

@Component({
    selector: "app-anagrafica-editor",
    templateUrl: "./anagrafica-editor.component.html",
    styleUrls: ["./anagrafica-editor.component.scss"]
})
export class AnagraficaEditorComponent implements OnInit {
    displayTagWriter: boolean = false;
    @Output()
    onUpdate: EventEmitter<Anagrafica> = new EventEmitter<Anagrafica>();
    anagraficaToEdit: Anagrafica;
    model: Anagrafica;
    anagraficheResult: String[];
    anagrafiche: String[];
    aree: Area[];
    areeSelezionate: Area[];
    msgs: Message[] = [];

    @Input("AnagraficaToEdit")
    set AnagraficaToEdit(anag: Anagrafica) {
        console.error("CARICO");
        this.anagraficaToEdit = anag;
        this.model = this.anagraficaToEdit;
        this.permessiService
            .caricaAreeRegole(this.anagraficaToEdit.Tag)
            .subscribe(data => (this.areeSelezionate = data));
    }

    showTagWriter() {
        this.displayTagWriter = true;
    }

    ricercaAnagrafica(event) {
        this.anagraficaService
            .ricercaAnagrafiche(event.query)
            .subscribe((data: Array<string>) => {
                this.anagraficheResult = data;
            });
    }

    constructor(
        private anagraficaService: AnagraficheService,
        private areeService: AreeService,
        private permessiService: PermessiService
    ) {}

    ngOnInit() {
        this.areeService
            .caricaAree()
            .subscribe((data: Array<Area>) => (this.aree = data));
    }

    public salva(): void {
        this.anagraficaService.salvaAnagrafica(this.model).subscribe(
            () => this.onUpdate.emit(this.model),
            err => {
                this.msgs = [];
                this.msgs.push({
                    severity: "error",
                    summary: "Tag già associato",
                    detail: "Il tag è già associato ad altra anagrafica"
                });
            }
        );
        if (this.model.Tag !== undefined && this.model.Tag.length > 0) {
            this.permessiService
                .associaAree(this.model.Tag, this.areeSelezionate)
                .subscribe();
        }
    }
}
