import { Component, OnInit } from "@angular/core";
import { Anagrafica } from "app/domain/Anagrafica";
import { AnagraficheService } from "app/service/anagrafiche.service";
import { Message } from "primeng/components/common/message";

@Component({
    selector: "app-anagrafica",
    templateUrl: "./anagrafica.component.html",
    styleUrls: ["./anagrafica.component.scss"]
})
export class AnagraficaComponent implements OnInit {
    loading: boolean = true;
    anagrafiche: Array<Anagrafica>;
    dialogEdit: boolean = false;
    anagraficaInEdit: Anagrafica;
    anagraficaSelezionata: Anagrafica;
    displayTagWriter: boolean = false;
    constructor(private anagraficaService: AnagraficheService) {}

    ngOnInit() {
        this.carica();
    }

    showTagWriter(anagrafica: Anagrafica) {
        this.anagraficaInEdit = new Anagrafica(anagrafica);
        this.anagraficaSelezionata = anagrafica;
        this.displayTagWriter = true;
    }

    carica(): void {
        this.loading = true;
        this.anagraficaService.caricaAnagrafiche().subscribe(data => {
            this.anagrafiche = data;
            this.loading = false;
        });
    }

    public modificaAnagrafica(anagrafica: Anagrafica) {
        this.anagraficaInEdit = new Anagrafica(anagrafica);
        this.anagraficaSelezionata = anagrafica;
        this.dialogEdit = true;
    }

    public nuovaAnagrafica() {
        this.anagraficaInEdit = new Anagrafica();
        this.anagraficaSelezionata = new Anagrafica();
        this.dialogEdit = true;
    }

    public update(anagrafica: Anagrafica) {
        let aggiungi: boolean = this.anagraficaSelezionata.Codice === undefined;
        Object.assign(this.anagraficaSelezionata, anagrafica);
        if (aggiungi) {
            this.anagrafiche.push(this.anagraficaSelezionata);
        }
        this.dialogEdit = false;
    }

    public tagAssociato(tag: string) {
        this.anagraficaSelezionata.Tag = tag;
        this.displayTagWriter = false;
    }

    public applicaAnagrafiche() {
        this.anagraficaService.applicaRegole();
    }

    public cancellaAnagrafica(anagrafica: Anagrafica): void {
        console.log(anagrafica);
        this.anagraficaService.cancellaAnagrafica(anagrafica).subscribe(() => {
            let index = this.anagrafiche.findIndex(
                d => d.Codice === anagrafica.Codice
            );
            this.anagrafiche.splice(index, 1);
        });
    }
}
