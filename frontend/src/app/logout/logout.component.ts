import { Component, OnInit } from "@angular/core";
import { ImpiantoService } from "app/service/impianto.service";
import { ActivatedRoute, Router } from "@angular/router";
import { GateService } from "../service/gate.service";

@Component({
    selector: "app-logout",
    templateUrl: "./logout.component.html",
    styleUrls: ["./logout.component.scss"]
})
export class LogoutComponent implements OnInit {
    constructor(
        private impiantoService: ImpiantoService,
        private router: Router
    ) {}

    ngOnInit() {}

    logout() {
        this.impiantoService
            .logout()
            .subscribe(data => console.log("OK"), error => this.naviga());
    }

    naviga() {
        this.router.navigate(["/home"]);
    }
}
