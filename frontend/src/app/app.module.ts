import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AreeService } from "./service/aree.service";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { ClarityModule } from "clarity-angular";
import { AppComponent } from "./app.component";
import { ROUTING } from "./app.routing";
import { HomeComponent } from "./home/home.component";
import { AboutComponent } from "./about/about.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuditComponent } from "./audit/audit.component";
import { AuditService } from "app/service/audit.service";
import { GateService } from "app/service/gate.service";
import { ImpiantoComponent } from "./impianto/impianto.component";
import { ImpiantoService } from "app/service/impianto.service";
import { PermessiComponent } from "./permessi/permessi.component";
import { PermessiService } from "app/service/permessi.service";
import { PermessiEditorComponent } from "./permessi/permessi-editor/permessi-editor.component";
import { ApiUrlInterceptor } from "app/util/ApiUrlInterceptor";
import { environment } from "environments/environment";
import { AnagraficaComponent } from "./anagrafica/anagrafica.component";
import { AnagraficheService } from "app/service/anagrafiche.service";
import { AnagraficaEditorComponent } from "./anagrafica/anagrafica-editor/anagrafica-editor.component";
import { MappaComponent } from "./mappa/mappa.component";
import { ConfigurazioneComponent } from "./configurazione/configurazione.component";
import { AntenneComponent } from "./configurazione/antenne/antenne.component";
import { AntenneService } from "app/service/antenne.service";
import { GatesComponent } from "./configurazione/gates/gates.component";
import { AreaComponent } from "./configurazione/area/area.component";
import { AreaEditorComponent } from "./configurazione/area/area-editor/area-editor.component";
import { GatesEditorComponent } from "./configurazione/gates/gates-editor/gates-editor.component";
import { AntenneEditorComponent } from "./configurazione/antenne/antenne-editor/antenne-editor.component";
import { SettingComponent } from "./configurazione/setting/setting.component";
import { ConfigurazioneService } from "app/service/configurazione.service";
import { LogoutComponent } from "./logout/logout.component";
import { DropdownModule } from "primeng/dropdown";
import { AutoCompleteModule } from "primeng/autocomplete";
import { MultiSelectModule } from "primeng/multiselect";
import { ListboxModule } from "primeng/listbox";
import { TagwriterComponent } from "./tagwriter/tagwriter.component";
import { ConfiguratoreTagService } from "./service/configuratoretag.service";
import { OverlayPanelModule } from "primeng/overlaypanel";
import { DialogModule } from "primeng/dialog";
import { MessagesModule } from "primeng/messages";
import { MessageModule } from "primeng/message";
import { GrowlModule } from "primeng/growl";
import { InputMaskModule } from "primeng/InputMask";

@NgModule({
    declarations: [
        AppComponent,
        AboutComponent,
        HomeComponent,
        AuditComponent,
        ImpiantoComponent,
        PermessiComponent,
        PermessiEditorComponent,
        AnagraficaComponent,
        AnagraficaEditorComponent,
        MappaComponent,
        ConfigurazioneComponent,
        AntenneComponent,
        GatesComponent,
        AreaComponent,
        AreaEditorComponent,
        GatesEditorComponent,
        AntenneEditorComponent,
        SettingComponent,
        LogoutComponent,
        TagwriterComponent
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ClarityModule.forRoot(),
        ROUTING,
        AutoCompleteModule,
        DropdownModule,
        MultiSelectModule,
        ListboxModule,
        OverlayPanelModule,
        DialogModule,
        MessageModule,
        MessagesModule,
        GrowlModule,
        InputMaskModule
    ],
    providers: [
        AreeService,
        AuditService,
        GateService,
        ImpiantoService,
        PermessiService,
        AnagraficheService,
        AntenneService,
        ConfigurazioneService,
        ConfiguratoreTagService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ApiUrlInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
