import { Component, OnInit } from "@angular/core";
import { AuditService } from "app/service/audit.service";
import { Audit } from "app/domain/Audit";
import { State } from "clarity-angular";
import { AuditHtml } from "../domain/AuditHtml";

@Component({
    selector: "app-audit",
    templateUrl: "./audit.component.html",
    styleUrls: ["./audit.component.scss"]
})
export class AuditComponent implements OnInit {
    state: State;
    audits: Array<Audit>;
    totalRecord: Number = 0;
    loading: boolean = true;
    parametriRicerca: State;
    _visualizzaAreeprincipali: boolean = true;


    set visualizzaAreeprincipali(valore: boolean) {
        this._visualizzaAreeprincipali = valore;
        this.carica(this.state);
    }

    get visualizzaAreeprincipali(): boolean {
        return this._visualizzaAreeprincipali;
    }

    stampa(): void {
        let printContents: AuditHtml;
        let popupWin;
        this.auditService.caricaAuditHtml(this.state, this.visualizzaAreeprincipali).subscribe(data => {
            printContents = data;
            popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
            popupWin.document.open();
            popupWin.document.write(`
              <html>
                <head>
                  <title>Log accessi</title>
                </head>
            <body onload="window.print();window.close()">${data.testo}</body>
              </html>`
            );
            popupWin.document.close();
        });

    }

    refresh(state: State) {
        this.loading = true;
        this.state = state;
        this.carica(state);
    }
    constructor(private auditService: AuditService) { }

    ngOnInit() {
        //    let state: State;
        //    state.page.from = 0;
        //  state.page.size = 50;
        // this.carica(state);
    }

    ricarica(): void {
        this.loading = false;
        let state: State;
        state.page.from = 0;
        state.page.size = 50;
        this.carica(state);
    }



    carica(state: State): void {
        this.auditService
            .caricaNumRecord(state, this.visualizzaAreeprincipali)
            .subscribe((result: Number) => (this.totalRecord = result));
        this.auditService
            .caricaAudit(state, this.visualizzaAreeprincipali)
            .subscribe((result: Array<Audit>) => {
                this.audits = result;
                this.loading = false;
            });
        this.parametriRicerca = state;
    }
}
