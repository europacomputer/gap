/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { ModuleWithProviders } from "@angular/core/src/metadata/ng_module";
import { Routes, RouterModule } from "@angular/router";

import { AboutComponent } from "./about/about.component";
import { HomeComponent } from "./home/home.component";
import { AuditComponent } from "app/audit/audit.component";
import { PermessiComponent } from "app/permessi/permessi.component";
import { AnagraficaComponent } from "app/anagrafica/anagrafica.component";
import { MappaComponent } from "app/mappa/mappa.component";
import { ConfigurazioneComponent } from "app/configurazione/configurazione.component";
import { environment } from "environments/environment";
import { LogoutComponent } from "./logout/logout.component";

const API_URL = environment.apiUrl;

export const ROUTES: Routes = [
    { path: "", redirectTo: "home", pathMatch: "full" },
    { path: "home", component: HomeComponent },
    { path: "audit", component: AuditComponent },
    { path: "permessi", component: PermessiComponent },
    { path: "anagrafiche", component: AnagraficaComponent },
    { path: "mappa", component: MappaComponent },
    { path: "configurazione", component: ConfigurazioneComponent },
    { path: "about", component: AboutComponent },
    { path: "logout", component: LogoutComponent },
    { path: "**", redirectTo: "home" }
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(ROUTES, {
    enableTracing: false
});
