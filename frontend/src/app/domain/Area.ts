import { Presenza } from "app/domain/Presenza";
import { Gate } from "app/domain/Gate";

export class Area {
    Nome: string;
    Presenze?: Array<Presenza>;
    Gates?: Array<Gate>;
    NomeAreaPrincipale?: String = "";
    Id?: Number;

    public constructor(area: Area = undefined) {
        if (area !== undefined) {
            this.Id = area.Id;
            this.Nome = area.Nome;
            this.Presenze = area.Presenze;
            this.NomeAreaPrincipale = area.NomeAreaPrincipale;
        }
    }
}
