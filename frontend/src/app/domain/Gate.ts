export class Gate {
    Codice: string;
    Nome: string;
    Stato: string;
    Id: Number;
    Ip: String;
    Porta: Number;
    X: String;
    Y: String;

    public constructor(gate: Gate = undefined) {
        if (gate !== undefined) {
            this.Codice = gate.Codice;
            this.Nome = gate.Nome;
            this.Id = gate.Id;
            this.Porta = gate.Porta;
            this.X = gate.X;
            this.Y = gate.Y;
            this.Ip = gate.Ip;
        }
    }
}
