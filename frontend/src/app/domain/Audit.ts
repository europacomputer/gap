export class Audit {
    Id: Number;
    Tag: string;
    Evento: string;
    Anomalia: string;
    Nota: string;
    Data: Number;
    DataAzione: Date;
    Area: string;
    Denominazione: String;
}
