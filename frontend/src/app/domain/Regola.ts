import { Tag } from "app/domain/Tag";

export class Regola {
    Tag1: Tag;
    Tag2: Tag;
    Periodo: String;
    Area: String;
    Id: Number;
    AnagraficaTag1: String;

    constructor(regola: Regola = undefined) {
        this.Tag1 = new Tag();
        this.Tag2 = new Tag();
        this.Id = null;
        if (regola != undefined) {
            this.Id = regola.Id;
            this.Area = regola.Area;
            this.Tag1.Codice = regola.Tag1.Codice;
            this.Tag2.Codice = regola.Tag2.Codice;
            this.Periodo = regola.Periodo;
        }
    }
}
