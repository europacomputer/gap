import { Tag } from "app/domain/Tag";
import { Anagrafica } from "app/domain/Anagrafica";

export class Presenza {
    Tag: Tag;
    NomeArea: string;
    Data: number;
    DataAzione: Date;
    Anagrafica: Anagrafica;
    Durata: number;
    Descrizione: String;

    get DurataValida(): boolean {
        console.log(this.Durata);
        return this.Durata < 2;
    }
}
