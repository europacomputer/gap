import { Regola } from "./Regola";

export class Anagrafica {
    Codice: string;
    Denominazione: string;
    Tag: string;
    RilevaInAudit: boolean;
    Pedonale: boolean;
    CodiceCollegato: string;

    public constructor(anagrafica: Anagrafica = undefined) {
        if (anagrafica !== undefined) {
            this.Codice = anagrafica.Codice;
            this.Denominazione = anagrafica.Denominazione;
            this.Tag = anagrafica.Tag;
            this.RilevaInAudit = anagrafica.RilevaInAudit;
            this.Pedonale = anagrafica.Pedonale;
            this.CodiceCollegato = anagrafica.CodiceCollegato;
        }
    }
}
