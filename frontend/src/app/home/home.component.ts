/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { Component, OnInit, OnDestroy } from "@angular/core";
import { AreeService } from "app/service/aree.service";
import { Presenza } from "app/domain/Presenza";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/interval";
import { Area } from "app/domain/Area";
import { GateService } from "app/service/gate.service";
import { Gate } from "app/domain/Gate";
import { ImpiantoComponent } from "app/impianto/impianto.component";
import { RadioControlValueAccessor } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs/Subscription";
import { AnagraficheService } from "../service/anagrafiche.service";
import { DatePipe } from "../../../node_modules/@angular/common";

@Component({
    styleUrls: ["./home.component.scss"],
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit, OnDestroy {
    anagraficheResult: String[];
    subParam: Subscription;
    aree: Area[];
    presenzaDaForzare: Presenza;
    nomeIngresso: string;
    notaIngresso: string;
    areaIngresso: string;
    confermaUscitaDialog: boolean = false;
    forzaEntrataDialog: boolean = false;
    notaForzatura: string;
    gates: Array<Gate>;
    gateSelezionato: Gate;
    emptyGate: Gate = new Gate();
    dataIngresso: string;
    datePipe: DatePipe = new DatePipe("en-US");
    constructor(
        private areeService: AreeService,
        private gateService: GateService,
        private anagraficaService: AnagraficheService
    ) {}

    ngOnInit() {
        this.emptyGate.Nome = "NESSUNO";
        this.carica();
        this.subParam = Observable.interval(2000).subscribe(() =>
            this.carica()
        );
    }

    ngOnDestroy() {
        this.subParam.unsubscribe();
    }

    confermaEntrata(): void {
        this.forzaEntrataDialog = false;
        this.areeService
            .forzaEntrata(
                this.areaIngresso,
                this.nomeIngresso,
                this.notaIngresso,
                this.dataIngresso
            )
            .subscribe(() => {
                this.carica();
                this.nomeIngresso = undefined;
                this.notaIngresso = undefined;
            });
    }

    ricercaAnagrafica(event) {
        this.anagraficaService
            .ricercaAnagrafiche(event.query)
            .subscribe((data: Array<string>) => {
                this.anagraficheResult = data;
            });
    }

    confermaUscita(): void {
        console.log(
            "Forza uscita presenza " +
                this.presenzaDaForzare.Tag.Codice +
                this.notaForzatura
        );
        this.confermaUscitaDialog = false;
        this.areeService
            .forzaUscita(
                this.presenzaDaForzare,
                this.notaForzatura,
                this.gateSelezionato.Codice
            )
            .subscribe(() => {
                this.carica();
                this.notaForzatura = "";
                this.gateSelezionato = undefined;
            });
    }

    carica(): void {
        this.areeService.caricaStato().subscribe((data: Array<Area>) => {
            this.aree = data;
        });
    }

    forzaIngresso(area: Area): void {
        this.dataIngresso = this.datePipe.transform(
            new Date(),
            "dd/MM/yyyy HH:mm"
        );
        this.forzaEntrataDialog = true;
        this.areaIngresso = area.Nome;
    }

    forzaMovimento(presenza: Presenza): void {
        this.gates = this.aree.find(a => a.Nome === presenza.NomeArea).Gates;
        this.gates.unshift(this.emptyGate);
        this.presenzaDaForzare = presenza;
        this.notaForzatura = "";
        this.confermaUscitaDialog = true;
        this.gateSelezionato = this.gates[0];
    }

    stampaStato(): void {
        let popupWin;
        this.areeService.caricaStatoHtml().subscribe((data: string) => {
            popupWin = window.open(
                "",
                "_blank",
                "top=0,left=0,height=100%,width=auto"
            );

            popupWin.document.open();
            popupWin.document.write(`
              <html>
                <head>
                  <title>Log accessi</title>
                </head>
            <body onload='window.print();window.close()'>${data}</body>
              </html>`);
            popupWin.document.close();
        });
    }
}
