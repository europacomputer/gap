import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Audit } from "app/domain/Audit";
import "rxjs/add/operator/shareReplay";
import "rxjs/add/observable/throw";
import { environment } from "environments/environment";
import { Observable } from "rxjs/Observable";
import { State } from "clarity-angular";
import { AuditHtml } from "../domain/AuditHtml";

const API_URL = environment.apiUrl;

@Injectable()
export class AuditService {
    constructor(private http: HttpClient) {}

    public caricaAudit(
        state: State,
        soloAreePrincipali: boolean
    ): Observable<Array<Audit>> {
        let parameterAreePrincipali: String = "";
        if (soloAreePrincipali) {
            parameterAreePrincipali = "?areeprincipali";
        }
        return this.http
            .post<Array<Audit>>(
                API_URL + `audit/ricerche` + parameterAreePrincipali,
                state
            )
            .shareReplay()
            .catch(this.handleError);
    }

    public caricaAuditHtml(
        state: State,
        soloAreePrincipali: boolean
    ): Observable<AuditHtml> {
        let parameterAreePrincipali: String = "";
        if (soloAreePrincipali) {
            parameterAreePrincipali = "?areeprincipali";
        }
        return this.http
            .post<AuditHtml>(
                API_URL + `audit/ricerche/html` + parameterAreePrincipali,
                state
            )
            .shareReplay()
            .catch(this.handleError);
    }

    public caricaAuditCsv(
        state: State,
        soloAreePrincipali: boolean
    ): Observable<String> {
        let parameterAreePrincipali: String = "";
        if (soloAreePrincipali) {
            parameterAreePrincipali = "?areeprincipali";
        }
        return this.http
            .post<String>(API_URL + `audit/ricerche/csv`, state)
            .shareReplay()
            .catch(this.handleError);
    }

    public caricaNumRecord(
        state: State,
        soloAreePrincipali: boolean
    ): Observable<Number> {
        let parameterAreePrincipali: String = "";
        if (soloAreePrincipali) {
            parameterAreePrincipali = "?areeprincipali";
        }
        return this.http
            .post<Array<Audit>>(
                API_URL + `audit/size` + parameterAreePrincipali,
                state
            )
            .shareReplay()
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        console.error("ApiService::handleError", error);
        return Observable.throw(error);
    }
}
