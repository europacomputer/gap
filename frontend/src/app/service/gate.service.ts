import { environment } from "environments/environment";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/shareReplay";
import { Gate } from "app/domain/Gate";

const API_URL = environment.apiUrl;

@Injectable()
export class GateService {
    constructor(private http: HttpClient) {}

    cancellaGate(gate: Gate): Observable<Number> {
        return this.http.delete<Number>(`${API_URL}gate/${gate.Id}`);
    }
    private handleError(error: Response | any) {
        console.error("ApiService::handleError", error);
        return Observable.throw(error);
    }

    public caricaGates(): Observable<Array<Gate>> {
        return this.http
            .get<Array<Gate>>(API_URL + "gate")
            .shareReplay()
            .catch(this.handleError);
    }

    salvaGate(gate: Gate): Observable<Object> {
        return this.http.post(`${API_URL}gate`, gate);
    }

    public apri(codiceGate: String): void {
        this.http
            .post(`${API_URL}gate/${codiceGate}/stato`, {}, {})
            .shareReplay()
            .subscribe();
    }
}
