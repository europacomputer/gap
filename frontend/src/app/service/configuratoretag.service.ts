import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from "environments/environment";
import { TagConfigReaderWriter } from "../domain/TagConfigReaderWriter";

const API_URL = environment.apiUrl;

@Injectable()
export class ConfiguratoreTagService {
    constructor(private http: HttpClient) {}
    leggiTag(): Observable<TagConfigReaderWriter> {
        return this.http
            .get<TagConfigReaderWriter>(`${API_URL}antenneconfigurazione`)
            .shareReplay()
            .catch(this.handleError);
    }

    reset(): Observable<void> {
        return this.http
            .get<void>(`${API_URL}antenneconfigurazione/restart`)
            .shareReplay()
            .catch(this.handleError);
    }

    scriviTag(tag: String): Observable<void> {
        return this.http
            .get<void>(`${API_URL}antenneconfigurazione/${tag}`)
            .shareReplay()
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        console.error("ApiService::handleError", error);
        return Observable.throw(error);
    }
}
