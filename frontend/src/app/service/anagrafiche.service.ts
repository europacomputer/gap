import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Anagrafica } from "app/domain/Anagrafica";
import { environment } from "environments/environment";

const API_URL = environment.apiUrl;

@Injectable()
export class AnagraficheService {
    salvaAnagrafica(anagrafica: Anagrafica): Observable<Object> {
        return this.http
            .post(`${API_URL}anagrafiche`, anagrafica)
            .shareReplay()
            .catch(this.handleError);
    }

    applicaRegole(): void {
        this.http.put(`${API_URL}anagrafiche`, null).subscribe();
    }

    cancellaAnagrafica(anagrafica: Anagrafica): Observable<Object> {
        return this.http.delete(`${API_URL}anagrafiche/${anagrafica.Codice}`);
    }

    constructor(private http: HttpClient) {}
    public caricaAnagrafiche(): Observable<Array<Anagrafica>> {
        return this.http
            .get<Array<Anagrafica>>(API_URL + "anagrafiche")
            .shareReplay()
            .catch(this.handleError);
    }

    public ricercaAnagrafiche(query: string): Observable<Array<String>> {
        return this.http
            .get<Array<String>>(API_URL + `anagrafiche/${query}`)
            .shareReplay()
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        console.error("ApiService::handleError", error);
        return Observable.throw(error);
    }
}
