import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import "rxjs/add/operator/shareReplay";
import "rxjs/add/observable/throw";
import { environment } from "environments/environment";
import { Observable } from "rxjs/Observable";
import { Configurazione } from "app/domain/Configurazione";
import { Observer } from "rxjs/Observer";

const API_URL = environment.apiUrl;
@Injectable()
export class ConfigurazioneService {
    constructor(private http: HttpClient) { }

    public caricaConfigurazione(): Observable<Configurazione> {
        return this.http
            .get<Configurazione>(API_URL + `configurazione`)
            .shareReplay()
            .catch(this.handleError);
    }

    public testMail(): Observable<String> {
        return this.http.get<String>(`${API_URL}configurazione/mail/test`, {});
    }

    public salvaConfigurazione(
        configurazione: Configurazione
    ): Observable<void> {
        return this.http.put<void>(`${API_URL}configurazione`, configurazione);
    }

    private handleError(error: Response | any) {
        console.error("ApiService::handleError", error);
        return Observable.throw(error);
    }
}
