import { environment } from "environments/environment";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import { Presenza } from "app/domain/Presenza";
import { Area } from "app/domain/Area";
import "rxjs/add/operator/shareReplay";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/mergeMap";

const API_URL = environment.apiUrl;

@Injectable()
export class AreeService {
    constructor(private http: HttpClient) {}

    public caricaAree(): Observable<Array<Area>> {
        return this.http
            .get<Array<Area>>(API_URL + "area")
            .shareReplay()
            .catch(this.handleError);
    }

    cancellaArea(area: Area): Observable<Object> {
        return this.http.delete(`${API_URL}area/${area.Id}`);
    }

    public caricaAreePrincipale(): Observable<Array<Area>> {
        return this.http
            .get<Array<Area>>(API_URL + "area/principali")
            .shareReplay()
            .catch(this.handleError);
    }

    public caricaStato(): Observable<Array<Area>> {
        return this.http
            .get<Array<Area>>(API_URL + "area/stato")
            .shareReplay()
            .catch(this.handleError);
    }

    public caricaStatoHtml(): Observable<String> {
        return this.http
            .get<Array<Area>>(API_URL + "area/stato/html")
            .shareReplay()
            .catch(this.handleError);
    }

    public forzaEntrata(
        codiceArea: String,
        nomeIngresso: String,
        notaIngresso: String,
        dataEntrata: String
    ): Observable<Object> {
        return this.http.request(
            "put",
            `${API_URL}area/${codiceArea}/${nomeIngresso}`,
            { body: `"${dataEntrata};${notaIngresso}"` }
        );
    }

    public forzaUscita(
        presenza: Presenza,
        nota: string,
        gate: String
    ): Observable<Object> {
        return this.http.request(
            "put",
            `${API_URL}area/${presenza.NomeArea}/${gate}/${
                presenza.Tag.Codice
            }`,
            { body: '"' + nota + '"' }
        );
    }

    salvaArea(area: Area): Observable<Object> {
        return this.http.post(`${API_URL}area`, area);
    }

    private handleError(error: Response | any) {
        console.error("ApiService::handleError", error);
        return Observable.throw(error);
    }
}
