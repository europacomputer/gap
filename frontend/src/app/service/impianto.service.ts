import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "environments/environment";
import { StatoImpianto } from "app/domain/stato.impianto";
import { Observable } from "rxjs/Observable";

const API_URL = environment.apiUrl;

@Injectable()
export class ImpiantoService {
    constructor(private http: HttpClient) {}

    restart(): void {
        this.http
            .put(`${API_URL}impianto`, {})
            .shareReplay()
            .subscribe();
    }

    public logout(): Observable<Object> {
        let headers = new HttpHeaders({
            "Content-Type": "application/json"
        }).set("authorization", "Basic xxx");
        return this.http
            .get(API_URL + "gate", {
                headers: headers
            })
            .shareReplay();
    }

    caricaImpianto(): Observable<Array<StatoImpianto>> {
        return this.http
            .get<Array<StatoImpianto>>(`${API_URL}impianto`)
            .shareReplay()
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        console.error("ApiService::handleError", error);
        return Observable.throw(error);
    }
}
