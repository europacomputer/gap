import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "environments/environment";
import { Observable } from "rxjs/Observable";
import { Regola } from "app/domain/Regola";
import { Area } from "../domain/Area";

const API_URL = environment.apiUrl;

@Injectable()
export class PermessiService {
    constructor(private http: HttpClient) {}

    caricaAreeRegole(tag: String): Observable<Array<Area>> {
        return this.http
            .get<Array<Area>>(`${API_URL}permessi/${tag}`)
            .shareReplay()
            .catch(this.handleError);
    }

    caricaRegole(): Observable<Array<Regola>> {
        return this.http
            .get<Array<Regola>>(`${API_URL}permessi`)
            .shareReplay()
            .catch(this.handleError);
    }

    salvaRegola(regola: Regola): Observable<Object> {
        return this.http.post(`${API_URL}permessi`, regola);
    }

    associaAree(tag: String, areeSelezionate: Array<Area>): Observable<Object> {
        return this.http.post(`${API_URL}permessi/${tag}`, areeSelezionate);
    }

    applicaRegole(): void {
        this.http.put(`${API_URL}permessi`, null).subscribe();
    }

    cancellaRegola(regola: Regola): Observable<Object> {
        return this.http.delete(`${API_URL}permessi/${regola.Id}`);
    }

    private handleError(error: Response | any) {
        console.error("ApiService::handleError", error);
        return Observable.throw(error);
    }
}
