import { environment } from "environments/environment";
import { Injectable } from "@angular/core";
import { Antenna } from "app/domain/Antenna";
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";

const API_URL = environment.apiUrl;

@Injectable()
export class AntenneService {
    constructor(private http: HttpClient) { }

    public caricaAntenne(): Observable<Array<Antenna>> {
        return this.http.get<Array<Antenna>>(API_URL + 'antenne').shareReplay().catch(this.handleError);
    }

    salvaAntenne(antenna: Antenna): Observable<Object> {
        return this.http.post(`${API_URL}antenne`, antenna);
    }

    private handleError(error: Response | any) {
        console.error('ApiService::handleError', error);
        return Observable.throw(error);
    }
}
