import { Component, OnInit } from "@angular/core";
import { AreeService } from "app/service/aree.service";
import { Area } from "app/domain/Area";
import { AntenneService } from "app/service/antenne.service";
import { Antenna } from "app/domain/Antenna";
import { ImpiantoService } from "app/service/impianto.service";

@Component({
    selector: "app-configurazione",
    templateUrl: "./configurazione.component.html",
    styleUrls: ["./configurazione.component.scss"]
})
export class ConfigurazioneComponent {
    constructor(private impiantoService: ImpiantoService) {}

    riavvia() {
        this.impiantoService.restart();
    }
}
