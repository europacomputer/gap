import { Component, OnInit } from "@angular/core";
import { Area } from "app/domain/Area";
import { AreeService } from "app/service/aree.service";
import { Observable } from "rxjs/Observable";

@Component({
    selector: "app-area",
    templateUrl: "./area.component.html",
    styleUrls: ["./area.component.scss"]
})
export class AreaComponent implements OnInit {
    aree: Array<Area>;
    areaInEdit: Area;
    dialogEdit: Boolean;
    constructor(private areeService: AreeService) {}

    ngOnInit() {
        this.caricaAree();
    }

    public modificaArea(area: Area) {
        this.areaInEdit = area;
        this.dialogEdit = true;
    }

    public nuovaArea() {
        this.areaInEdit = new Area();
        this.dialogEdit = true;
    }

    public update(area: Area) {
        let aggiungi: boolean = this.areaInEdit.Id === undefined;
        Object.assign(this.areaInEdit, area);
        if (aggiungi) {
            this.aree.push(this.areaInEdit);
        }
        this.dialogEdit = false;
    }

    public caricaAree() {
        this.areeService.caricaAree().subscribe(data => (this.aree = data));
    }

    public cancellaArea(area: Area): void {
        this.areeService
            .cancellaArea(area)
            .catch(this.handleErrorCancellazione)
            .subscribe(() => {
                let index = this.aree.findIndex(d => d.Id === area.Id);
                this.aree.splice(index, 1);
            });
    }

    private handleErrorCancellazione(
        error: Response | any
    ): Observable<Number> {
        console.log(error.error);
        return Observable.throw(error);
    }
}
