import {
    Component,
    OnInit,
    Output,
    Input,
    EventEmitter,
    SimpleChanges
} from "@angular/core";
import { Area } from "app/domain/Area";
import { AreeService } from "app/service/aree.service";
import { OnChanges } from "@angular/core/src/metadata/lifecycle_hooks";

@Component({
    selector: "app-area-editor",
    templateUrl: "./area-editor.component.html",
    styleUrls: ["./area-editor.component.scss"]
})
export class AreaEditorComponent implements OnInit, OnChanges {
    @Output() onUpdate: EventEmitter<Area> = new EventEmitter<Area>();
    @Input() areaToEdit: Area;
    model: Area;
    areePrincipali: Array<Area>;
    emptyArea: Area = new Area();
    constructor(private areaService: AreeService) { }

    public salva(): void {
        this.areaService.salvaArea(this.model).subscribe((id: number) => {
            if (this.areaToEdit.Id === undefined) {
                this.model.Id = id;
            }
            this.onUpdate.emit(this.model);
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes["areaToEdit"]) {
            this.model = new Area(this.areaToEdit);
        }
    }

    ngOnInit() {
        this.areaService
            .caricaAreePrincipale()
            .subscribe(data => {
                this.areePrincipali = data;
                this.emptyArea.Nome = "NESSUNO";
                this.areePrincipali.unshift(new Area());
            }
            );
    }
}
