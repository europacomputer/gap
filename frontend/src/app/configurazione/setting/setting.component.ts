import { Component, OnInit } from "@angular/core";
import { ConfigurazioneService } from "app/service/configurazione.service";
import { Configurazione } from "app/domain/Configurazione";

@Component({
    selector: "app-setting",
    templateUrl: "./setting.component.html",
    styleUrls: ["./setting.component.scss"]
})
export class SettingComponent implements OnInit {
    testResult: String;
    showTestResult: boolean;
    configurazione: Configurazione;
    submitTestMail: boolean;
    constructor(private configurazioneService: ConfigurazioneService) {}

    ngOnInit() {
        this.configurazioneService
            .caricaConfigurazione()
            .subscribe(data => (this.configurazione = data));
    }

    salva(): void {
        this.testResult = "";
        this.configurazioneService
            .salvaConfigurazione(this.configurazione)
            .subscribe();
    }

    testMail(): void {
        this.submitTestMail = true;
        this.configurazioneService.testMail().subscribe(data => {
            this.testResult = data;
            this.submitTestMail = false;
            this.showTestResult = true;
        });
    }
}
