import {
    Component,
    OnInit,
    OnChanges,
    Output,
    EventEmitter,
    Input,
    SimpleChanges
} from "@angular/core";
import { Antenna } from "app/domain/Antenna";
import { AntenneService } from "app/service/antenne.service";
import { AreeService } from "app/service/aree.service";
import { Area } from "app/domain/Area";
import { Gate } from "app/domain/Gate";
import { GateService } from "app/service/gate.service";

@Component({
    selector: "app-antenne-editor",
    templateUrl: "./antenne-editor.component.html",
    styleUrls: ["./antenne-editor.component.scss"]
})
export class AntenneEditorComponent implements OnInit, OnChanges {
    aree: Area[];
    gates: Gate[];
    @Output() onUpdate: EventEmitter<Antenna> = new EventEmitter<Antenna>();
    @Input() antennaToEdit: Antenna;
    model: Antenna;
    constructor(
        private antennaService: AntenneService,
        private areeService: AreeService,
        private gatesService: GateService
    ) {}

    public salva(): void {
        this.antennaService.salvaAntenne(this.model).subscribe((id: number) => {
            this.onUpdate.emit(this.model);
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes["antennaToEdit"]) {
            this.model = new Antenna(this.antennaToEdit);
        }
    }

    ngOnInit() {
        this.areeService.caricaAree().subscribe(data => (this.aree = data));
        this.gatesService.caricaGates().subscribe(data => (this.gates = data));
    }
}
