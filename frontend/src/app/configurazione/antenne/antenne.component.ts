import { Component, OnInit } from "@angular/core";
import { Antenna } from "app/domain/Antenna";
import { AntenneService } from "app/service/antenne.service";

@Component({
    selector: "app-antenne",
    templateUrl: "./antenne.component.html",
    styleUrls: ["./antenne.component.scss"]
})
export class AntenneComponent implements OnInit {
    antenne: Array<Antenna>;
    antennaInEdit: Antenna;
    dialogEdit: Boolean;
    constructor(private antenneService: AntenneService) {}

    public caricaAntenne() {
        this.antenneService
            .caricaAntenne()
            .subscribe(data => (this.antenne = data));
    }

    public modificaAntenna(antenna: Antenna) {
        this.antennaInEdit = antenna;
        this.dialogEdit = true;
    }

    public update(antenna: Antenna) {
        Object.assign(this.antennaInEdit, antenna);
        this.dialogEdit = false;
    }

    ngOnInit() {
        this.caricaAntenne();
    }
}
