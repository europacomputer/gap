import {
    Component,
    OnInit,
    OnChanges,
    Output,
    Input,
    EventEmitter,
    SimpleChanges
} from "@angular/core";
import { Gate } from "app/domain/Gate";
import { GateService } from "app/service/gate.service";

@Component({
    selector: "app-gates-editor",
    templateUrl: "./gates-editor.component.html",
    styleUrls: ["./gates-editor.component.scss"]
})
export class GatesEditorComponent implements OnInit, OnChanges {
    @Output() onUpdate: EventEmitter<Gate> = new EventEmitter<Gate>();
    @Input() gateToEdit: Gate;
    model: Gate;
    constructor(private gateService: GateService) {}

    public salva(): void {
        this.gateService.salvaGate(this.model).subscribe((id: number) => {
            if (this.gateToEdit.Id === undefined) {
                this.model.Id = id;
            }
            this.onUpdate.emit(this.model);
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes["gateToEdit"]) {
            this.model = new Gate(this.gateToEdit);
        }
    }

    ngOnInit() {}
}
