import { Component, OnInit } from "@angular/core";
import { Gate } from "app/domain/Gate";
import { GateService } from "app/service/gate.service";
import { Observable } from "rxjs/Observable";

@Component({
    selector: "app-gates",
    templateUrl: "./gates.component.html",
    styleUrls: ["./gates.component.scss"]
})
export class GatesComponent implements OnInit {
    gates: Array<Gate>;
    gateInEdit: Gate;
    dialogEdit: Boolean;

    constructor(private gateService: GateService) {}

    public caricaGates() {
        this.gateService.caricaGates().subscribe(data => (this.gates = data));
    }

    ngOnInit() {
        this.caricaGates();
    }

    public modificaGate(gate: Gate) {
        this.gateInEdit = gate;
        this.dialogEdit = true;
    }

    public nuovoGate() {
        this.gateInEdit = new Gate();
        this.dialogEdit = true;
    }

    public update(gate: Gate) {
        let aggiungi: boolean = this.gateInEdit.Id === undefined;
        Object.assign(this.gateInEdit, gate);
        if (aggiungi) {
            this.gates.push(this.gateInEdit);
        }
        this.dialogEdit = false;
    }

    public cancellaGate(area: Gate): void {
        this.gateService
            .cancellaGate(area)
            .catch(this.handleErrorCancellazione)
            .subscribe(() => {
                let index = this.gates.findIndex(d => d.Id === area.Id);
                this.gates.splice(index, 1);
            });
    }
    private handleErrorCancellazione(
        error: Response | any
    ): Observable<Number> {
        console.log(error.error);
        return Observable.throw(error);
    }
}
