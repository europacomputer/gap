﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace GAP2
{
    public class Program
    {
        //lanciare con install per installarlo
        public static int Main(string[] args)
        {
            HostFactory.New(x =>
            {
                x.UseLog4Net();
            });
            //lanciare con install per installarlo
            return (int)HostFactory.Run(x =>
            {
                x.Service<GapService>(s =>
                {
                    s.ConstructUsing(() => new GapService());
                    s.WhenStarted(service => service.Start());
                    s.WhenStopped(service => service.Stop());
                });
            });
        }
    }
}